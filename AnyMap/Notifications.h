//
//  Notifications.h
//  AnyMap
//
//  Created by Antonio M on 07/12/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface Notifications : NSObject
@property (nonatomic)NSInteger  idNotification;
@property (nonatomic)NSInteger  type;
@property (strong,nonatomic)NSString *message;
@property (nonatomic)NSInteger profile_id;
@property (strong,nonatomic)NSString *profile_name;
@property (strong,nonatomic)NSString *profile_image;
@property (nonatomic)NSInteger profile_following;
@property (nonatomic)NSInteger profile_followers;
@property (nonatomic)NSInteger pin_id;
@property (strong,nonatomic)NSString *pin_title;
@property (strong,nonatomic)NSString *pin_image;
@property (strong,nonatomic)NSString *pin_description;
@property (nonatomic)NSInteger mapId;
@property (nonatomic)NSInteger mapNoPins;
@property (nonatomic)NSInteger mapCreatorId;
@property (strong,nonatomic)NSString *mapDescription;
@property (strong,nonatomic)NSString *mapCreatorImage;
@property (strong,nonatomic)NSString *mapTitle;
@property (strong,nonatomic)NSString *createdAt;
@property (nonatomic)NSInteger isDeleted;
@property (nonatomic)NSInteger isRead;
@property (strong,nonatomic)NSString *from;
@property (strong,nonatomic)NSString *title;
@property (strong,nonatomic)NSString *subTitle;
@property (strong,nonatomic)NSString *tickerText;
@end
//"gcm.message_id" = "0:1449719326919613%8c64989f8c64989f";
//"gcm.notification.created_at" = "blalala ";
//"gcm.notification.from" = 349495834376;
//"gcm.notification.id" = 132;
//"gcm.notification.is_push" = 1;
//"gcm.notification.map_creator_id" = 474;
//"gcm.notification.map_creator_image" = "http://d3ih5u5soisk0n.cloudfront.net/profile_images/fundamentallyflawedprofilepic.png, android.support.content.wakelockid=2";
//"gcm.notification.map_description" = "If supermarket white loaves don't cut it for you, then its time to head out the doors to these amazing bakeries that line the shelves everyday with fresh baguettes, pain de mie , croissants and everything in between.";
//"gcm.notification.map_id" = 2704;
//"gcm.notification.map_no_of_pins" = 21;
//"gcm.notification.map_title" = "Best Artisan Bakeries in Singapore";
//"gcm.notification.pin_description" = "";
//"gcm.notification.pin_id" = 2578;
//"gcm.notification.pin_image" = "http://54.169.108.42/images/pin_images/PPWH.png";
//"gcm.notification.pin_title" = "Murder of Huang Na";
//"gcm.notification.profile_id" = 232332;
//"gcm.notification.profile_name" = joko;
//"gcm.notification.subtitle" = "trending map ";
//"gcm.notification.tickerText" = nothing;
//"gcm.notification.type" = 4;
//"gcm.notification.vibrate" = 1;