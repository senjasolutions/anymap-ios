/*-*-objc-*-*/
//  ExploreMapViewController.h
//  AnyMap
//
//  Created by Antonio M on 17/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>
#ifndef NO_GM
#import <GoogleMaps/GoogleMaps.h>
#endif

@interface ExploreMapViewController : UIViewController

#ifndef NO_GM
@property (strong, nonatomic) IBOutlet GMSMapView *mapView;
#endif
@property(nonatomic)NSInteger index;
@end
