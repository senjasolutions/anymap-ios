//
//  NotificationTableViewController.h
//  AnyMap
//
//  Created by Antonio M on 23/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationTableViewController : UITableViewController
@property (strong,nonatomic)NSMutableArray *notifikasi;
@end
