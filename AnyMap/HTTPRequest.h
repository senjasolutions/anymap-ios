/*-*-c++-*-*/

#ifndef __ANYMAP_HTTP_REQUEST_H__
#define __ANYMAP_HTTP_REQUEST_H__

#if DEBUG && __APPLE__
#include <iostream>
#endif

namespace AnyMap {

template<typename T> class HTTPRequest {
public:
    void send(T* write_data, std::size_t (*write_function)(char const*, std::size_t, std::size_t, T*))
    {
        if (handle) {
            curl_easy_setopt(handle, CURLOPT_WRITEDATA, write_data);
            curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, write_function);
            curl_easy_setopt(handle, CURLOPT_URL, url->c_str());
            curl_easy_setopt(handle, CURLOPT_FOLLOWLOCATION, 1L);
#ifdef DEBUG
            curl_easy_setopt(handle, CURLOPT_VERBOSE, 1L);
            char error[CURL_ERROR_SIZE];
            curl_easy_setopt(handle, CURLOPT_ERRORBUFFER, error);
            auto res =
#endif
            curl_easy_perform(handle);
#ifdef DEBUG
            if (res != CURLE_OK)
                std::cerr << "cURL perform error: " << error << std::endl;
        } else {
            std::cerr << "cURL handle is NULL" << std::endl;
#endif
        }
    }
protected:
    HTTPRequest(std::string const& base_url)
    {
        if ((handle = curl_easy_init()))
            url = new std::string(base_url);
    }
    virtual ~HTTPRequest()
    {
        if (handle) {
            delete url;
            curl_easy_cleanup(handle);
        }
    }
    CURL* get_handle() { return handle; }
    std::string* get_url() { return url; }
private:
    CURL* handle;
    std::string* url;
};

}

#endif
