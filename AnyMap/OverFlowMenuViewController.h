//
//  OverFlowMenuViewController.h
//  AnyMap
//
//  Created by Antonio M on 23/10/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol overFlowDelegate
- (void)showEdit;
- (void)hideOverFlow;
- (void)rateTheApp;
- (void)feedback;
- (void)aboutAM;
@end
@interface OverFlowMenuViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *tapGesture;
@property (weak)id<overFlowDelegate>delegateOverFlow;
@end
