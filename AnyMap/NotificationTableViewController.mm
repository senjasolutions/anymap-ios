//
//  NotificationTableViewController.m
//  AnyMap
//
//  Created by Antonio M on 23/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//
#import "NotificationTableViewController.h"
#import "NotificationTableViewCell.h"
#import "OtherCreatorViewController.h"
#import "MapDetailLocationViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface NotificationTableViewController ()

@end

@implementation NotificationTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    CGFloat ratio = 0;
    if (screenHeight == 480.0f)
    {
        ratio = 97;
    }
    else if (screenHeight == 568.0f)
    {
        ratio = 97;
    }
    else if (screenHeight == 667.0f)
    {
        ratio = 97;
    }
    else if (screenHeight == 736.0f)
    {
        ratio = 97;
    }

    self.tableView.rowHeight = ratio;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self loadTheDataFromPlistNotification];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)loadTheDataFromPlistNotification
{
        NSError *errorDesc = nil;
        NSPropertyListFormat format;
        NSString *plistPath;
        NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                  NSUserDomainMask, YES) objectAtIndex:0];
        plistPath = [rootPath stringByAppendingPathComponent:@"Notifications.plist"];
        if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
            plistPath = [[NSBundle mainBundle] pathForResource:@"Notifications" ofType:@"plist"];
        }
        NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
        NSDictionary *temp = (NSDictionary *)[NSPropertyListSerialization propertyListWithData:plistXML options:NSPropertyListMutableContainersAndLeaves format:&format error:&errorDesc];
        
        if (!temp) {
            NSLog(@"Error reading plist: %@, format: %lu", errorDesc, (unsigned long)format);
        }
    self.notifikasi =(NSMutableArray*)temp;
            NSLog(@"data notifikasi : %@ ",self.notifikasi);
    [self.tableView reloadData];
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.notifikasi.count + 1.
;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    
    if(indexPath.row == 0)
    {
        NotificationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"notifCell"];
        
        
        if (!cell) {
            [tableView registerNib:[UINib nibWithNibName:@"NotificationCell" bundle:nil] forCellReuseIdentifier:@"notifCell"];
            cell = [tableView dequeueReusableCellWithIdentifier:@"notifCell" forIndexPath:indexPath];
            
        }
        cell.notificationTime.text = @"";
        return cell;
    }
    else
    {
        
        NotificationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"notifCell"];
        
        
        if (!cell) {
            [tableView registerNib:[UINib nibWithNibName:@"NotificationCell" bundle:nil] forCellReuseIdentifier:@"notifCell"];
            cell = [tableView dequeueReusableCellWithIdentifier:@"notifCell" forIndexPath:indexPath];
            
        }
        

        
    
        NSString *key = [self.notifikasi objectAtIndex:indexPath.row -1];
        //config the cell
        cell.notificationDescrip.text = [key valueForKey:@"message"];
        if ([[key valueForKey:@"createdAt"]isEqualToString:@"null"]) {
            cell.notificationTime.text =@"";
        }
        else{
            cell.notificationTime.text = [key valueForKey:@"createdAt"];
        }
        
        
        if ([[key valueForKey:@"type"]isEqualToString:@"0"]) {
            if ([key valueForKey:@"profile_image"] == (id)[NSNull null]) {
                cell.notificationImage.image = [UIImage imageNamed:@""];
            }
            else
            {
                [cell.notificationImage sd_setImageWithURL:[NSURL URLWithString:[key valueForKey:@"profile_image"]] placeholderImage:[UIImage imageNamed:@""] ];
            }
            
        }
        else if ([[key valueForKey:@"type"]isEqualToString:@"1"])
        {
            if ([key valueForKey:@"profile_image"] == (id)[NSNull null]) {
                cell.notificationImage.image = [UIImage imageNamed:@""];
            }
            else
            {
                [cell.notificationImage sd_setImageWithURL:[NSURL URLWithString:[key valueForKey:@"profile_image"]] placeholderImage:[UIImage imageNamed:@""] ];
            }
            
        }
        else if ([[key valueForKey:@"type"]isEqualToString:@"2"])
        {
            if ([key valueForKey:@"pin_image"] == (id)[NSNull null]) {
                cell.notificationImage.image = [UIImage imageNamed:@""];
            }
            else
            {
                [cell.notificationImage sd_setImageWithURL:[NSURL URLWithString:[key valueForKey:@"pin_image"]] placeholderImage:[UIImage imageNamed:@""] ];
            }
            
        }
        else if ([[key valueForKey:@"type"]isEqualToString:@"3"])
        {
            if ([key valueForKey:@"profile_image"] == (id)[NSNull null]) {
                cell.notificationImage.image = [UIImage imageNamed:@""];
            }
            else
            {
                [cell.notificationImage sd_setImageWithURL:[NSURL URLWithString:[key valueForKey:@"profile_image"]] placeholderImage:[UIImage imageNamed:@""] ];
            }
            
        }
        else if ([[key valueForKey:@"type"]isEqualToString:@"4"])
        {
            if ([key valueForKey:@"profile_image"] == (id)[NSNull null]) {
                cell.notificationImage.image = [UIImage imageNamed:@""];
            }
            else
            {
                [cell.notificationImage sd_setImageWithURL:[NSURL URLWithString:[key valueForKey:@"profile_image"]] placeholderImage:[UIImage imageNamed:@""] ];
            }
            
        }
        
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
    //do nothing
    }
    else
    {
    NSString *value = [self.notifikasi objectAtIndex:indexPath.row -1];
    
    if ([[value valueForKey:@"type"]isEqualToString:@"4"]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        MapDetailLocationViewController *mapDetailLocation = [storyboard instantiateViewControllerWithIdentifier:@"MapDetailLocation"];
        
        mapDetailLocation.titleLocation = [value valueForKey:@"map_title"];
        mapDetailLocation.mapID = [value valueForKey:@"map_id"];
        mapDetailLocation.indexRow = indexPath.row -1;
        mapDetailLocation.isFromBookmarked=  YES;
        [self presentViewController:mapDetailLocation animated:YES completion:nil];
    }
    else if ([[value valueForKey:@"type"]isEqualToString:@"3"]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"OuterProfile" bundle:nil];
        OtherCreatorViewController *otherCreator= [storyboard instantiateViewControllerWithIdentifier:@"OtherCreatorViewController"];
        otherCreator.profile = [value valueForKey:@"profile_name"];
        otherCreator.counterFollowers = [value valueForKey:@"profile_followers"];
        otherCreator.counterFollowings = [value valueForKey:@"profile_following"];
        otherCreator.describes= [value valueForKey:@"map_title"];
        otherCreator.identifier = [value valueForKey:@"profile_id"];
        otherCreator.picURL = [value valueForKey:@"profile_image"];
        otherCreator.mapID = [value valueForKey:@"map_id"];
        [self.navigationController presentViewController:otherCreator animated:YES completion:nil];
    }
        else
        {
            //do nothing
        }
    
    }
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
