//
//  ExploreRootViewController.m
//  AnyMap
//
//  Created by Antonio M on 29/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import "ExploreRootViewController.h"
#import "ExploreMapListViewController.h"
#import "ExploreMapViewController.h"
#import "InterestFilterViewController.h"
#import "SearchViewController.h"
#import "mapDetailCollectionViewController.h"
@interface ExploreRootViewController ()<filterDelegate>
@property(nonatomic)BOOL conditionVC;
@property(nonatomic)BOOL searchCondition;
@property(nonatomic,strong)InterestFilterViewController *interestFilter;
@end

@implementation ExploreRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.textSearch.delegate = self;
    [[UITextField appearance] setTintColor:[UIColor whiteColor]];

    UIViewController *currentVC = [self.childViewControllers objectAtIndex:0];
    if ([currentVC isKindOfClass:[ExploreMapViewController class]]) {
        if (!self.conditionVC) {
            ExploreMapListViewController *emlVC = [[UIStoryboard storyboardWithName:@"ExploreList" bundle:nil]instantiateViewControllerWithIdentifier:@"exploreMapList"];
            [self addChildViewController:emlVC];
            [emlVC didMoveToParentViewController:self];
            emlVC.view.frame = self.mapContainerView.bounds;
            [self.mapContainerView addSubview:emlVC.view];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)switchVC:(id)sender {
    ExploreMapViewController *map = (ExploreMapViewController *)[self.childViewControllers objectAtIndex:0];
    ExploreMapListViewController *mapList = (ExploreMapListViewController *)[self.childViewControllers objectAtIndex:1];
        if (!self.conditionVC) {
            self.hamburgerMenu.image = [UIImage imageNamed:@"btn_list_mode"];
            self.conditionVC = YES;
            [map didMoveToParentViewController:self];
             
            [self.mapContainerView addSubview:map.view];


        }else
        {
            self.conditionVC = NO;
            self.hamburgerMenu.image = [UIImage imageNamed:@"btn_map_mode"];
            [mapList didMoveToParentViewController:self];
            [self.mapContainerView addSubview:mapList.view];
        }
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}
- (IBAction)filtering:(id)sender {
    self.interestFilter = [[InterestFilterViewController alloc]initWithNibName:@"InterestFilterView" bundle:nil];
    self.interestFilter.view.frame = CGRectMake(0.0f,0.0f, self.parentViewController.view.frame.size.width, self.parentViewController.view.frame.size.height);
    self.interestFilter.view.alpha = 0.0f;
    NSLog(@" nilai frame size %@",NSStringFromCGSize(self.interestFilter.view.frame.size));
    [self.tabBarController.view insertSubview:self.interestFilter.view aboveSubview:self.tabBarController.view];
    self.interestFilter.delegateFilter = self;
    [UIView animateWithDuration:0.2f animations:^{
        self.interestFilter.view.alpha =1.0f;
    }completion:^(BOOL finished){}];
}
- (void)done
{
    NSLog(@"done filter");
    [UIView animateWithDuration:0.2f animations:^{
        self.interestFilter.view.alpha = 0.0f;
    }completion:^(BOOL finished){
    }];
}
#pragma UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.tag ==0 ) {
        textField.placeholder = @"Interests or Topics";
    }
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    if (self.textSearch.text.length != 0) {
     [self performSegueWithIdentifier:@"searchResult" sender:self];
    }
    else
    {
        //no respon
    }
    return YES;
}
- (IBAction)showSearch:(id)sender {
    UIBarButtonItem *bar = (UIBarButtonItem *)sender;
    if (!self.searchCondition) {
        self.searchCondition = YES;
        bar.image = [UIImage imageNamed:@"btn_close.png"];
        [UIView animateWithDuration:0.2f animations:^{
            [self.searchField setHidden:NO];
            
        }completion:^(BOOL FINISHED)
         {
             [self.textSearch  becomeFirstResponder];
         }];
    }
    else
    {
        if (self.textSearch.text.length ==0) {
            self.searchCondition = NO;
            bar.image = [UIImage imageNamed:@"btn_search.png"];
            [UIView animateWithDuration:0.2f animations:^{
                [self.searchField setHidden:YES];
                
            }completion:^(BOOL FINISHED)
             {
                 [self.textSearch  resignFirstResponder];
             }];
   
        }
        else
        {
            self.textSearch.text = @"";
        }
    
    }

}

 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     if ([segue.identifier isEqualToString:@"searchResult"]) {
         SearchViewController *search  = (SearchViewController *)[segue destinationViewController];
         search.resultString = self.textSearch.text;
     }
 }


@end
