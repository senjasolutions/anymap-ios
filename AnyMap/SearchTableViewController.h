//
//  SearchTableViewController.h
//  AnyMap
//
//  Created by Antonio M on 29/10/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchTableViewController : UITableViewController
@property (strong,nonatomic)NSString *term;
@end
