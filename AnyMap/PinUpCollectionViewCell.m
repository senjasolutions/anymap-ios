
//
//  PinUpCollectionViewCell.m
//  AnyMap
//
//  Created by Antonio M on 06/10/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import "PinUpCollectionViewCell.h"

@implementation PinUpCollectionViewCell
- (id)initWithCoder:(NSCoder *)decoder
{
    if ((self = [super initWithCoder:decoder]) != nil) {
        NSString *nibName = @"PinPopupCell";
        UIView *view = [[UINib nibWithNibName:nibName bundle:nil] instantiateWithOwner:self options:nil].firstObject;
        CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
        CGFloat ratio = 0;
        if (screenHeight == 480.0f)
        {
            ratio = 284;
        }
        else if (screenHeight == 568.0f)
        {
            ratio = 284;
        }
        else if (screenHeight == 667.0f)
        {
            ratio = 338;
        }
        else if (screenHeight == 736.0f)
        {
            ratio = 338;
        }
        self.contentView.frame =CGRectMake(0, 0, ratio, 438);
        self.frame = CGRectMake(0, 0, ratio, 438);
        view.frame= CGRectMake(0, 0, ratio, 438);
        NSLog(@" size frame pin up : %@",NSStringFromCGSize(view.frame.size));
        NSLog(@" content view frame pin up : %@",NSStringFromCGSize(self.contentView.frame.size));
        NSLog(@" cell frame  pin up : %@",NSStringFromCGSize(self.frame.size));
        NSLog(@" cell bounds pin up : %@",NSStringFromCGRect(self.bounds));
        [self.contentView addSubview:view];
    }
    
    return self;
}
- (id)initWithFrame:(CGRect)frame
{
    if ((self =[super initWithFrame:frame]) != nil) {
    }
    return self;
}


@end
