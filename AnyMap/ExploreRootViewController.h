//
//  ExploreRootViewController.h
//  AnyMap
//
//  Created by Antonio M on 29/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExploreRootViewController : UIViewController<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UIBarButtonItem *hamburgerMenu;

@property (strong, nonatomic) IBOutlet UIView *mapContainerView;
@property (strong, nonatomic) IBOutlet UIView *searchContainer;
@property (strong, nonatomic) IBOutlet UITextField *textSearch;
@property (strong, nonatomic) IBOutlet UIView *searchField;

@end
