/*-*-objc-*-*/

#import <Google/CloudMessaging.h>
#import <Google/Analytics.h>
#import "Constants.h"
#import <CoreLocation/CoreLocation.h>
#import "Notifications.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <IQKeyboardManager/KeyboardManager.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,GGLInstanceIDDelegate, GCMReceiverDelegate,CLLocationManagerDelegate>
@property (strong,nonatomic) CLLocationManager *locationManager;
@property (strong,nonatomic) CLLocation *currentLocation;
@property (strong, nonatomic) id<GAITracker> tracker;
//@property (assign, nonatomic) AnyMap::User *currentUser;
@property (strong, nonatomic) UIWindow *window;
@property(nonatomic, strong) void (^registrationHandler)
(NSString *registrationToken, NSError *error);
@property(nonatomic, assign) BOOL connectedToGCM;
@property(nonatomic, strong) NSString* registrationToken;
@property(nonatomic, assign) BOOL subscribedToTopic;
@property(nonatomic, readonly, strong) NSString *registrationKey;
@property(nonatomic, readonly, strong) NSString *messageKey;
@property(nonatomic, readonly, strong) NSString *gcmSenderID;
@property(nonatomic, readonly, strong) NSDictionary *registrationOptions;
@property (strong, nonatomic) NSMutableArray *downloadMaps;
@property (strong, nonatomic) NSString *idMap;
@end
