//
//  OverFlowMenuViewController.m
//  AnyMap
//
//  Created by Antonio M on 23/10/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import "OverFlowMenuViewController.h"

@interface OverFlowMenuViewController ()

@end

@implementation OverFlowMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeView:)];
    [tapGesture setNumberOfTapsRequired:1];
    [tapGesture setNumberOfTouchesRequired:1];
    [self.tapGesture addGestureRecognizer:tapGesture];
}
- (void)closeView:(UITapGestureRecognizer *)sender
{
    NSLog(@"close the view");
    [UIView animateWithDuration:0.3f animations:^{
        self.view.alpha = 0.0f;
    } completion:^(BOOL finished)
     {
         [self.view removeFromSuperview];
     }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)edit:(id)sender {
    [self.delegateOverFlow showEdit];
}
- (IBAction)about:(id)sender {
    [self.delegateOverFlow aboutAM];
}
- (IBAction)rate:(id)sender {
    NSLog(@"rate the app");
    [self.delegateOverFlow rateTheApp];
}
- (IBAction)feedback:(id)sender {
    [self.delegateOverFlow feedback];
}
- (IBAction)settings:(id)sender {
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
