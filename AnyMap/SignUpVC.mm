/*-*-objc-*-*/
//  SignUpVC.m
//  AnyMap
//
//  Created by Antonio M on 15/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//


#import "TermOfUseViewController.h"
#ifndef NO_FB
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#endif
#ifndef NO_GP
#import <GooglePlus/GooglePlus.h>
#import <GoogleOpenSource/GoogleOpenSource.h>
#endif
#import "AppDelegate.h"
#import "SignUpVC.h"
#import "NetworkManager.h"
#import "MBProgressHUD.h"
#import "RootNavTabBarViewController.h"

#ifdef NO_GP
@interface SignUpVC ()
#else
@interface SignUpVC ()<GPPSignInDelegate>
#endif
@property(nonatomic,weak)NSString *gender;
@property (nonatomic) CGFloat screenHeight;
@property(strong,nonatomic)TermOfUseViewController *termOfUse;
@end

@implementation SignUpVC{
#ifndef NO_GP
    GPPSignIn *signIn;
#endif
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.username.delegate = self;
    self.password.delegate = self;
    self.email.delegate = self;
    self.screenHeight =   [UIScreen mainScreen].bounds.size.height;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)back:(id)sender {
    NSLog(@"nope");
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)waitScreen
{
    self.spinner = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.spinner.center = CGPointMake(self.view.bounds.size.width * 0.5, self.view.bounds.size.height * 0.5);
    [self.view addSubview:self.spinner];
    [self.spinner startAnimating];
    self.overlayImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"overlay_shade"]];
    self.overlayImageView.frame =CGRectMake(0.0f, 0.0f, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:self.overlayImageView];
    self.overlayImageView.alpha = 0.0f;
    [UIView animateWithDuration:0.25f
                     animations:^ {
                         self.overlayImageView.alpha = 1.0f;
                     }
                     completion:^(BOOL finished) {
                     }];
}


- (IBAction)signFacebook:(id)sender {
#ifndef NO_FB
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"public_profile",@"email"] fromViewController:self.navigationController handler:
     ^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error");
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
         } else {
             NSLog(@"Logged in");
             [self waitScreen];
             if ([result.grantedPermissions containsObject:@"email"])
             {
                 if ([FBSDKAccessToken currentAccessToken])
                 {
                     [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, link, first_name, last_name, picture.type(large), email, birthday, bio ,location ,friends ,hometown , friendlists"}]
                      startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                          
                          if (!error) {
                              NSString *facebookID = result[@"id"];
                              NSString *email = result[@"email"];
                              if (!email)
                                  email = @"";
                              NSString *gender = result[@"gender"];
                              NSString *name  =result[@"name"];
                              NSString *token = [FBSDKAccessToken currentAccessToken].tokenString;
                              if ([gender isEqualToString:@"female"]) {
                                  gender = @"f";
                              }
                              else
                              {
                                  gender =@"m";
                              }
                              __block BOOL loginSuccessed ;
                              NetworkManager *network =[[NetworkManager alloc]init];
                              MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                              hud.mode = MBProgressHUDModeText;
                              hud.labelText = @"Loading";
                              dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
                                  loginSuccessed  =  [network signUpByFacebook:email facebookID:facebookID username:name tokenFB:token];
                                  dispatch_async(dispatch_get_main_queue(), ^{
                                      if (loginSuccessed) {
                                          UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                          RootNavTabBarViewController *rootTabBar =  [main instantiateViewControllerWithIdentifier:@"ROOTTABBAR"];
                                          [self presentViewController:rootTabBar animated:YES completion:nil];
                                      }
                                      else
                                      {
                                        UIAlertView *alert= [[UIAlertView  alloc ]initWithTitle:@"Information" message:@"Sign up failed " delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
                                          [alert show];
                                          [self.spinner stopAnimating];
                                          self.overlayImageView.alpha = 0.0;
                                          [self.overlayImageView removeFromSuperview];
                                      }
                                      
                                      [MBProgressHUD hideHUDForView:self.view animated:YES];
                                  });
                              });

                              
                       
                          }
                      }];
                     
                     
                 }
             }
         }
     }];

#endif
}
- (IBAction)signGoogle:(id)sender {
    NSLog(@"login with google");
#ifndef NO_GP
    signIn = [GPPSignIn sharedInstance];
    signIn.shouldFetchGooglePlusUser = YES;
    signIn.clientID = @"349495834376-hmu05chuk6911pbtiv3g808s8hefupjr.apps.googleusercontent.com";
    signIn.scopes = @[ kGTLAuthScopePlusLogin ];
    signIn.delegate = self;
    signIn.shouldFetchGoogleUserEmail = YES;
    if ([signIn authentication]) {
        
    }
    else
    {
        [signIn authenticate];
    }
#endif
}
- (IBAction)female:(id)sender {
    if (self.female.touchInside) {
        self.gender =@"f";
    }
}
- (IBAction)male:(id)sender {
    if (self.male.touchInside) {
        self.gender = @"m";
    }
}
- (IBAction)signUp:(id)sender {
    if ([self.username.text isEqualToString:@""] || [self.email.text isEqualToString:@""] || [self.password.text isEqualToString:@""]) {
        UIAlertView *alert= [[UIAlertView alloc ]initWithTitle:@"Information" message:@"Fields can't be empty" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if (![self validEmail:self.email.text])
    {
        UIAlertView *alert= [[UIAlertView alloc ]initWithTitle:@"Information" message:@"The email format is wrong " delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alert show];

    }
    else
    {
        __block BOOL loginSuccessed ;
        NetworkManager *network =[[NetworkManager alloc]init];
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = @"Loading";
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            loginSuccessed  =  [network signUpByAM:self.email.text password:self.password.text username:self.username.text];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (loginSuccessed) {
                    UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    RootNavTabBarViewController *rootTabBar =  [main instantiateViewControllerWithIdentifier:@"ROOTTABBAR"];
                    [self presentViewController:rootTabBar animated:YES completion:nil];
                }
                else
                {
                    UIAlertView *alert= [[UIAlertView alloc ]initWithTitle:@"Information" message:@"The email you provided is already registered" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
                    [alert show];
                    [self.spinner stopAnimating];
                    self.overlayImageView.alpha = 0.0;
                    [self.overlayImageView removeFromSuperview];
                }
                
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            });
        });
    }
}
- (IBAction)usernameChange:(id)sender {
    NSLog(@"username change");
    
}

- (BOOL) validEmail:(NSString*) emailString {
    
    if([emailString length]==0){
        return NO;
    }
    
    NSString *regExPattern = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    
    NSLog(@"%lu", (unsigned long)regExMatches);
    if (regExMatches == 0) {
        return NO;
    } else {
        return YES;
    }
}
#pragma UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    textField.placeholder = nil;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.tag ==0 ) {
        textField.placeholder = @"Username";
    }else if (textField.tag ==1)
    {
        textField.placeholder = @"Email";
    }else if (textField.tag ==2)
    {
        textField.placeholder =@"Password";
    }
    
    
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)termOfUse:(id)sender {
    NSLog(@"term of use");
    self.termOfUse = [[TermOfUseViewController alloc]initWithNibName:@"TermsOfUseView" bundle:nil];
    self.termOfUse.view.frame = CGRectMake(0.0f,0.0f, self.view.frame.size.width, self.view.frame.size.height);
    self.termOfUse.view.alpha = 0.0f;

    [self.view insertSubview:self.termOfUse.view aboveSubview:self.view];
    [UIView animateWithDuration:0.2f animations:^{
        self.termOfUse.view.alpha = 1.0f;
    }completion:^(BOOL finished){

    }];
}
#ifndef NO_GP
#pragma googlePlus
- (void)finishedWithAuth: (GTMOAuth2Authentication *)auth
                   error: (NSError *) error {
//    NSLog(@"Received error %@ and auth object %@",error, auth);
//    if (!error) {
//        GTLQueryPlus *query = [GTLQueryPlus queryForPeopleGetWithUserId:@"me"];
//        
//        NSLog(@"email %@ ", [NSString stringWithFormat:@"Email: %s",signIn.authentication.userEmail.UTF8String]);
//        NSLog(@"Received error %@ and auth object %@",error, auth);
//        
//        // 1. Create a |GTLServicePlus| instance to send a request to Google+.
//        GTLServicePlus* plusService = [[GTLServicePlus alloc] init] ;
//        plusService.retryEnabled = YES;
//        
//        // 2. Set a valid |GTMOAuth2Authentication| object as the authorizer.
//        [plusService setAuthorizer:[GPPSignIn sharedInstance].authentication];
//        
//        // 3. Use the "v1" version of the Google+ API.*
//        plusService.apiVersion = @"v1";
//        [plusService executeQuery:query
//                completionHandler:^(GTLServiceTicket *ticket,
//                                    GTLPlusPerson *person,
//                                    NSError *error) {
//                    if (error) {
//                        //Handle Error
//                    } else {
//                        NSArray * userEmails = person.emails;
//                        NSString * email = ((GTLPlusPersonEmailsItem *)[userEmails objectAtIndex:0]).value;
//                        NSLog(@"Email= %s", email.UTF8String);
//                        NSLog(@"GoogleID=%@", person.identifier);
//                        NSLog(@"User Name=%@", [person.name.givenName stringByAppendingFormat:@" %@", person.name.familyName]);
//                        NSLog(@"Gender=%@", person.gender);
//                        NSString *gender;
//                        if ([person.gender isEqualToString:@"male"]) {
//                            gender = @"m";
//                        }
//                        else
//                        {
//                            gender = @"f";
//                        }
//                        
//                        auto user = ((AppDelegate *)[UIApplication sharedApplication].delegate).currentUser;
//                        auto is_signed_up = user->sign_up_google(string([person.name.givenName stringByAppendingFormat:@" %@", person.name.familyName].UTF8String),
//                                                                 string(email.UTF8String),
//                                                                 string(auth.accessToken.UTF8String));
//                        if (is_signed_up)
//                            [self performSegueWithIdentifier:@"exploreMenu" sender:self];
//                        else
//                        {
//                            UIAlertView *alert= [[UIAlertView alloc ]initWithTitle:@"Information" message:@"Sign up failed " delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
//                            [alert show];
//                        }
//                    }
//                }];
//
//    }
//    else
//    {
//        UIAlertView *alert= [[UIAlertView alloc ]initWithTitle:@"Information" message:@"Sign up failed " delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
//        [alert show];
//
//    }
}
#endif
@end
