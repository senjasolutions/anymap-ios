

//
//  OtherCreatorViewController.m
//  AnyMap
//
//  Created by Antonio M on 09/10/15.
//  Copyright © 2015 AnyMap. All rights reserved.

#import "AppDelegate.h"

#import "AddedPinsOtherCreatorViewController.h"
#import "OtherCreatorViewController.h"
#import "MapOtherCreatorViewController.h"
#import "NetworkManager.h"
#import "LoginVC.h"
@interface OtherCreatorViewController ()
@property(nonatomic)BOOL conditionVC;
@end

@implementation OtherCreatorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.profileName.text = self.profile;
    self.countFollower.text = self.counterFollowers;
    self.countFollowing.text = self.counterFollowings;
    [self.avatarProfile sd_setImageWithURL:[NSURL URLWithString:self.picURL] placeholderImage:[UIImage imageNamed:@"placeholder_pic_big"] ];
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"followUser"])
    {
        
        NSMutableArray *category = [[NSUserDefaults standardUserDefaults]objectForKey:@"followUser"];
        for (int i = 0; i<[category count]; i++)
        {
            NSString * user = [category objectAtIndex:i];
            if ([user isEqualToString:self.identifier])
            {
                self.forFollow.selected = YES;
                break;
            }
        }
    }
    [self initDefaultClass];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
//    if ([segue.destinationViewController isMemberOfClass:[BookmarkedMapsTableViewController class]]) {
//        auto bookmarkedMapsViewController = (BookmarkedMapsTableViewController *)segue.destinationViewController;
//        if (!bookmarkedMapsViewController.referenceMaps)
//            if (!self.bookmarkedMaps)
//                self.bookmarkedMaps = new Value();
//            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//                ((AppDelegate *)[UIApplication sharedApplication].delegate).currentUser->obtain_followed_maps(*self.bookmarkedMaps, std::string(self.identifier.UTF8String));
//                bookmarkedMapsViewController.referenceMaps = self.bookmarkedMaps;
//                for (auto const map : *self.bookmarkedMaps)
//                    bookmarkedMapsViewController.bookmarkedMaps->append(map);
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [bookmarkedMapsViewController.tableView reloadData];
//                });
//            });
//    }
    
    if ([segue.destinationViewController isMemberOfClass:[MapOtherCreatorViewController class]]) {
        MapOtherCreatorViewController *mapOther =(MapOtherCreatorViewController *)[segue destinationViewController];
        mapOther.mapID = self.mapID;
        mapOther.creator_id = self.identifier;
    
    }
    else if ([segue.destinationViewController isMemberOfClass:[AddedPinsOtherCreatorViewController class]])
    {
        AddedPinsOtherCreatorViewController *addPins = (AddedPinsOtherCreatorViewController *)[segue destinationViewController];
        addPins.creator_id = self.identifier;
    }
}

- (void)dealloc
{

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backNav:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)follow:(UIButton *)sender {
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"hasSignIn"])
    {
        sender.selected =!sender.selected;
        NetworkManager *network = [[NetworkManager alloc]init];
        
        if (sender.selected) {
            NSString *followUser = [NSString stringWithFormat:@"%@|0",self.identifier];
            [network followUser:followUser  statusUser:self.identifier];
             NSInteger countFollower =  [self.counterFollowers integerValue] + 1;
            self.countFollower.text = [NSString stringWithFormat:@"%ld",(long)countFollower];
        }
        else
        {
            NSString *unfollowuser = [NSString stringWithFormat:@"%@|1",self.identifier];
            [network unFollowUser:unfollowuser statusUser:self.identifier];
            NSInteger countFollower =  [self.counterFollowers integerValue] ;
            self.countFollower.text = [NSString stringWithFormat:@"%ld",(long)countFollower];
        }
    }
    else
    {
        [self infoNeedSignInToFollow];
    }
}


- (void)infoNeedSignInToFollow
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Information"
                                  message:@"Please signup or login to follow this user"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* no = [UIAlertAction
                         actionWithTitle:@"Cancel"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                         }];
    
    
    UIAlertAction*  yes = [UIAlertAction
                           actionWithTitle:@"Login/Signup"
                           style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * action)
                           {
                               
                               UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                               LoginVC *login = [main instantiateViewControllerWithIdentifier:@"LOGIN"];
                               [self presentViewController:login animated:YES completion:nil];
                           }];
    
    
    [alert addAction:no];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}
- (void)initDefaultClass
{
    ((AppDelegate *)[UIApplication sharedApplication].delegate).idMap = nil;
    ((AppDelegate *)[UIApplication sharedApplication].delegate).idMap = self.identifier;
    UIViewController *currentVC = [self.childViewControllers objectAtIndex:0];
    if ([currentVC isKindOfClass:[MapOtherCreatorViewController class]]) {
        if (!self.conditionVC) {
            AddedPinsOtherCreatorViewController *addVC = [[UIStoryboard storyboardWithName:@"AddedPinsOtherCreator" bundle:nil]instantiateViewControllerWithIdentifier:@"AddedPinsOtherCreatorViewController"];
            [self addChildViewController:addVC];
            [currentVC didMoveToParentViewController:self];
            NSLog(@"nilai : %@", ((AppDelegate *)[UIApplication sharedApplication].delegate).idMap);
            addVC.view.frame = self.containerView.bounds;
            [self.containerView insertSubview:addVC.view belowSubview:currentVC.view];
        }
    }
    
}
- (IBAction)changeSegments:(id)sender {
    
    MapOtherCreatorViewController *mapOther = (MapOtherCreatorViewController *)[self.childViewControllers objectAtIndex:0];
    AddedPinsOtherCreatorViewController *addPins = (AddedPinsOtherCreatorViewController *)[self.childViewControllers objectAtIndex:1];
    if (self.segmentControl.selectedSegmentIndex == 0) {
        self.conditionVC = YES;
        [mapOther didMoveToParentViewController:self];
        [self.containerView addSubview:mapOther.view];
    }
    else if (self.segmentControl.selectedSegmentIndex == 1)
    {
        self.conditionVC = NO;
        addPins.creator_id = self.identifier;
        [addPins didMoveToParentViewController:self];
        [self.containerView addSubview:addPins.view];
    }
}


@end
