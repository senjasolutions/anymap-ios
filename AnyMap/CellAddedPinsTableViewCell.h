//
//  CellAddedPinsTableViewCell.h
//  AnyMap
//
//  Created by Antonio M on 13/11/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 a class to make property pins detail
 */
@interface CellAddedPinsTableViewCell : UITableViewCell
/**
 A property to make  title for the map
 */
@property (strong, nonatomic) IBOutlet UILabel *mapTitle;
/**
 A property to make pin title
 */
@property (strong, nonatomic) IBOutlet UILabel *pinTitle;
/**
 A set an image for other profile
 */
@property (strong, nonatomic) IBOutlet UIButton *closePin;
/**
 A set an image to pin owner
 */
@property (strong, nonatomic) IBOutlet UIImageView *pinOwner;
/**
 A set a text label for pin description
 */
@property (strong, nonatomic) IBOutlet UILabel *pinDescription;
/**
 A property to make action like the pin
 */
@property (strong, nonatomic) IBOutlet UIButton *counterLikesPin;

@end
