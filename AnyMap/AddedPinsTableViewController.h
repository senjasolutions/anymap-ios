//
//  AddedPinsTableViewController.h
//  AnyMap
//
//  Created by Antonio M on 09/11/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddedPinsTableViewController : UITableViewController
@property (nonatomic,strong)NSMutableArray *arrayAddedPins;
@end
