/*-*-objc-*-*/
//  cellPageViewController.m
//  AnyMap
//
//  Created by Antonio M on 30/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//


#import "AppDelegate.h"
#import "cellPageViewController.h"
#import "UIImageView+AFNetworking.h"
#import "carousel.h"
#import <SDWebImage/UIImageView+WebCache.h>



@interface cellPageViewController ()<UIScrollViewDelegate,UIGestureRecognizerDelegate>
@property(nonatomic)NSInteger index;
@property(nonatomic,strong)NSArray *titleLogo;
@property(nonatomic)BOOL drag;
@property(strong,nonatomic)NSMutableArray *arrayPins;
@end

@implementation cellPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];

}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self changeRawData];
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
//    NSLog(@"class this is being called in: %@, in response to this event: %@",[self class], event);
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ( [ gestureRecognizer isKindOfClass:[ UITapGestureRecognizer class ] ] )
    {
        return NO;
    }
    return YES;
}
-(void)changeRawData
{
    self.index = 0;
    NSMutableArray *arrayMaps = [[NSMutableArray alloc]init];
    arrayMaps = ((AppDelegate *)[UIApplication sharedApplication].delegate).downloadMaps;
//    NSLog(@" nilai map pada pins na  : %@",arrayMaps);
    
    self.arrayPins = [[NSMutableArray alloc]init];
    for (int i =0 ; i<[arrayMaps count]; i++) {
        
        NSString *pins = [arrayMaps objectAtIndex:i];
        NSMutableArray *arraytempPins = [pins valueForKey:@"pins"];
        
        for (int i = 0; i<[arraytempPins count]; i++) {
            NSString *pin = [arraytempPins objectAtIndex:i];
            if ([self.arrayPins count] > 4 ) {
                break;
            }
            else
            {
                [self.arrayPins addObject:pin];
            }
        }
        
    }
    
    if ([self.arrayPins count] != 0 || !self.arrayPins) {
        self.pageControl.numberOfPages = [self.arrayPins count];
        
//        NSLog(@"nilai pin array : %@",self.arrayPins);
        NSString *pin =[self.arrayPins objectAtIndex:0];
        
//        NSLog(@"nilai pin : %lu",(unsigned long)[self.arrayPins count]);
        UIPageViewController *pageVC = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
        pageVC.dataSource = self;
        pageVC.delegate = self;
        carousel *carousel = [[UIStoryboard storyboardWithName:@"Carousel" bundle:nil] instantiateViewControllerWithIdentifier:@"carouselPage"];
        carousel.pageIndex = self.index;
        carousel.header = [pin valueForKey:@"title"];
        carousel.idPin = [pin valueForKey:@"id"];
        [pageVC setViewControllers:@[carousel] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        [self addChildViewController:pageVC];
        [self.view insertSubview:pageVC.view atIndex:0];
        pageVC.view.frame = self.view.bounds;
        [pageVC didMoveToParentViewController:self];
        self.view.gestureRecognizers = pageVC.gestureRecognizers;
        for (UIGestureRecognizer * gesRecog in pageVC.gestureRecognizers)
        {
            gesRecog.delegate = self;
        }
        for (UIView *view in pageVC.view.subviews ) {
            if ([view isKindOfClass:[UIScrollView class]]) {
                UIScrollView *scroll = (UIScrollView *)view;
                scroll.delegate = self;
            }
        }
        self.drag = NO;
        
        if ([pin valueForKey:@"image"] == (id)[NSNull null]) {
            carousel.background.image = [UIImage imageNamed:@""];
        }
        else
        {
            [carousel.background sd_setImageWithURL:[NSURL URLWithString:[pin valueForKey:@"image"]] placeholderImage:[UIImage imageNamed:@""] ];
        }

    }
    
}

- (void)dealloc
{
   
}

#pragma disabledBouncePageVC
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    self.drag = YES;
//    NSLog(@"dragged");
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (!self.drag) {
        return;
    }
    if (self.index  < 0 && scrollView.contentOffset.x <= scrollView.bounds.size.width) {
//        NSLog(@"dragabel");
        scrollView.contentOffset = CGPointMake(scrollView.bounds.size.width, 0);
    }
    if (self.index  > [self.arrayPins count] && scrollView.contentOffset.x > scrollView.bounds.size.width) {
        scrollView.contentOffset = CGPointMake(scrollView.bounds.size.width, 0);
    }
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset{
    
    if (!self.drag) {
        return;
    }
    if (self.index  < 0 && scrollView.contentOffset.x <= scrollView.bounds.size.width) {
//        NSLog(@"tester");
        *targetContentOffset = CGPointMake(scrollView.bounds.size.width, 0);
    }
    if (self.index  > [self.arrayPins count] && scrollView.contentOffset.x >= scrollView.bounds.size.width) {
        *targetContentOffset = CGPointMake(scrollView.bounds.size.width, 0);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    /*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    if ([viewController isKindOfClass:[carousel class]]) {
        NSUInteger index = ((carousel *)viewController).pageIndex;
        if (index == 0) {
            return nil;
        }
//        NSLog(@"nilai index before : %lu", (unsigned long)index);
        index--;
        return [self viewControllerAtIndex:index];
    }
    else
    {
        return nil;
    }


}
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    
    if ([viewController isKindOfClass:[carousel class]])
    {
        NSUInteger index = ((carousel *)viewController).pageIndex;
        if (index == [self.arrayPins count]) {
            return nil;
        }
//        NSLog(@"nilai index : %lu", (unsigned long)index);
        index++;
        return [self viewControllerAtIndex:index];
    }
    else
        return  nil;

}
- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return self.arrayPins.count;
}
- (carousel *)viewControllerAtIndex:(NSUInteger)index
{
    if (index == [self.arrayPins count]) {
        return nil;
    }
    NSString *pin =[self.arrayPins objectAtIndex:index];
//    NSLog(@"nilai pin : %@",pin);
    carousel  *vc = [[UIStoryboard storyboardWithName:@"Carousel" bundle:nil] instantiateViewControllerWithIdentifier:@"carouselPage"];
    vc.pageIndex = index;
    self.index = index;
    vc.header = [pin valueForKey:@"title"];
    vc.idPin = [pin valueForKey:@"id"];
    self.index = index;
    return vc;
}
- (void)pageViewController:(UIPageViewController *)viewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
    if (!completed){return;}
    UIViewController *currentViewController = viewController.viewControllers[0];
    if ([currentViewController isKindOfClass:[carousel class]]) {
        carousel *vc = (carousel *)currentViewController;
        NSString *pin =[self.arrayPins objectAtIndex:vc.pageIndex];
//        NSLog(@"nilai page index :  %ld", (long)vc.pageIndex);
        if ([pin valueForKey:@"image"] == (id)[NSNull null]) {
            vc.background.image = [UIImage imageNamed:@""];
        }
        else
        {
            [vc.background sd_setImageWithURL:[NSURL URLWithString:[pin valueForKey:@"image"]] placeholderImage:[UIImage imageNamed:@""] ];
        }

        [self.pageControl setCurrentPage:vc.pageIndex];
        }
}
@end
