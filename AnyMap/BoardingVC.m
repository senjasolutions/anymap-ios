//
//  BoardingVC.m
//  AnyMap
//
//  Created by Antonio M on 15/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import "BoardingVC.h"
#import "OnBoardPageVC.h"

@interface BoardingVC ()<UIScrollViewDelegate,UIPageViewControllerDelegate>
@property(nonatomic,strong)NSArray * imageBackground;
@property(nonatomic,strong)NSArray *imageLogo;
@property(nonatomic,strong)NSArray *titleLogo;
@property(nonatomic,strong)NSArray *desc;
@property(nonatomic)NSInteger index;
@property(nonatomic)BOOL drag;
@end

@implementation BoardingVC

- (void)viewDidLoad {
    [super viewDidLoad];
}
- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self rawData];
    
    NSLog(@" nama parent view nya adalah : %@",self.vc);
    self.vc.paging.numberOfPages = 4;
    
   UIPageViewController *pageVC = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    pageVC.dataSource = self;
    OnBoardPageVC *onBoardPage = [[UIStoryboard storyboardWithName:@"Page" bundle:nil] instantiateViewControllerWithIdentifier:@"OnBoardPageVC"];
    self.index = 0;
    onBoardPage.pageIndex = self.index;
    onBoardPage.imageBackground = [UIImage imageNamed:[self.imageBackground objectAtIndex:self.index]];
    onBoardPage.imageLogo = [UIImage imageNamed:[self.imageLogo objectAtIndex:self.index]];
    onBoardPage.titleLabel = [self.titleLogo objectAtIndex:self.index];
    onBoardPage.descLabel =[self.desc objectAtIndex:self.index];
    [pageVC setViewControllers:@[onBoardPage] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    [self addChildViewController:pageVC];
    [self.view insertSubview:pageVC.view atIndex:0];
    pageVC.view.frame = self.view.bounds;
    pageVC.delegate = self;
    [pageVC didMoveToParentViewController:self];
    self.view.gestureRecognizers = pageVC.gestureRecognizers;
    for (UIView *view in pageVC.view.subviews ) {
        if ([view isKindOfClass:[UIScrollView class]]) {
            UIScrollView *scroll = (UIScrollView *)view;
            scroll.delegate = self;
        }
    }
    self.drag = NO;
}
#pragma disabledBouncePageVC
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    self.drag = YES;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (!self.drag) {
        return;
    }
    if (self.index  == 0 && scrollView.contentOffset.x < scrollView.bounds.size.width) {
        scrollView.contentOffset = CGPointMake(scrollView.bounds.size.width, 0);
    }
    if (self.index  == 3 && scrollView.contentOffset.x > scrollView.bounds.size.width) {
        scrollView.contentOffset = CGPointMake(scrollView.bounds.size.width, 0);
    }
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset{
    
    if (!self.drag) {
        return;
    }
    if (self.index  == 0 && scrollView.contentOffset.x <= scrollView.bounds.size.width) {
        *targetContentOffset = CGPointMake(scrollView.bounds.size.width, 0);
    }
    if (self.index  == 3 && scrollView.contentOffset.x >= scrollView.bounds.size.width) {
        *targetContentOffset = CGPointMake(scrollView.bounds.size.width, 0);
    }
}
- (void)rawData
{
    self.imageBackground = [NSArray arrayWithObjects:@"onboarding_1.png",@"onboarding_2.png",@"onboarding_3.png",@"",nil];
    self.imageLogo = [NSArray arrayWithObjects:@"onboarding_icon_1.png",@"onboarding_icon_2.png",@"onboarding_icon_3.png",@"", nil];
    self.titleLogo =[NSArray arrayWithObjects:@"Explore Singapore",@"Uncover Experiences",@"Get Recommendations", @"",nil];
    self.desc =[NSArray arrayWithObjects:@"Over 1000 Locations and Maps",@"Find out what you have been missing",@"Places people find interesting",@"", nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    if ([viewController isKindOfClass:[OnBoardPageVC class]]) {
        NSUInteger index = ((OnBoardPageVC *)viewController).pageIndex;
        if (index == 0) {
            return nil;
        }
        index--;
        return [self viewControllerAtIndex:index];
    }
    else
    {
      return nil;
    }
}
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{

    if ([viewController isKindOfClass:[OnBoardPageVC class]])
    {
        NSUInteger index = ((OnBoardPageVC *)viewController).pageIndex;
        if (index ==3) {
            return nil;
        }
        index++;
        if (index == [self.imageBackground count]) {
            return nil;
        }
        return [self viewControllerAtIndex:index];
    }
    else
        return  nil;
}
- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return 4;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}

- (OnBoardPageVC *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.imageBackground count] == 0) || (index >= [self.imageBackground count])) {
        return nil;
    }
    OnBoardPageVC *vc = [[UIStoryboard storyboardWithName:@"Page" bundle:nil] instantiateViewControllerWithIdentifier:@"OnBoardPageVC"];
    if (index == 3) {
        vc.currentView = YES;
        
    }
    vc.pageIndex = index;
    vc.imageBackground = [UIImage imageNamed:[self.imageBackground objectAtIndex:index]];
    vc.imageLogo = [UIImage imageNamed:[self.imageLogo objectAtIndex:index]];
    vc.titleLabel = [self.titleLogo objectAtIndex:index];
    vc.descLabel =[self.desc objectAtIndex:index];
    return vc;
}
- (void)pageViewController:(UIPageViewController *)viewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
    if (!completed){return;}
    UIViewController *currentViewController = viewController.viewControllers[0];
    if ([currentViewController isKindOfClass:[OnBoardPageVC class]]) {
        OnBoardPageVC *vc = (OnBoardPageVC *)currentViewController;
        NSLog(@"nilai VC : %ld",(long)vc.pageIndex);
        [self.vc.paging setCurrentPage:vc.pageIndex];
        self.index = vc.pageIndex;
        if (vc.pageIndex == 3) {
            self.vc.paging.hidden = YES;
            [UIView animateWithDuration:0.2f animations:^{
                self.vc.footerViewButtons.frame = CGRectMake(0.0f,self.vc.view.frame.size.height + self.vc.footerViewButtons.frame.size.height, self.vc.footerViewButtons.frame.size.width, self.vc.footerViewButtons.frame.size.height);
            }completion:^(BOOL FINISHED){}];
        }
        else if (vc.pageIndex < 3)
        {
        
                NSLog(@"paging hidden");
                self.vc.paging.hidden = NO;
            [UIView animateWithDuration:0.2f animations:^{
                self.vc.footerViewButtons.frame = CGRectMake(0.0f,self.vc.view.frame.size.height  -self.vc.footerViewButtons.frame.size.height, self.vc.footerViewButtons.frame.size.width, self.vc.footerViewButtons.frame.size.height);
            }completion:^(BOOL FINISHED){}];
   
        }

    }
}
@end
