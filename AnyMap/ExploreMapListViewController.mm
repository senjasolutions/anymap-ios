
/*-*-objc-*-*/
//  ExploreMapListViewController.m
//  AnyMap
//
//  Created by Antonio M on 17/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//


#import "AppDelegate.h"
#import "NetworkManager.h"
#import "ExploreMapListTableViewCell.h"
#import "CarouselTableViewCell.h"
#import "MapDetailLocationViewController.h"
#import "cellPageViewController.h"
#import "PinUpViewController.h"
#import "LoginVC.h"
#import "NetworkCheck.h"
#import "ExploreMapListViewController.h"
#import "OtherCreatorViewController.h"
#import "MBProgressHUD.h"
@interface ExploreMapListViewController ()<UIAlertViewDelegate>
@property (strong,nonatomic)NSMutableArray *arrayMap;
@end

@implementation ExploreMapListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    CGFloat ratio = 0;
    if (screenHeight == 480.0f)
    {
        ratio = 178;
    }
    else if (screenHeight == 568.0f)
    {
        ratio = 178;
    }
    else if (screenHeight == 667.0f)
    {
        ratio =178;
    }
    else if (screenHeight == 736.0f)
    {
        ratio = 178;
    }
    
    self.tableView.rowHeight = ratio;
    [self.tableView setContentInset:UIEdgeInsetsMake(0.0, 0.0, 9.0, 0.0)];
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor whiteColor];
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self
                            action:@selector(refresh:)
                  forControlEvents:UIControlEventValueChanged];
    [self loadFirstMaps];
    [self location];
}
- (void)refresh:(UIRefreshControl *)sender
{
if ([NetworkCheck instance].internetStatus != NotReachable) {
    UIViewController *vc = (UIViewController *)self.parentViewController;
    ExploreMapViewController *exploreMap= (ExploreMapViewController *)[vc.childViewControllers objectAtIndex:0];
    mapDetailCollectionViewController *mapDetailCollection =(mapDetailCollectionViewController *)[exploreMap.childViewControllers objectAtIndex:0];
    // End the refreshing
    if (self.refreshControl) {
        [self.refreshControl endRefreshing];
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"Loading...";
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            [self updateTheMaps];
            [mapDetailCollection loadFirstMaps];
            [self loadFirstMaps];
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hide:YES];
            });
        });
    }
}
    else
    {
        [self.refreshControl endRefreshing];
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Information" message:@"Check your internet connection" delegate:self cancelButtonTitle:nil otherButtonTitles:@"ok", nil];
        [alert show];
        
    }
    
}

- (void)updateTheMaps
{
    NetworkManager *network = [[NetworkManager alloc]init];
    auto coordinate = self.locationManager.location.coordinate;
    double latitude = coordinate.latitude;
    double longitude = coordinate.longitude;
    NSLog(@"location update the maps:  %f : %f ",longitude,latitude);
    ((AppDelegate *)[UIApplication sharedApplication].delegate).downloadMaps = nil;
    ((AppDelegate *)[UIApplication sharedApplication].delegate).downloadMaps = [network maps_by_response:[NSString stringWithFormat:@"%f",latitude] longitude:[NSString stringWithFormat:@"%f",longitude]];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}
- (void)location{
        self.locationManager = [CLLocationManager new];
        self.locationManager.delegate = self;
        self.locationManager.distanceFilter = kCLDistanceFilterNone;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        //------
        if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [self.locationManager requestWhenInUseAuthorization];
        }
        else{
            [self.locationManager startUpdatingLocation];
            
        }
}
#pragma locationManager
-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
        {
            // do some error handling
        }
            break;
        default:{
            [manager startUpdatingLocation];
        
        }
            break;
    }
}

- (void)loadFirstMaps
{
     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
     hud.labelText = @"Loading...";
     dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
         self.arrayMap = nil;
         self.arrayMap =  ((AppDelegate *)[UIApplication sharedApplication].delegate).downloadMaps ;
         NSLog(@"array map list adalah : %@ ",self.arrayMap);

            dispatch_async(dispatch_get_main_queue(), ^{
                         [self.tableView reloadData];
                if ([self.arrayMap count] == 0) {
                    
                }
                else{
                [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
                }
                [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
    });
    

}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    if ([self.arrayMap count] == 0) {
        
    }
    else{
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    
}
- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    if (self.arrayMap.count == 0) {
        // Display a message when the table is empty
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        
        messageLabel.text = @"No Maps. Please pull down to refresh.";
        messageLabel.textColor = [UIColor blackColor];
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.font = [UIFont fontWithName:@"Roboto-Italic" size:10];
        [messageLabel sizeToFit];
        
        self.tableView.backgroundView = messageLabel;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        return 0;
    }
    else
    {
         return 1;
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayMap.count +1;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MapDetailLocationViewController *mapDetailLocation = [storyboard instantiateViewControllerWithIdentifier:@"MapDetailLocation"];
    NSString *value = [self.arrayMap objectAtIndex:indexPath.row -1];
    mapDetailLocation.titleLocation = [value valueForKey:@"title"];
    mapDetailLocation.pinsArray = [value valueForKey:@"pins"];
    mapDetailLocation.mapID = [value valueForKey:@"id"];
    mapDetailLocation.indexRow = indexPath.row -1;
    mapDetailLocation.isFromBookmarked = NO;
    [self presentViewController:mapDetailLocation animated:YES completion:nil];
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            return CGFloat(240);
        }
    }
    return 178;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        CarouselTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"carousel"];
        if (!cell) {
            
            [tableView registerNib:[UINib nibWithNibName:@"CarouselCell" bundle:nil] forCellReuseIdentifier:@"carousel"];
            cell = [tableView dequeueReusableCellWithIdentifier:@"carousel" forIndexPath:indexPath];
            
        }
        cellPageViewController *cellPage = [[cellPageViewController alloc]initWithNibName:@"CarouselPageView" bundle:nil];
        cellPage.view.frame = cell.contentView.bounds;
        [self addChildViewController:cellPage];
        [cell.contentView addSubview:cellPage.view];
        [cellPage didMoveToParentViewController:self];
        
        return cell;
    }
    else
    {
        ExploreMapListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"exploreMapList"];
        if (!cell) {
            NSLog(@"add the cell");
            [tableView registerNib:[UINib nibWithNibName:@"Clist" bundle:nil] forCellReuseIdentifier:@"exploreMapList"];
            cell = [tableView dequeueReusableCellWithIdentifier:@"exploreMapList" forIndexPath:indexPath];
            
        }
        NSString *value = [self.arrayMap objectAtIndex:indexPath.row-1];
         NSUserDefaults *standar = [[NSUserDefaults alloc]init];
        NSMutableArray *arrayBookmarkedMaps  = [standar valueForKey:@"bookmarkedMaps"];
        cell.bookmark.selected = NO;
        for (int i = 0; i < [arrayBookmarkedMaps count]; i++) {
            if ([[arrayBookmarkedMaps objectAtIndex:i] containsObject:[value valueForKey:@"id"]]) {
                cell.bookmark.selected = YES;
                break;
            }
        }
        cell.bookmark.tag = indexPath.row -1;
        cell.creatorButton.tag = indexPath.row -1;
        [cell.bookmark addTarget:self action:@selector(addBookmark:) forControlEvents:UIControlEventTouchUpInside];
        [cell.creatorButton addTarget:self action:@selector(otherCreator:) forControlEvents:UIControlEventTouchUpInside];
        cell.headerMap.text = [value valueForKey:@"title"];
        cell.counterLocation.text = [NSString stringWithFormat:@"%@ Locations ", [value valueForKey:@"no_of_pins"]];
        
        if ([value valueForKey:@"description"] == (id)[NSNull null]) {
            cell.descriptionMap.text = @"";
        }else{
            cell.descriptionMap.text = [value valueForKey:@"description"];
        }
        [cell.creatorName setTitle: [value valueForKey:@"profile_name"] forState:UIControlStateNormal];
        if ([value valueForKey:@"profile_image"] == (id)[NSNull null]) {
            cell.creatorPhoto.image = [UIImage imageNamed:@""];
        }
        else
        {
            [cell.creatorPhoto sd_setImageWithURL:[NSURL URLWithString:[value valueForKey:@"profile_image"]] placeholderImage:[UIImage imageNamed:@""] ];
        }
        
        if ([value valueForKey:@"image"] == (id)[NSNull null]) {
            cell.map.image = [UIImage imageNamed:@""];
        }
        else
        {
            [cell.map sd_setImageWithURL:[NSURL URLWithString:[value valueForKey:@"image"]] placeholderImage:[UIImage imageNamed:@""] ];
        }
        
        return cell;
    }
}

-(IBAction)addBookmark:(UIButton*) sender{
    NSString *mapID= [self.arrayMap objectAtIndex:sender.tag];
    NSMutableArray *mapTemp =[self.arrayMap objectAtIndex:sender.tag];
    NSLog(@"map id: %@",[mapID valueForKey:@"id"]);
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"hasSignIn"])
    {
        sender.selected =!sender.selected;
        NetworkManager *network = [[NetworkManager alloc]init];
        
        if (sender.selected) {
            NSLog(@"bookmark the map");
            NSString *bookmarkMap = [NSString stringWithFormat:@"%@|0",[mapID valueForKey:@"id"]];
            [network bookmarkMap:bookmarkMap  statusMap:[mapID valueForKey:@"id"] arrayMap:mapTemp];
        }
        else
        {
            NSLog(@"unbookmark the map");
            NSString *unbookmarkmap = [NSString stringWithFormat:@"%@|1",[mapID valueForKey:@"id"]];
            [network unBookmarkMap:unbookmarkmap statusMap:[mapID valueForKey:@"id"] arrayMap:mapTemp];
        }
    }
    else
    {
        [self infoNeedSignInToBookmark];
    }
}

- (void)saveLocally :(UIButton *)sender statusBookmark:(NSString *)mapID
{

    
    NSMutableArray *map=  [self.arrayMap objectAtIndex:sender.tag];
    
    NSMutableArray *arrayPins =  [map valueForKey:@"pins"];
    
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[map valueForKey:@"category_id"],[map valueForKey:@"created_at"],[map valueForKey:@"created_by_id"],[map valueForKey:@"description"],[map valueForKey:@"id"],[map valueForKey:@"image"],[map valueForKey:@"is_featured"],[map valueForKey:@"is_following"],[map valueForKey:@"is_private"],[map valueForKey:@"no_of_followers"],[map valueForKey:@"no_of_pins"],[map valueForKey:@"profile_followers"],[map valueForKey:@"profile_following"],[map valueForKey:@"profile_image"],[map valueForKey:@"profile_name"],[map valueForKey:@"score"],[map valueForKey:@"title"],nil] forKeys:[NSArray arrayWithObjects:@"category_id",@"created_at",@"created_by_id",@"description",@"id",@"image",@"is_featured",@"is_following",@"is_private",@"no_of_followers",@"no_of_pins",@"profile_followers",@"profile_following",@"profile_image",@"profile_name",@"score",@"title",nil]];
    
//    
//    NSUserDefaults *pref =[[NSUserDefaults alloc]init];
//    
//    [pref setObject:arrayPins forKey:@"bookMap"];
//    [pref synchronize];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *arr = [arrayPins mutableCopy];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:arr];
    [defaults setObject:dict forKey:@"bookMap"];
    [defaults setObject:data forKey:[map valueForKey:@"id"]];
    [defaults synchronize];
    
    
    NSData *dataTwp = [defaults objectForKey:[map valueForKey:@"id"]];
    NSArray *arrloadTheData = [NSKeyedUnarchiver unarchiveObjectWithData:dataTwp];

    NSLog(@"look the data : %@  :%@",[defaults objectForKey:@"bookMap"],arrloadTheData);
    
}

- (void)infoNeedSignInToBookmark
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Information"
                                  message:@"Please signup or login to bookmark this map"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* no = [UIAlertAction
                         actionWithTitle:@"Cancel"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                         }];
    
    
    UIAlertAction*  yes = [UIAlertAction
                         actionWithTitle:@"Login/Signup"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                        
                             UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                             LoginVC *login = [main instantiateViewControllerWithIdentifier:@"LOGIN"];
                             [self presentViewController:login animated:YES completion:nil];
                         }];
    
    
    [alert addAction:no];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        NSLog(@"nilai parent VC nya adalah : %@",self.parentViewController);
        [self.parentViewController performSegueWithIdentifier:@"signup" sender: self];
    }
}
-(IBAction)otherCreator:(UIButton*) sender{
    NSLog(@"Row Creator %ld",  (long)sender.tag);
      NSString *value = [self.arrayMap objectAtIndex:sender.tag];

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"OuterProfile" bundle:nil];
    OtherCreatorViewController *otherCreator= [storyboard instantiateViewControllerWithIdentifier:@"OtherCreatorViewController"];

    otherCreator.profile = [value valueForKey:@"profile_name"];
    otherCreator.counterFollowers = [value valueForKey:@"profile_followers"];
    otherCreator.counterFollowings = [value valueForKey:@"profile_following"];
    otherCreator.indexUser = sender.tag;
    otherCreator.describes= [value valueForKey:@"title"];
    otherCreator.identifier = [value valueForKey:@"created_by_id"];
    otherCreator.picURL = [value valueForKey:@"profile_image"];
    otherCreator.mapID = [value valueForKey:@"id"];
    [self.navigationController presentViewController:otherCreator animated:YES completion:nil];
}

@end
