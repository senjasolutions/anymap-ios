#ifdef DEBUG
#include <stdio.h>
#endif
#include <stdlib.h>
#include <string.h>
#import "AMImageManager.h"

size_t AMSaveImage(char const *dl_img, size_t size, size_t nmemb, AMImageData *mem_img)
{
    size_t real_size = size * nmemb;
    mem_img->memory = (char *)realloc(mem_img->memory, mem_img->size + real_size + 1);
    if (mem_img->memory == NULL) {
#ifdef DEBUG
        printf("not enough memory (realloc returned null)\n");
#endif
        return 0;
    }
    memcpy(&(mem_img->memory[mem_img->size]), dl_img, real_size);
    mem_img->size += real_size;
    mem_img->memory[mem_img->size] = 0;
    return real_size;
}

static AMImageManager *sharedManager = nil;

@interface AMImageManager ()
@property (strong, nonatomic) NSMutableDictionary *images;
@end

@implementation AMImageManager

- (instancetype)init
{
    if ((self = [super init]))
        self.images = [NSMutableDictionary dictionary];

    return self;
}

+ (AMImageManager *)sharedManager
{
    return sharedManager ? sharedManager : (sharedManager = [AMImageManager new]);
}

- (void)setImage:(UIImage *)image forURL:(NSURL *)url
{
    self.images[url] = image;
}

- (UIImage *)imageForURL:(NSURL *)url
{
    return self.images[url];
}

@end
