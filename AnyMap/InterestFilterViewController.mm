/*-*-objc-*-*/
//  InterestFilterViewController.m
//  AnyMap
//
//  Created by Antonio M on 05/10/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//
#import "AppDelegate.h"
#import "ExploreMapListViewController.h"
#import "ExploreMapViewController.h"
#import "mapDetailCollectionViewController.h"
#import "InterestFilterViewController.h"
#import "NetworkManager.h"
#import "MBProgressHUD.h"
@interface InterestFilterViewController ()

@end

@implementation InterestFilterViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeView:)];
    [tapGesture setNumberOfTapsRequired:1];
    [tapGesture setNumberOfTouchesRequired:1];
    [self.tapView addGestureRecognizer:tapGesture];
    [self location];
    
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"category"]) {
        
        NSMutableArray *category = [[NSUserDefaults standardUserDefaults]objectForKey:@"category"];
        
        for (int i = 0 ; i < [self.buttons count]; i++)
        {
            UIButton *buttonCategory = [self.buttons objectAtIndex:i];
            for (int j =0 ; j< [category count]; j++)
            {
                NSString *value = [category objectAtIndex:j];
                if (buttonCategory.tag == [value longLongValue]) {
                    [buttonCategory setSelected:YES];
                }
                
            }
        }
        
    }
}
- (void)closeView:(UITapGestureRecognizer *)sender
{
    [self.delegateFilter done];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)filter:(UIButton *)sender {
    sender.selected = !sender.selected;
}
- (IBAction)done:(id)sender
{
    UIViewController *vc = (UIViewController *)self.delegateFilter;
    ExploreMapListViewController *explore= (ExploreMapListViewController *)[vc.childViewControllers objectAtIndex:1];
    ExploreMapViewController *exploreMap= (ExploreMapViewController *)[vc.childViewControllers objectAtIndex:0];
    mapDetailCollectionViewController *mapDetailCollection =(mapDetailCollectionViewController *)[exploreMap.childViewControllers objectAtIndex:0];
    
    NSUserDefaults *pref = [[NSUserDefaults alloc]init];
    NSMutableArray *arrayCategoryTemp = [[NSMutableArray alloc]init];
    for (int i = 0; i <[self.buttons count]; i++) {
        UIButton * button = [self.buttons objectAtIndex:i];
        if (button.selected) {
            NSString *formatCategory = [NSString stringWithFormat:@"%ld",(long)button.tag];
            [arrayCategoryTemp addObject:formatCategory];
        }
    }
    [pref setObject:arrayCategoryTemp forKey:@"category"];
    [pref synchronize];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Loading...";
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [self updateTheMaps];
        [mapDetailCollection loadFirstMaps];
        [explore loadFirstMaps];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegateFilter done];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
    });
}
- (void)location{
    self.locationManager = [CLLocationManager new];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    //------
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    else{
        [self.locationManager startUpdatingLocation];
        
    }
}

- (void)updateTheMaps
{
    NetworkManager *network = [[NetworkManager alloc]init];
    auto coordinate = self.locationManager.location.coordinate;
    double latitude = coordinate.latitude;
    double longitude = coordinate.longitude;
    NSLog(@"location in interest filter longitude:  %f and the latitude: %f ",longitude,latitude);
    ((AppDelegate *)[UIApplication sharedApplication].delegate).downloadMaps = nil;
    ((AppDelegate *)[UIApplication sharedApplication].delegate).downloadMaps = [network maps_by_response:[NSString stringWithFormat:@"%f",latitude] longitude:[NSString stringWithFormat:@"%f",longitude]];

}
#pragma locationManager
-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
        {
            // do some error handling
        }
            break;
        default:{
            [manager startUpdatingLocation];
        }
            break;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
