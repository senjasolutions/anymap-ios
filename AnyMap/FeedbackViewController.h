//
//  FeedbackViewController.h
//  AnyMap
//
//  Created by Antonio M on 17/12/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedbackViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextView *feedbackText;

@property (strong, nonatomic) IBOutlet UIButton *feedbackButton;
@end
