/*-*-objc-*-*/
//  mapDetailCollectionViewController.m
//  AnyMap
//
//  Created by Antonio M on 23/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#ifdef DEBUG
#endif
#import "mapDetailCollectionViewController.h"
#import "CardMapPhotoCollectionViewCell.h"
#import "AMImageManager.h"
#import "AppDelegate.h"
#import "MapDetailLocationViewController.h"
#import "Pin.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MBProgressHUD.h"
#import "NetworkManager.h"
@interface mapDetailCollectionViewController ()
@property (assign, nonatomic) NSUInteger index;
@property (strong,nonatomic)NSMutableArray *markers;
@property(nonatomic)NSInteger indexRow;
@property(nonatomic)NSInteger  indexPin;
@property(strong,nonatomic)NSMutableArray *arrayMaps;
@end
@implementation mapDetailCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self location];
    [self loadFirstMaps];
}
- (void)location{
    self.locationManager = [CLLocationManager new];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    //------
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    else{
        [self.locationManager startUpdatingLocation];
        
    }
}
#pragma locationManager
-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
        {
            // do some error handling
        }
            break;
        default:{
            [manager startUpdatingLocation];
        }
            break;
    }
}

- (void)loadFirstMaps
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Loading...";
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        self.arrayMaps = nil;
        self.arrayMaps =  ((AppDelegate *)[UIApplication sharedApplication].delegate).downloadMaps ;
        NSLog(@"array map list adalah : %@ ",self.arrayMaps);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.collectionView reloadData];
            self.index = 0;
            if ([self.arrayMaps count] == 0) {
                
            }
            else
            {
            [self updatePins:self.exploreMap index:self.index];
            [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:self.index inSection:0] atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
            }
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
    });
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

#pragma mark - UICollectionViewDataSource

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CardMapPhotoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellMap" forIndexPath:indexPath];
    self.indexRow = indexPath.row;
    NSString *value = [self.arrayMaps objectAtIndex:indexPath.row];
    cell.titleHead.text = [value valueForKey:@"title"];
    if ([value valueForKey:@"description"] == (id)[NSNull null]) {
        cell.describeTitle.text = @"";
    }
    else
    {
        cell.describeTitle.text = [value valueForKey:@"description"];
    }
    cell.counterLocation.text =[NSString stringWithFormat:@"%@ Locations", [value valueForKey:@"no_of_pins"]];
    
    if ([value valueForKey:@"image"] == (id)[NSNull null]) {
        cell.photoImage.image = [UIImage imageNamed:@""];
    }
    else
    {
        [cell.photoImage sd_setImageWithURL:[NSURL URLWithString:[value valueForKey:@"image"]] placeholderImage:[UIImage imageNamed:@""] ];
    }
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section
{
    return self.arrayMaps.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    CGFloat ratio = 0;
    if (screenHeight == 480.0f)
    {
        ratio = 302;
    }
    else if (screenHeight == 568.0f)
    {
        ratio = 302;
    }
    else if (screenHeight == 667.0f)
    {
        ratio = 357;
    }
    else if (screenHeight == 736.0f)
    {
        ratio = 396;
    }
    return CGSizeMake(ratio,139);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{

    return UIEdgeInsetsMake(0.0, 9.0, 0.0, 9.0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}
#pragma mark - UIScrollViewDelegate
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    NSInteger count = self.arrayMaps.count;
    if (velocity.x > 0.0f)
    {
        if (self.index < count-1)
        {
            self.index++;
            [self updatePins:self.exploreMap index:self.index];

        }
    }
    else if (velocity.x < 0.0f)
    {
        if (self.index > 0)
        {
            self.index--;
            [self updatePins:self.exploreMap index:self.index];
        }
    }
    
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    CGFloat ratio = 0;
    CGFloat ratioLine = 0;
    if (screenHeight == 480.0f)
    {
        ratio = 302.0f;
    }
    else if (screenHeight == 568.0f)
    {
        ratio = 302.0f;
    }
    else if (screenHeight == 667.0f)
    {
        ratio = 357.0f;
    }
    else if (screenHeight == 736.0f)
    {
        ratio = 396.0f;
    }
//    CGFloat lebarPack = self.collectionView.frame.size.height * ratio;
//    CGFloat lineSpacing = self.collectionView.frame.size.width * ratioLine;
//    NSLog(@"nilai ny adalah : %f",lebarPack+lineSpacing);
//    CGFloat pointOne = (lebarPack + lineSpacing) * self.index;
//    CGFloat inset = (self.collectionView.frame.size.width - self.collectionView.frame.size.height * ratio) / 2.0;
//    CGFloat pointX = pointOne + inset + (0.5 * lebarPack) - (0.5 * self.collectionView.frame.size.width);
      CGFloat pointX = self.index * (ratio + ratioLine);

    

      NSLog(@"%f",pointX);
    *targetContentOffset = CGPointMake(pointX, 0.0f);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"mapDetailLocation" sender:indexPath];
}

-(void)updatePins:(ExploreMapViewController *)vc index:(NSInteger )index
{
    NSLog(@"index map :%ld",(long)index);
    NSString * indexMap = [self.arrayMaps objectAtIndex:index];
    NSMutableArray *pins =  [indexMap valueForKey:@"pins"];
    self.indexPin = 0;
#ifndef NO_GM
    NSMutableArray *arrayDynamic = [[NSMutableArray alloc]init];
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    CLLocation *myLocation = self.exploreMap.mapView.myLocation;
    GMSMarker *marker;
    vc.index = index;
    [self.exploreMap.mapView clear];
    Pin *pinAttribute;
    for (int i = 0 ; i < [pins count]  ; i ++) {
        pinAttribute = [[Pin alloc]init];
        NSString *valuePin =  [pins objectAtIndex:i];
        pinAttribute.idPin =  [valuePin valueForKey:@"id"];
        pinAttribute.longitude = [valuePin valueForKey:@"longitude"];
        pinAttribute.latitude= [valuePin valueForKey:@"latitude"];
        [arrayDynamic addObject:pinAttribute];
    }
    for (Pin *pinmarker in arrayDynamic) {
//        NSLog(@"check if my Location in there: %@",pinmarker.idPin);
        marker = [[GMSMarker alloc] init];
        marker.icon = [UIImage imageNamed:(@"pin.png")];
        marker.userData = [NSString stringWithFormat:@"%ld", (long)self.indexPin];
        CGPoint anchor = CGPointMake(0.5f, 0.5f);
        marker.groundAnchor = anchor;
        marker.position = CLLocationCoordinate2DMake([pinmarker.latitude doubleValue], [pinmarker.longitude  doubleValue]);
        bounds = [bounds includingCoordinate:marker.position];
        self.indexPin++;
        marker.map = self.exploreMap.mapView ;
    }
//    NSLog(@"nilai bounds before nya adalah : %@",bounds);
    CLLocationCoordinate2D location2D= CLLocationCoordinate2DMake(myLocation.coordinate.latitude, myLocation.coordinate.longitude);
    bounds =[bounds includingCoordinate:location2D];
    [self.exploreMap.mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:20.0f]];
#endif
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"mapDetailLocation"]) {
        NSIndexPath *index = (NSIndexPath *)sender;
        NSString *value = [self.arrayMaps objectAtIndex:index.row];
        MapDetailLocationViewController * childViewController = (MapDetailLocationViewController *) [segue destinationViewController];
        childViewController.titleLocation = [value valueForKey:@"title"];
        childViewController.mapDetailColletion = self;
        childViewController.pinsArray = [value valueForKey:@"pins"];
        childViewController.mapID = [value valueForKey:@"id"];
        childViewController.indexRow = index.row;
        }
}
@end
