//
//  ForgotPasswordViewController.h
//  AnyMap
//
//  Created by Antonio M on 21/01/16.
//  Copyright © 2016 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextView *email;
@property (strong, nonatomic) IBOutlet UIButton *doneButton;

@end
