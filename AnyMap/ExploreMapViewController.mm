//
//
//  ExploreMapViewController.m
//  AnyMap
//
//  Created by Antonio M on 17/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import "ExploreMapViewController.h"
#import "mapDetailCollectionViewController.h"
#import "PinUpViewController.h"
#import "NetworkManager.h"

#ifndef NO_GM
@interface ExploreMapViewController ()<GMSMapViewDelegate,CLLocationManagerDelegate>
#else
@interface ExploreMapViewController ()
#endif

@property (nonatomic)BOOL conditionVC;
@property(nonatomic) NSMutableArray *markers;
@property(strong,nonatomic)PinUpViewController *pinUpVC;
@end

@implementation ExploreMapViewController

#ifndef NO_GM
- (void)viewDidLoad {
    [super viewDidLoad];
    self.mapView.delegate = self;
    self.mapView.settings.rotateGestures = NO;
    self.mapView.settings.tiltGestures = NO;
    self.mapView.myLocationEnabled = YES;
    UIEdgeInsets mapInsets = (UIEdgeInsetsMake(70.0, 6.0, 148.0, 6.0));
    self.mapView.padding = mapInsets;
     CLLocationManager * locationManager = [[CLLocationManager alloc] init];
    CLLocation *fake = [[CLLocation alloc]initWithLatitude:1.3048425 longitude:103.8318243];
    [locationManager.delegate locationManager:locationManager didUpdateLocations:@[fake]];
     locationManager.delegate = self;
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [locationManager requestWhenInUseAuthorization];
    }
    else{
        [locationManager startUpdatingLocation];

    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
//    NSLog(@"posisi sekarang di explore map  : %@", [locations lastObject]);
}
- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
//    NSLog(@"tap the marker");
    CLLocationCoordinate2D firstLocation = marker.position;
//    NSLog(@"nilai latitude nya adalah %f ",firstLocation.latitude);
//    NSLog(@"nilai longitude nya adalah %f ",firstLocation.longitude);
//    NSLog(@"marker data : %@",marker.userData);
    self.pinUpVC = [self.storyboard instantiateViewControllerWithIdentifier:@"pinUpView"];
    self.pinUpVC.view.frame = CGRectMake(0.0f, 0.0f, self.tabBarController.view.frame.size.width, self.tabBarController.view.frame.size.height);
    self.pinUpVC.view.alpha = 0.0f;
    self.pinUpVC.isFromBookmark = NO;
    self.pinUpVC.index = self.index;
    self.pinUpVC.pinID = marker.userData;
    self.pinUpVC.myLocationLongitude = self.mapView.myLocation.coordinate.longitude;
    self.pinUpVC.myLocationLatitude = self.mapView.myLocation.coordinate.latitude;
//    NSLog(@"nilai pin : %@",marker.userData);
//    NSLog(@" nilai frame size %@",NSStringFromCGSize(self.pinUpVC.view.frame.size));
    [self.tabBarController.view insertSubview:self.pinUpVC.view aboveSubview:self.tabBarController.view];
    [UIView animateWithDuration:0.2f animations:^{
        self.pinUpVC.view.alpha = 1.0f;
    }completion:^(BOOL finished){
    }];
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews
{

}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"value : %@ ",[[NSUserDefaults standardUserDefaults] objectForKey:@"installDevice"]);
    if ([segue.identifier isEqualToString:@"mapCollection"]) {
        mapDetailCollectionViewController * childViewController = (mapDetailCollectionViewController *) [segue destinationViewController];
        childViewController.exploreMap = self;
        NSLog(@"map detail segue");
    }
}
#endif

@end
