/*-*-objc-*-*/


#import "AppDelegate.h"
#ifdef NO_IB
#import "AppDelegate.h"
#import "BoardingViewController.h"
#endif
#import "LoadingViewController.h"
#import <INTULocationManager/INTULocationManager.h>
#import "NetworkCheck.h"
@implementation LoadingViewController

#ifdef NO_IB
- (void)loadView
{
    self.view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame];
    self.view.backgroundColor = [UIColor colorWithRed:0.16078431372549018 green:0.58823529411764708 blue:0.95294117647058818 alpha:1.0f];

    CGFloat logoImageViewHeight = self.view.frame.size.height * 0.317;
    UIImageView *logoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(90.0f, 251.0f, logoImageViewHeight, logoImageViewHeight)];
    logoImageView.center = self.view.center;
    logoImageView.contentMode = UIViewContentModeScaleAspectFit;
    logoImageView.image = [UIImage imageNamed:@"bg_logo"];
    [self.view addSubview:logoImageView];
}
#endif

- (void)viewDidLoad{
    [super viewDidLoad];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
     if ([NetworkCheck instance].internetStatus != NotReachable) {
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyHouse
                                       timeout:10.0
                          delayUntilAuthorized:YES  // This parameter is optional, defaults to NO if omitted
                                         block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                             if (status == INTULocationStatusSuccess) {
                                                 
                                                 auto latitude = currentLocation.coordinate.latitude;
                                                 auto longitude = currentLocation.coordinate.longitude;
                                                 NetworkManager *network = [[NetworkManager alloc]init];
                                                 NSLog(@"saat di update latitude : %f", latitude);
                                                 NSLog(@"saat di update longitude : %f", longitude);
                                                 NSLog(@"value nya adalah  : %@ ",[[NSUserDefaults standardUserDefaults] objectForKey:@"installDevice"]);
                                                 NSLog(@"key nya adalah  : %@ ",[[[NSUserDefaults standardUserDefaults] objectForKey:@"installDevice"] objectForKey:@"api_key"]);
                                                 ((AppDelegate *)[UIApplication sharedApplication].delegate).downloadMaps = [network maps_by_response:[NSString stringWithFormat:@"%f",latitude] longitude:[NSString stringWithFormat:@"%f",longitude]];
                                                 if ([[[[NSUserDefaults standardUserDefaults] objectForKey:@"installDevice"] objectForKey:@"install_status"] isEqualToString:@"ok"] && [((AppDelegate *)[UIApplication sharedApplication].delegate).downloadMaps count]!=0 ) {
#ifndef NO_IB
                                                     [self performSegueWithIdentifier:@"navigation" sender:self];
#else
                                                     UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[BoardingViewController new]];
                                                     [navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
                                                     navigationController.navigationBar.shadowImage = [UIImage new];
                                                     [UINavigationBar appearance].barTintColor = [UIColor colorWithRed:0.1607843137254902 green:0.5882352941176471 blue:0.9529411764705882 alpha:1.0];
                                                     ((AppDelegate *)[UIApplication sharedApplication].delegate).window.rootViewController = navigationController;
#endif
                                                 }
                                                 
                                             }
                                             else if (status == INTULocationStatusTimedOut) {
                                                 NSLog(@"time out in here");
#ifndef NO_IB
                                                 [self performSegueWithIdentifier:@"navigation" sender:self];
#else
                                                 UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[BoardingViewController new]];
                                                 [navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
                                                 navigationController.navigationBar.shadowImage = [UIImage new];
                                                 [UINavigationBar appearance].barTintColor = [UIColor colorWithRed:0.1607843137254902 green:0.5882352941176471 blue:0.9529411764705882 alpha:1.0];
                                                 ((AppDelegate *)[UIApplication sharedApplication].delegate).window.rootViewController = navigationController;
#endif
                                                 
                                             }
                                             else {
                                                 auto latitude = currentLocation.coordinate.latitude;
                                                 auto longitude = currentLocation.coordinate.longitude;
                                                 NetworkManager *network = [[NetworkManager alloc]init];
                                                 NSLog(@"failed to do location latitude: %f", latitude);
                                                 NSLog(@"failed to do location longitude : %f", longitude);
                                                 NSLog(@"value nya adalah  : %@ ",[[NSUserDefaults standardUserDefaults] objectForKey:@"installDevice"]);
                                                 NSLog(@"key nya adalah  : %@ ",[[[NSUserDefaults standardUserDefaults] objectForKey:@"installDevice"] objectForKey:@"api_key"]);
                                                 ((AppDelegate *)[UIApplication sharedApplication].delegate).downloadMaps = [network maps_by_response:[NSString stringWithFormat:@"%f",latitude] longitude:[NSString stringWithFormat:@"%f",longitude]];
                                                 if ([[[[NSUserDefaults standardUserDefaults] objectForKey:@"installDevice"] objectForKey:@"install_status"] isEqualToString:@"ok"] && [((AppDelegate *)[UIApplication sharedApplication].delegate).downloadMaps count]!=0 ) {
#ifndef NO_IB
                                                     [self performSegueWithIdentifier:@"navigation" sender:self];
#else
                                                     UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[BoardingViewController new]];
                                                     [navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
                                                     navigationController.navigationBar.shadowImage = [UIImage new];
                                                     [UINavigationBar appearance].barTintColor = [UIColor colorWithRed:0.1607843137254902 green:0.5882352941176471 blue:0.9529411764705882 alpha:1.0];
                                                     ((AppDelegate *)[UIApplication sharedApplication].delegate).window.rootViewController = navigationController;
#endif
                                                 }
                                                 
                                             }
                                         }];
     }
    else
    {
#ifndef NO_IB
        [self performSegueWithIdentifier:@"navigation" sender:self];
#else
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[BoardingViewController new]];
        [navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
        navigationController.navigationBar.shadowImage = [UIImage new];
        [UINavigationBar appearance].barTintColor = [UIColor colorWithRed:0.1607843137254902 green:0.5882352941176471 blue:0.9529411764705882 alpha:1.0];
        ((AppDelegate *)[UIApplication sharedApplication].delegate).window.rootViewController = navigationController;
#endif
        
    }
    NetworkManager *network = [[NetworkManager alloc]init];
    [network installGlobalDevice];
  
}
#pragma locationManager
-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
        {
            // do some error handling
        }
            break;
        default:
        {
//            [manager startUpdatingLocation];
//            auto coordinate = self.locationManager.location.coordinate;
//            auto latitude = coordinate.latitude;
//            auto longitude = coordinate.longitude;
//            NetworkManager *network = [[NetworkManager alloc]init];
//            NSLog(@"saat di update latitude : %f", latitude);
//            NSLog(@"saat di update longitude : %f", longitude);
//            ((AppDelegate *)[UIApplication sharedApplication].delegate).downloadMaps = [network maps_by_response:[NSString stringWithFormat:@"%f",latitude] longitude:[NSString stringWithFormat:@"%f",longitude]];
//            if ([[[[NSUserDefaults standardUserDefaults] objectForKey:@"installDevice"] objectForKey:@"install_status"] isEqualToString:@"ok"] && [((AppDelegate *)[UIApplication sharedApplication].delegate).downloadMaps count]!=0 ) {
//#ifndef NO_IB
//                [self performSegueWithIdentifier:@"navigation" sender:self];
//#else
//                UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[BoardingViewController new]];
//                [navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
//                navigationController.navigationBar.shadowImage = [UIImage new];
//                [UINavigationBar appearance].barTintColor = [UIColor colorWithRed:0.1607843137254902 green:0.5882352941176471 blue:0.9529411764705882 alpha:1.0];
//                ((AppDelegate *)[UIApplication sharedApplication].delegate).window.rootViewController = navigationController;
//#endif
//            }
        }
            break;
    }
}
@end
