//
//  ExploreMapListViewController.h
//  AnyMap
//
//  Created by Antonio M on 17/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "ExploreRootViewController.h"
#import <CoreLocation/CoreLocation.h>
@interface ExploreMapListViewController : UITableViewController<CLLocationManagerDelegate>
@property(weak,nonatomic)ExploreRootViewController *exploreRoot;
@property (strong,nonatomic) CLLocationManager *locationManager;
@property (strong,nonatomic) CLLocation *currentLocation;
- (void)location;
- (void)updateMaps;
- (void)loadFirstMaps;
@end
