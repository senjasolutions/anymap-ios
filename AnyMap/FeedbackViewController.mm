//
//  FeedbackViewController.m
//  AnyMap
//
//  Created by Antonio M on 17/12/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import "FeedbackViewController.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "NetworkManager.h"
@interface FeedbackViewController ()

@end

@implementation FeedbackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)addFeedBack:(id)sender {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NetworkManager *network =[[NetworkManager alloc]init];
            bool statusReport =    [network addLog:@"message" addLogID:self.feedbackText.text];
            if (statusReport) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertView *alert= [[UIAlertView alloc ]initWithTitle:@"Information" message:@"Feedback has been sent" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
                    [alert show];
                    [self.view removeFromSuperview];
                });
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertView *alert= [[UIAlertView alloc ]initWithTitle:@"Information" message:@"Feedback failed" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
                    [alert show];
                });
            }

    });
}
- (IBAction)back:(id)sender {
    NSLog(@"back screen ");
    [UIView animateWithDuration:0.2f animations:^{
        self.view.alpha =0.0f;
    }completion:^(BOOL finished){    [self.view removeFromSuperview];}];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
