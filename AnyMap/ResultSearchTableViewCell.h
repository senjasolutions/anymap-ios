//
//  ResultSearchTableViewCell.h
//  AnyMap
//
//  Created by Antonio M on 29/10/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultSearchTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *locationName;

@end
