//
//  AddedPinsOtherCreatorViewController.m
//  AnyMap
//
//  Created by Antonio M on 25/01/16.
//  Copyright © 2016 AnyMap. All rights reserved.
//
#import "AppDelegate.h"
#import "AddedPinsOtherCreatorViewController.h"
#import "AMAddPinsOtherTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface AddedPinsOtherCreatorViewController ()

@end

@implementation AddedPinsOtherCreatorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    CGFloat ratio = 0;
    if (screenHeight == 480.0f)
    {
        ratio = 178;
    }
    else if (screenHeight == 568.0f)
    {
        ratio = 178;
    }
    else if (screenHeight == 667.0f)
    {
        ratio =178;
    }
    else if (screenHeight == 736.0f)
    {
        ratio = 178;
    }
    
    self.tableView.rowHeight = ratio;
    [self.tableView setContentInset:UIEdgeInsetsMake(0.0, 0.0, 9.0, 0.0)];
    
    NSMutableArray *mapsArray = ((AppDelegate *)[UIApplication sharedApplication].delegate).downloadMaps ;
    
    
    NSArray *filterMapsArray  = [mapsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"created_by_id=%@",((AppDelegate *)[UIApplication sharedApplication].delegate).idMap]];

    
    NSLog(@"by the pins  : %@",((AppDelegate *)[UIApplication sharedApplication].delegate).idMap);
//
    NSMutableArray *filteredArray = [NSMutableArray arrayWithArray:filterMapsArray];
    NSString *arrayPins = [filteredArray mutableCopy];
    self.arrayPinsOtherCreator = [[[arrayPins valueForKey:@"pins"] mutableCopy]lastObject];
    
    NSLog(@"pins yang terkumpul : %lu",(unsigned long)[self.arrayPinsOtherCreator count]);
//
    [self.tableView reloadData];
//
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  self.arrayPinsOtherCreator.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *key = [self.arrayPinsOtherCreator objectAtIndex:indexPath.row];
    AMAddPinsOtherTableViewCell   *cell = [tableView dequeueReusableCellWithIdentifier:@"AMOtherPinsCreator"];
    if (!cell) {
        NSLog(@"add the cell to pins");
        [tableView registerNib:[UINib nibWithNibName:@"CardAddedPinsOtherCreator" bundle:nil] forCellReuseIdentifier:@"AMOtherPinsCreator"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"AMOtherPinsCreator" forIndexPath:indexPath];
        
    }
    cell.pinTitle.text = [key valueForKey:@"title"];
    cell.pinDescription.text = [key valueForKey:@"description"];
    [cell.pinLikes setTitle:[key valueForKey:@"like_count"] forState:UIControlStateNormal];
    if ([key valueForKey:@"image"] == (id)[NSNull null]) {
        cell.photoImagePins.image = [UIImage imageNamed:@""];
    }
    else
    {
        [cell.photoImagePins sd_setImageWithURL:[NSURL URLWithString:[key valueForKey:@"image"]] placeholderImage:[UIImage imageNamed:@""] ];
    }
    

       return cell;
}


@end
