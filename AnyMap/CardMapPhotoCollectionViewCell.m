//
//  CardMapPhotoCollectionViewCell.m
//  AnyMap
//
//  Created by Antonio M on 23/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import "CardMapPhotoCollectionViewCell.h"

@implementation CardMapPhotoCollectionViewCell
- (id)initWithCoder:(NSCoder *)decoder
{
    if ((self = [super initWithCoder:decoder]) != nil) {
        NSString *nibName = @"CardMapPhotoCell";
        UIView *view = [[UINib nibWithNibName:nibName bundle:nil] instantiateWithOwner:self options:nil].firstObject;
        CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
        CGFloat ratio = 0;
        if (screenHeight == 480.0f)
        {
            ratio = 302;
        }
        else if (screenHeight == 568.0f)
        {
            ratio = 302;
        }
        else if (screenHeight == 667.0f)
        {
            ratio = 357;
        }
        else if (screenHeight == 736.0f)
        {
            ratio = 396;
        }
        self.frame = CGRectMake(0, 0, ratio, 139);
//        view.frame= CGRectMake(0, 0, ratio, 139);
//        NSLog(@" size frame  : %@",NSStringFromCGSize(view.frame.size));
//        NSLog(@" content view frame  : %@",NSStringFromCGSize(self.contentView.frame.size));
//        NSLog(@" cell frame  : %@",NSStringFromCGSize(self.frame.size));
//        NSLog(@" cell bounds  : %@",NSStringFromCGRect(self.bounds));
        [self.contentView addSubview:view];
    }
    
    return self;
}
- (id)initWithFrame:(CGRect)frame
{
    if ((self =[super initWithFrame:frame]) != nil) {
    }
    return self;
}
@end
