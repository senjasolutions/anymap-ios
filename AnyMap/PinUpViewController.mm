//
//  PinUpViewController.m
//  AnyMap
//
//  Created by Antonio M on 06/10/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//
#ifdef DEBUG
#endif
#import "mapDetailCollectionViewController.h"
#import "PinUpCollectionViewCell.h"
#import "AppDelegate.h"
#import "PinUpViewController.h"
#import "PinUpDetailViewController.h"
#import <OpenInGoogleMaps/OpenInGoogleMapsController.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "NetworkManager.h"
#import "LoginVC.h"

@interface PinUpViewController ()
@property (strong,nonatomic)NSMutableArray *arrayMap;
@end

@implementation PinUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.indexCollection= 0;
    // Do any additional setup after loading the view.
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeView:)];
    [tapGesture setNumberOfTapsRequired:1];
    [tapGesture setNumberOfTouchesRequired:1];
    [self.dummyGesture addGestureRecognizer:tapGesture];
    
    if (self.isFromBookmark) {
        self.arrayMap = [self.bookmarkMap mutableCopy];
//        NSLog(@"nilai pins arrau from bookmark :%@",self.arrayPINS);
        
        [self.collectionView reloadData];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    if (self.isFromBookmark) {
        self.arrayMap = [self.bookmarkMap mutableCopy];
//        NSLog(@"nilai pins arrau from bookmark :%@",self.arrayPINS);
        
        [self.collectionView reloadData];
    }
    else{
    NSLog(@"nilai map id nya adalah : %ld",(long)self.index);
    self.arrayMap = ((AppDelegate *)[UIApplication sharedApplication].delegate).downloadMaps;
    NSString *value = [self.arrayMap objectAtIndex:self.index];
    NSLog(@"value pins : %@",[value valueForKey:@"pins"]);
    }

}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.collectionView reloadData];
}
- (void)closeView:(UITapGestureRecognizer *)sender
{
    
    [UIView animateWithDuration:0.3f animations:^{
        self.view.alpha = 0.0f;
    } completion:^(BOOL finished)
     {
       [self.view removeFromSuperview];
     }];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UICollectionViewDataSource

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PinUpCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"pinUpCell" forIndexPath:indexPath];
    NSMutableArray *arraypins  = [[NSMutableArray alloc]init];
    NSString *value = [[NSString alloc]init];
    if (self.isFromBookmark) {
        NSLog(@"enter in here");
        self.arrayMap = [self.bookmarkMap mutableCopy];
        value = [self.arrayMap mutableCopy];
        arraypins = self.arrayPINS;
        
    }
    else{
        self.arrayMap = ((AppDelegate *)[UIApplication sharedApplication].delegate).downloadMaps;
        value  = [self.arrayMap objectAtIndex:self.index];
       arraypins = [value valueForKey:@"pins"];
    }

       if (indexPath.row == 0) {
           NSString *valuePin = [arraypins objectAtIndex:self.pinID.integerValue];
           cell.headerTitle.text = [valuePin valueForKey:@"title"];
           NSString * distance = [valuePin valueForKey:@"distance"];
           float convertDistance  = ceilf([distance floatValue] *10) /10;
           NSLog(@"convert distance : %f",convertDistance);
           cell.counter.text = [NSString stringWithFormat:@"%.1fkm",convertDistance];
           cell.creatorName.text = [value valueForKey:@"profile_name"];
           cell.roadName.text = [valuePin valueForKey:@"address"];
           cell.viewMore.tag = [self.pinID integerValue];
           [cell.viewMore addTarget:self action:@selector(detailMap:) forControlEvents:UIControlEventTouchUpInside];
//           cell.pinLike.selected = static_cast<BOOL>((*like_states)[pin["id"].asString()]);
           NSUserDefaults *standar = [[NSUserDefaults alloc]init];
           NSMutableArray *arrayLikePins  = [standar valueForKey:@"likeThePinMaps"];
           cell.pinLike.selected = NO;
           [cell.pinLike setTitle:[valuePin valueForKey:@"like_count"] forState:UIControlStateNormal];
           if ([arrayLikePins containsObject:[valuePin valueForKey:@"id"]]) {
               cell.pinLike.selected = YES;
               NSString *stringCounter = [NSString stringWithFormat:@"%ld" ,[[valuePin valueForKey:@"like_count"] integerValue] +1];
               [cell.pinLike setTitle:stringCounter forState:UIControlStateNormal];
           }
           
           [cell.pinLike addTarget:self action:@selector(likeMe:) forControlEvents:UIControlEventTouchUpInside];
           cell.direction.tag =[self.pinID integerValue];
           [cell.direction addTarget:self action:@selector(direction:) forControlEvents:UIControlEventTouchUpInside];
           cell.pinLike.tag = [self.pinID integerValue];

           if ([valuePin valueForKey:@"image"] == (id)[NSNull null]) {
               cell.photoDetail.image = [UIImage imageNamed:@""];
           }
           else
           {
               [cell.photoDetail sd_setImageWithURL:[NSURL URLWithString:[valuePin valueForKey:@"image"]] placeholderImage:[UIImage imageNamed:@""] ];
           }
           
           if ([value valueForKey:@"profile_image"] == (id)[NSNull null]) {
               cell.photoCreator.image = [UIImage imageNamed:@""];
           }
           else
           {
               [cell.photoCreator sd_setImageWithURL:[NSURL URLWithString:[value valueForKey:@"profile_image"]] placeholderImage:[UIImage imageNamed:@""] ];
           }
           return cell;
            }
        else if (indexPath.row <= [self.pinID integerValue])
            {
                NSString *valuePin = [arraypins objectAtIndex:indexPath.row -1];
                cell.headerTitle.text = [valuePin valueForKey:@"title"];
                NSString * distance = [valuePin valueForKey:@"distance"];
                float convertDistance  = ceilf([distance floatValue] *10) /10;
                NSLog(@"convert distance : %f",convertDistance);
                cell.counter.text = [NSString stringWithFormat:@"%.1fkm",convertDistance];
                cell.creatorName.text = [value valueForKey:@"profile_name"];
                cell.roadName.text = [valuePin valueForKey:@"address"];
                cell.viewMore.tag = indexPath.row -1;
                [cell.viewMore addTarget:self action:@selector(detailMap:) forControlEvents:UIControlEventTouchUpInside];
                //           cell.pinLike.selected = static_cast<BOOL>((*like_states)[pin["id"].asString()]);
                NSUserDefaults *standar = [[NSUserDefaults alloc]init];
                NSMutableArray *arrayLikePins  = [standar valueForKey:@"likeThePinMaps"];
                cell.pinLike.selected = NO;
                [cell.pinLike setTitle:[valuePin valueForKey:@"like_count"] forState:UIControlStateNormal];
                if ([arrayLikePins containsObject:[valuePin valueForKey:@"id"]]) {
                    cell.pinLike.selected = YES;
                    NSString *stringCounter = [NSString stringWithFormat:@"%ld" ,[[valuePin valueForKey:@"like_count"] integerValue] +1];
                    [cell.pinLike setTitle:stringCounter forState:UIControlStateNormal];
                }
                [cell.pinLike addTarget:self action:@selector(likeMe:) forControlEvents:UIControlEventTouchUpInside];
                cell.direction.tag =indexPath.row-1;
                [cell.direction addTarget:self action:@selector(direction:) forControlEvents:UIControlEventTouchUpInside];
                cell.pinLike.tag = indexPath.row-1;
                
                if ([valuePin valueForKey:@"image"] == (id)[NSNull null]) {
                    cell.photoDetail.image = [UIImage imageNamed:@""];
                }
                else
                {
                    [cell.photoDetail sd_setImageWithURL:[NSURL URLWithString:[valuePin valueForKey:@"image"]] placeholderImage:[UIImage imageNamed:@""] ];
                }
                
                if ([value valueForKey:@"profile_image"] == (id)[NSNull null]) {
                    cell.photoCreator.image = [UIImage imageNamed:@""];
                }
                else
                {
                    [cell.photoCreator sd_setImageWithURL:[NSURL URLWithString:[value valueForKey:@"profile_image"]] placeholderImage:[UIImage imageNamed:@""] ];
                }
                return cell;
            }
        else if (  indexPath.row > [self.pinID integerValue])
        {
            NSString *valuePin = [arraypins objectAtIndex:indexPath.row];
            cell.headerTitle.text = [valuePin valueForKey:@"title"];
            NSString * distance = [valuePin valueForKey:@"distance"];
            float convertDistance  = ceilf([distance floatValue] *10) /10;
            NSLog(@"convert distance : %f",convertDistance);
            cell.counter.text = [NSString stringWithFormat:@"%.1fkm",convertDistance];
            cell.creatorName.text = [value valueForKey:@"profile_name"];
            cell.roadName.text = [valuePin valueForKey:@"address"];
            cell.viewMore.tag = indexPath.row;
            [cell.viewMore addTarget:self action:@selector(detailMap:) forControlEvents:UIControlEventTouchUpInside];
            NSUserDefaults *standar = [[NSUserDefaults alloc]init];
            NSMutableArray *arrayLikePins  = [standar valueForKey:@"likeThePinMaps"];
            cell.pinLike.selected = NO;
            [cell.pinLike setTitle:[valuePin valueForKey:@"like_count"] forState:UIControlStateNormal];
            if ([arrayLikePins containsObject:[valuePin valueForKey:@"id"]]) {
                cell.pinLike.selected = YES;
                NSString *stringCounter = [NSString stringWithFormat:@"%ld" ,[[valuePin valueForKey:@"like_count"] integerValue] +1];
                [cell.pinLike setTitle:stringCounter forState:UIControlStateNormal];
            }
            
            [cell.pinLike addTarget:self action:@selector(likeMe:) forControlEvents:UIControlEventTouchUpInside];
            cell.direction.tag =indexPath.row;
            [cell.direction addTarget:self action:@selector(direction:) forControlEvents:UIControlEventTouchUpInside];
            cell.pinLike.tag = indexPath.row;
            
            if ([valuePin valueForKey:@"image"] == (id)[NSNull null]) {
                cell.photoDetail.image = [UIImage imageNamed:@""];
            }
            else
            {
                [cell.photoDetail sd_setImageWithURL:[NSURL URLWithString:[valuePin valueForKey:@"image"]] placeholderImage:[UIImage imageNamed:@""] ];
            }
            
            if ([value valueForKey:@"profile_image"] == (id)[NSNull null]) {
                cell.photoCreator.image = [UIImage imageNamed:@""];
            }
            else
            {
                [cell.photoCreator sd_setImageWithURL:[NSURL URLWithString:[value valueForKey:@"profile_image"]] placeholderImage:[UIImage imageNamed:@""] ];
            }

            return cell;
            
            }
    return cell;
    
}
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section
{
    if (self.isFromBookmark) {
        NSLog(@"check the number collection : %lu",(unsigned long)[self.arrayPINS count]);
        self.arrayMap = [self.bookmarkMap mutableCopy];
        
        return [self.arrayPINS count];
    }
    else{
        self.arrayMap = ((AppDelegate *)[UIApplication sharedApplication].delegate).downloadMaps;
        NSString *value = [self.arrayMap objectAtIndex:self.index];
        NSMutableArray *arraypins = [value valueForKey:@"pins"];
        self.arrayPINS = nil;
        self.arrayPINS = [arraypins mutableCopy];
        return [self.arrayPINS count];

    }

    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    CGFloat ratio = 0;
    if (screenHeight == 480.0f)
    {
        ratio = 284;
    }
    else if (screenHeight == 568.0f)
    {
        ratio = 284;
    }
    else if (screenHeight == 667.0f)
    {
        ratio = 338;
    }
    else if (screenHeight == 736.0f)
    {
        ratio = 338;
    }
    return CGSizeMake(ratio,438);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    CGFloat ratio = 18.0;
    if (screenHeight == 736.0f)
    {
        ratio = 38.0;
    }

    
    return UIEdgeInsetsMake(0.0, ratio,0.0 ,ratio);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 12;
}
#pragma detailMap
- (IBAction)likeMe:(UIButton *)sender
{
    NSString *value = [self.arrayPINS objectAtIndex:sender.tag];
    NSLog(@"pin id : %@",[value valueForKey:@"id"]);
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"hasSignIn"])
    {
        sender.selected =!sender.selected;
        NetworkManager *network = [[NetworkManager alloc]init];
        
        if (sender.selected) {
            NSLog(@"like the pin");
            NSString *likePIN = [NSString stringWithFormat:@"%@|0",[value valueForKey:@"id"]];
            [network likeThePin:likePIN  statusPin:[value valueForKey:@"id"]];
            NSInteger counlike = [[value valueForKey:@"like_count"] integerValue] + 1 ;
            [sender setTitle:[NSString stringWithFormat:@"%ld",(long)counlike] forState:UIControlStateNormal];
        }
        else
        {
            NSLog(@"unlike the pin");
            NSString *unLikeThePin = [NSString stringWithFormat:@"%@|1",[value valueForKey:@"id"]];
            [sender setTitle:[NSString stringWithFormat:@"%@",[value valueForKey:@"like_count"]] forState:UIControlStateNormal];
            [network unLikeThePin:unLikeThePin  statusPin:[value valueForKey:@"id"]];
        }
    }
    else
    {
        [self infoNeedSignInToLikeThePin];
    }

}
- (void)infoNeedSignInToLikeThePin
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Information"
                                  message:@"Please signup or login to like this pin"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* no = [UIAlertAction
                         actionWithTitle:@"Cancel"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                         }];
    
    
    UIAlertAction*  yes = [UIAlertAction
                           actionWithTitle:@"Login/Signup"
                           style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * action)
                           {
                               
                               UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                               LoginVC *login = [main instantiateViewControllerWithIdentifier:@"LOGIN"];
                               [self presentViewController:login animated:YES completion:nil];
                           }];
    
    
    [alert addAction:no];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (IBAction)direction:(id)sender
{
    UIButton *senderButton = (UIButton*)sender;
    NSString *value = [self.arrayPINS objectAtIndex:senderButton.tag];
    NSLog(@"pin id : %@",[value valueForKey:@"id"]);
    NSString *longitude = [value valueForKey:@"longitude"];
    NSString *latitude =  [value valueForKey:@"latitude"];

    NSLog(@"my desstination : %f, %f",[longitude doubleValue],[latitude doubleValue]);
    
    GoogleDirectionsDefinition *definition = [[GoogleDirectionsDefinition alloc]init];
    definition.startingPoint = nil;
    
    definition.destinationPoint =  [GoogleDirectionsWaypoint waypointWithLocation:CLLocationCoordinate2DMake([latitude doubleValue],[longitude doubleValue])];

    definition.travelMode = kGoogleMapsTravelModeDriving;
    
    [OpenInGoogleMapsController sharedInstance].fallbackStrategy   = kGoogleMapsFallbackSafari;
    
    [[OpenInGoogleMapsController sharedInstance] openDirections:definition];
}
- (IBAction)detailMap:(UIButton *)sender
{
    [self performSegueWithIdentifier:@"showDetailMap" sender:sender];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSString *valueMap = [[NSString alloc]init];
    if (self.isFromBookmark) {
        NSLog(@"enter in here");
        self.arrayMap = [self.bookmarkMap mutableCopy];
        valueMap = [self.arrayMap mutableCopy];
        
    }
    else{
        self.arrayMap = ((AppDelegate *)[UIApplication sharedApplication].delegate).downloadMaps;
        valueMap = [self.arrayMap objectAtIndex:self.index];
    }


    UIButton *senderButton = (UIButton*)sender;
    if ([segue.identifier isEqualToString:@"showDetailMap"]) {
        PinUpDetailViewController * pinUpDetail = (PinUpDetailViewController *) [segue destinationViewController];
        NSString *value = [self.arrayPINS objectAtIndex:senderButton.tag];
        NSLog(@"pin id : %@",[value valueForKey:@"id"]);
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:[value valueForKey:@"image"]
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    pinUpDetail.cover = image;
                                }
                                
                            }];
        
        SDWebImageManager *managerTwo = [SDWebImageManager sharedManager];
        [managerTwo downloadImageWithURL:[valueMap valueForKey:@"profile_image"]
                                 options:0
                                progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                    // progression tracking code
                                }
                               completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                   if (image) {
                                       pinUpDetail.creator = image;
                                   }
                                   
                               }];
        
        pinUpDetail.profile = [value valueForKey:@"profile_name"];
        pinUpDetail.road =[value valueForKey:@"address"];
        NSString * distance = [NSString stringWithFormat: @"%@",[value valueForKey:@"distance"]];
        float convertDistance  = ceilf([distance floatValue] *10) /10;
        pinUpDetail.distanceRoad = [NSString stringWithFormat:@"%.1fkm",convertDistance];
        pinUpDetail.descript = [value valueForKey:@"description"];
        pinUpDetail.title =[value valueForKey:@"title"];
        pinUpDetail.mapTitleS = [value valueForKey:@"title"];
        pinUpDetail.profile = [valueMap valueForKey:@"profile_name"];
        pinUpDetail.indexPin = senderButton.tag;
        pinUpDetail.indexMap = self.index;
        pinUpDetail.latitude = [[value valueForKey:@"latitude"] doubleValue];
        pinUpDetail.longitude = [[value valueForKey:@"longitude"] doubleValue];
        pinUpDetail.pinID = [value valueForKey:@"id"];
        NSUserDefaults *standar = [[NSUserDefaults alloc]init];
        NSMutableArray *arrayLikePins  = [standar valueForKey:@"likeThePinMaps"];
        BOOL isPinLike = NO;
        for (int i = 0 ; i< [arrayLikePins count]; i++ ) {
            if ([[arrayLikePins objectAtIndex:i] isEqualToString:[value valueForKey:@"id"]]) {
                isPinLike= YES;
                break;
            }
        }
        if (isPinLike) {
            NSInteger counlike = [[value valueForKey:@"like_count"] integerValue] + 1 ;
            pinUpDetail.pinButtonValue =  [NSString stringWithFormat:@"%ld" ,(long)counlike];
            pinUpDetail.buttonState = YES;
            
        }
        else
        {
            pinUpDetail.buttonState = NO;
            pinUpDetail.pinButtonValue =[value valueForKey:@"like_count"];
        }
        
    }
}
#pragma mark - UIScrollViewDelegate
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    NSLog(@"scrolling view");
    self.arrayMap = ((AppDelegate *)[UIApplication sharedApplication].delegate).downloadMaps;
    NSString *value = [self.arrayMap objectAtIndex:self.index];
    NSMutableArray *arraypins = [value valueForKey:@"pins"];
    NSInteger count = [arraypins count];
    if (velocity.x > 0.0f)
    {
        if (self.indexCollection < count-1)
        {
            self.indexCollection++;
           
            
        }
    }
    else if (velocity.x < 0.0f)
    {
        if (self.indexCollection > 0)
        {
            self.indexCollection--;
            NSLog(@"index berkurang");
        }
    }
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    CGFloat ratio = 0;
    CGFloat ratioLine = 12;
    if (screenHeight == 480.0f)
    {
        ratio = 284.0f;
    }
    else if (screenHeight == 568.0f)
    {
        ratio = 284.0f;
    }
    else if (screenHeight == 667.0f)
    {
        ratio = 338.0f;
    }
    else if (screenHeight == 736.0f)
    {
        ratio = 338.0f;
    }
    //    CGFloat lebarPack = self.collectionView.frame.size.height * ratio;
    //    CGFloat lineSpacing = self.collectionView.frame.size.width * ratioLine;
    //    NSLog(@"nilai ny adalah : %f",lebarPack+lineSpacing);
    //    CGFloat pointOne = (lebarPack + lineSpacing) * self.index;a
    //    CGFloat inset = (self.collectionView.frame.size.width - self.collectionView.frame.size.height * ratio) / 2.0;
    //    CGFloat pointX = pointOne + inset + (0.5 * lebarPack) - (0.5 * self.collectionView.frame.size.width);
    CGFloat pointX = self.indexCollection * (ratio + ratioLine);
    
    
    
    NSLog(@"%f",pointX);
    *targetContentOffset = CGPointMake(pointX, 0.0f);
}
@end
