//
//  CardMapPhotoCollectionViewController.m
//  AnyMap
//
//  Created by Antonio M on 22/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import "CardMapPhotoCollectionViewController.h"
#import "CardMapPhotoCollectionViewCell.h"
@interface CardMapPhotoCollectionViewController ()

@end

@implementation CardMapPhotoCollectionViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self.collectionView setDataSource:self];
    [self.collectionView  setDelegate:self];
    [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cardMapPhoto"];
    NSArray  *helpAssets;
    helpAssets = [NSArray arrayWithObjects:@"help_1_no_480.png",@"help_2_no_480.png",@"help_3_no_480.png",@"help_4_no_480.png",@"help_6_no_480.png", nil];
    self.itemIDs = helpAssets.mutableCopy;
    
    NSLog(@"tes collection view : %@",self.itemIDs);
    [self.collectionView reloadData];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
   
}
#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {

    
    return self.itemIDs.count;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CardMapPhotoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cardMapPhoto" forIndexPath:indexPath];
    
    
    return cell;
}
#pragma mark <UICollectionViewDelegate>

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

@end
