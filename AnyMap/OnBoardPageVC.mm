/*-*-objc-*-*/
//  OnBoardPageVC.m
//  AnyMap
//
//  Created by Antonio M on 15/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//


#import "AppDelegate.h"
#import "OnBoardPageVC.h"
/*
#import "ExploreMapListViewController.h"
#import "ExploreMapViewController.h"
#import "mapDetailCollectionViewController.h"
 */

#import "NetworkManager.h"
#import "RootNavTabBarViewController.h"

@interface OnBoardPageVC ()
@property(nonatomic)NSMutableArray *counterSelected;
@property(nonatomic)NSMutableArray *selectedButton;
@end

@implementation OnBoardPageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    //---- For getting current gps location
    self.locationManager = [CLLocationManager new];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    //------
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    else{
        [self.locationManager startUpdatingLocation];
        
    }

    
    [self.locationManager stopUpdatingLocation];
    self.counterSelected = [[NSMutableArray alloc]init];
    if (self.currentView) {
        [self.onBoardingPage setHidden:YES];
        [self.interestPageView setHidden:NO];
        self.startExplore.enabled = NO;
    }
    else
    [self setup];
}
-(void)setup
{
    self.ImageViewOnBoard.image = self.imageBackground;
    self.ImageViewIconOnBoard.image = self.imageLogo;
    self.TitleOnBoardPage.text =self.titleLabel;
    self.descOnBoardPage.text = self.descLabel;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma  setInterest
- (IBAction)interest:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        [self.counterSelected addObject:sender];
    }
    else {
        [self.counterSelected removeObject: sender];
    }
    
    if ([self.counterSelected count] == 0) {
        self.startExplore.enabled = NO;
        [self.startExplore setSelected:NO];
    }
    else
    {
        self.startExplore.enabled = YES;
        self.startExplore.selected = YES;
 
    }
}

- (IBAction)startExplore:(UIButton *)sender {
    NSUserDefaults *pref = [[NSUserDefaults alloc]init];
    NSMutableArray *arrayCategoryTemp = [[NSMutableArray alloc]init];
    for (int i = 0; i <[self.buttons count]; i++) {
        UIButton * button = [self.buttons objectAtIndex:i];
        if (button.selected) {
            NSString *formatCategory = [NSString stringWithFormat:@"%ld",(long)button.tag];
            [arrayCategoryTemp addObject:formatCategory];
        }
    }
    [pref setObject:arrayCategoryTemp forKey:@"category"];
    [pref synchronize];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    RootNavTabBarViewController *rootNavTabBar = [storyboard instantiateViewControllerWithIdentifier:@"ROOTTABBAR"];
    [self.navigationController pushViewController:rootNavTabBar animated:NO];
    

}

@end
