//
//  SearchViewController.h
//  AnyMap
//
//  Created by Antonio M on 19/10/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *result;
@property (strong,nonatomic)NSString *resultString;
@end
