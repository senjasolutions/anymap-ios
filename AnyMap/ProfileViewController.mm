//
//  ProfileViewController.m
//  AnyMap
//
//  Created by Antonio M on 23/10/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//
#define YOUR_APP_STORE_ID 1045910254
#import "AppDelegate.h"
#import "ProfileViewController.h"
#import "AboutDialogViewController.h"
#import "AddedPinsTableViewController.h"
#import "BookmarkedMapsTableViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "FeedbackViewController.h"
#import "NetworkManager.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "LoginVC.h"
@interface ProfileViewController ()<overFlowDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate>
@property(strong,nonatomic)OverFlowMenuViewController *overFlow;
@property(strong,nonatomic)FeedbackViewController *feedBack;
@property(strong,nonatomic)AboutDialogViewController *about;
@property(nonatomic)BOOL conditionVC;
@property (copy) NSString *urlPath;
@end

@implementation ProfileViewController
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"hasSignIn"])
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information"
                                                        message:@"Please signup or login to see your profile"
                                                       delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"Cancel",@"Login/Signup" ,nil];
        [alert show];
        
        
    }
    else{
        
        if (![[[NSUserDefaults standardUserDefaults]objectForKey:@"installDevice"]objectForKey:@"follower"] || ![[[NSUserDefaults standardUserDefaults]objectForKey:@"installDevice"]objectForKey:@"following"] ) {
            self.counterProfile.text =@"0";
            self.counterFollowing.text = @"0";
        }
        else
        {
            self.counterProfile.text =[[[NSUserDefaults standardUserDefaults]objectForKey:@"installDevice"]objectForKey:@"follower"];
            self.counterFollowing.text =[[[NSUserDefaults standardUserDefaults]objectForKey:@"installDevice"]objectForKey:@"following"];
        }
    }

}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 1) {
        NSLog(@"nilai parent VC : %@",self.parentViewController.parentViewController);
        LoginVC *login = [self.storyboard instantiateViewControllerWithIdentifier:@"LOGIN"];
        [self presentViewController:login animated:YES completion:nil];
    }
    else if (buttonIndex == 0)
    {
    
//        [self setSelectedIndex: 0];
        [self.tabBarController setSelectedIndex:0];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"hasSignIn"])
    {
        self.textName.text = [[[NSUserDefaults standardUserDefaults]objectForKey:@"installDevice"]objectForKey:@"name"];
        [self.avatarPhoto sd_setImageWithURL:[NSURL URLWithString:[[[NSUserDefaults standardUserDefaults]objectForKey:@"installDevice"]objectForKey:@"profile_image"]] placeholderImage:[UIImage imageNamed:@"placeholder_pic_big"] ];
        self.counterProfile.text =[[[NSUserDefaults standardUserDefaults]objectForKey:@"installDevice"]objectForKey:@"follower"];
        self.counterFollowing.text =[[[NSUserDefaults standardUserDefaults]objectForKey:@"installDevice"]objectForKey:@"following"];
    
    }
      [self initDefaultClass];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
}

- (void)initDefaultClass
{
    UIViewController *currentVC = [self.childViewControllers objectAtIndex:0];
    if ([currentVC isKindOfClass:[BookmarkedMapsTableViewController class]]) {
        if (!self.conditionVC) {
            AddedPinsTableViewController *addVC = [[UIStoryboard storyboardWithName:@"AddedPins" bundle:nil]instantiateViewControllerWithIdentifier:@"AddedPinsTableViewController"];
            [self addChildViewController:addVC];
            [currentVC didMoveToParentViewController:self];
            addVC.view.frame = self.containerView.bounds;
            [self.containerView insertSubview:addVC.view belowSubview:currentVC.view];
        }
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (IBAction)showOverFlowMenu:(id)sender {
    self.overFlow = [[OverFlowMenuViewController alloc]initWithNibName:@"OverflowProfile" bundle:nil];
    
    self.overFlow.view.alpha = 0.0f;
    
    self.overFlow.view.frame = CGRectMake(0.0f,0.0f, self.overFlow.view.frame.size.width, self.overFlow.view.frame.size.height);
    
    [self.navigationController.view insertSubview:self.overFlow.view aboveSubview:self.navigationController.navigationBar];
    
    self.overFlow.delegateOverFlow = self;
    
    [UIView animateWithDuration:0.2f animations:^{
        
        self.overFlow.view.alpha =1.0f;
        
    }completion:^(BOOL finished){}];
}
- (void)showEdit
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"hasSignIn"])
    {
        self.pickPhoto.hidden = NO;
        
        self.doneButton.hidden = NO;
        
        [self.textName setEnabled:YES];
        
        [self.textName becomeFirstResponder];
        
        [self hideOverFlow];
        self.doneView.hidden = NO;
        self.moreView.hidden = YES;
        
    }
}
-(void)hideOverFlow
{
    [UIView animateWithDuration:0.2f animations:^{
        self.overFlow.view.alpha =0.0f;
    }completion:^(BOOL finished){
        [self.overFlow.view removeFromSuperview];
    }];
}
- (IBAction)pick:(id)sender {
    NSLog(@"take or pick");
    UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
    pickerController.delegate = self;
    [self presentViewController:pickerController animated:YES completion:nil];

}
- (IBAction)done:(UIButton *)sender {
    self.doneView.hidden = YES;
    self.moreView.hidden = NO;
    self.pickPhoto.hidden = YES;
    [self.textName setEnabled:NO];
    
    
    __block  NSString * infoUploadingImage;
    NetworkManager *network = [[NetworkManager alloc]init];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.labelText = @"Uploading...";
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        infoUploadingImage  =  [network editProfile:self.textName.text imageURL:self.urlPath imagePath:self.avatarPhoto.image];
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([[infoUploadingImage valueForKey:@"status" ] isEqualToString:@"ok"]) {
                UIAlertView *alert= [[UIAlertView alloc ]initWithTitle:@"Information" message:@"Profile Updated!" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
                [alert show];
            }
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
    });
    
}

#pragma mark -
#pragma mark UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSURL *refURL = [info valueForKey:UIImagePickerControllerReferenceURL];
    self.avatarPhoto.image  = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *imageAsset)
    {
        ALAssetRepresentation *imageRep = [imageAsset defaultRepresentation];
        NSLog(@"[imageRep filename] : %@", [imageRep filename]);
        [self writeImageData:self.avatarPhoto.image filename:[imageRep filename]];
    };
    ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
    [assetslibrary assetForURL:refURL resultBlock:resultblock failureBlock:nil];
    [picker dismissViewControllerAnimated:YES completion:nil];
}
- (void)writeImageData:(UIImage *)image filename:(NSString *)name
{
    NSData *pngData = UIImagePNGRepresentation(image);
    NSString *urlTemp = [self documentsPathForFileName:name];
    [pngData writeToFile:urlTemp atomically:YES]; //Write the file
    self.urlPath = [NSString stringWithFormat:@"%@",urlTemp];
    NSLog(@"image yang akan di post ke peladen : %@",self.urlPath);
    //read the url data again
}
- (NSString *)documentsPathForFileName:(NSString *)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    
    return [documentsPath stringByAppendingPathComponent:name];
}

#pragma UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    textField.placeholder = nil;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.tag ==0 ) {
        textField.placeholder = @"?";
    }
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}
- (IBAction)changeSegment:(id)sender {
    BookmarkedMapsTableViewController *bookMarked = (BookmarkedMapsTableViewController *)[self.childViewControllers objectAtIndex:0];
    AddedPinsTableViewController *addPins = (AddedPinsTableViewController *)[self.childViewControllers objectAtIndex:1];
    if (self.segmentIndex.selectedSegmentIndex == 0) {
        self.conditionVC = YES;
        [bookMarked didMoveToParentViewController:self];
        [self.containerView addSubview:bookMarked.view];
    }
    else if (self.segmentIndex.selectedSegmentIndex == 1)
    {
        self.conditionVC = NO;
        [addPins didMoveToParentViewController:self];
        [self.containerView addSubview:addPins.view];
    }
}
- (void)rateTheApp
{

    
    NSLog(@"try to rate the app");
    static NSString *const iOS7AppStoreURLFormat = @"itms-apps://itunes.apple.com/app/id%d";
    static NSString *const iOSAppStoreURLFormat = @"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%d";
    
  NSURL *theUrl =  [NSURL URLWithString:[NSString stringWithFormat:([[UIDevice currentDevice].systemVersion floatValue] >= 7.0f)? iOS7AppStoreURLFormat: iOSAppStoreURLFormat, YOUR_APP_STORE_ID]];
    [[UIApplication sharedApplication] openURL:theUrl];
}
- (void)feedback
{
    NSLog(@"add the feedback");
    self.feedBack = [[FeedbackViewController alloc]initWithNibName:@"FeedbackEntryDialog" bundle:nil];
    self.feedBack.view.alpha = 0.0f;
    self.feedBack.view.frame = CGRectMake(0.0f,0.0f, self.feedBack.view.frame.size.width, self.feedBack.view.frame.size.height);
    [self.tabBarController.view insertSubview:self.feedBack.view aboveSubview:self.overFlow.view];
    [UIView animateWithDuration:0.2f animations:^{
        self.feedBack.view.alpha =1.0f;
    }completion:^(BOOL finished){}];

    
}
- (void)aboutAM
{
    self.about = [[AboutDialogViewController alloc]initWithNibName:@"AboutDialog" bundle:nil];
    self.about.view.alpha = 0.0f;
    self.about.view.frame = CGRectMake(0.0f,0.0f, self.about.view.frame.size.width, self.about.view.frame.size.height);
    [self.tabBarController.view insertSubview:self.about.view aboveSubview:self.overFlow.view];
    [UIView animateWithDuration:0.2f animations:^{
        self.about.view.alpha =1.0f;
    }completion:^(BOOL finished){}];

}
@end
