//
//  CardMapPhotoCollectionViewController.h
//  AnyMap
//
//  Created by Antonio M on 22/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "CardMapPhotoCollectionDataSource.h"
@interface CardMapPhotoCollectionViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (copy, nonatomic) NSString *cellReuseIdentifier;
@property (copy,nonatomic)NSArray *itemIDs;
@end
