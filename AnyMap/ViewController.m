//
//  ViewController.m
//  AnyMap
//
//  Created by Antonio M on 21/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import "ViewController.h"
#import "BoardingVC.h"
@interface ViewController ()
@property(strong,nonatomic)BoardingVC *childViewController;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
 
}
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString: @"embedView"]) {
        self.childViewController = (BoardingVC *) [segue destinationViewController];
        self.childViewController.vc =self;
    }
}

- (IBAction)skip:(id)sender {
    [self.navigationController performSegueWithIdentifier:@"exploreMenu" sender:self];
}
- (IBAction)signUp:(id)sender {
     [self.navigationController performSegueWithIdentifier:@"signUp" sender:self];
}
- (IBAction)login:(id)sender {
     [self.navigationController performSegueWithIdentifier:@"login" sender:self];
}

@end
