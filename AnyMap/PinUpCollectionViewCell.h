//
//  PinUpCollectionViewCell.h
//  AnyMap
//
//  Created by Antonio M on 06/10/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 A contain collections pin view cell based by uicolletion view cell
 */
@interface PinUpCollectionViewCell : UICollectionViewCell
/**
 A set an image in photo detail
 */
@property (strong, nonatomic) IBOutlet UIImageView *photoDetail;
/**
 A set an header title text
 */
@property (strong, nonatomic) IBOutlet UILabel *headerTitle;
/**
 A set a counter text
 */
@property (strong, nonatomic) IBOutlet UILabel *counter;
/**
 A set a name of creator
 */
@property (strong, nonatomic) IBOutlet UILabel *creatorName;
/**
 An action button to see more info the pin
 */
@property (strong, nonatomic) IBOutlet UIButton *viewMore;
/**
 A set an action direction for the google action
 */
@property (strong, nonatomic) IBOutlet UIButton *direction;
/**
 A set an action to like the pin
 */
@property (strong, nonatomic) IBOutlet UIButton *pinLike;
/**
 A set an image in photo creator
 */
@property (strong, nonatomic) IBOutlet UIImageView *photoCreator;
/**
 A set road name label
 */
@property (strong, nonatomic) IBOutlet UILabel *roadName;
@end
