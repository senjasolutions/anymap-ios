//
//  NetworkCheck.m
//  AnyMap
//
//  Created by Antonio M on 13/02/16.
//  Copyright © 2016 AnyMap. All rights reserved.
//
#import "NetworkCheck.h"
@implementation NetworkCheck



static NetworkCheck * _instance = nil;
+ (void)initialize {
    _instance = [[NetworkCheck alloc] init];
}
+ (NetworkCheck*)instance { return _instance; }

- (id)init {
    self = [super init];
    if (self != nil){
        internetReachable = [Reachability reachabilityForInternetConnection];
        [internetReachable startNotifier];
    }
    return self;
}

- (NetworkStatus)internetStatus {
    return [internetReachable currentReachabilityStatus];
}

- (void)addTarget:(id)target selector:(SEL)selector {
    [[NSNotificationCenter defaultCenter] addObserver:target selector:selector name:kReachabilityChangedNotification object:nil];
}

- (void)removeTarget:(id)target {
    [[NSNotificationCenter defaultCenter] removeObserver:target name:kReachabilityChangedNotification object:nil];
}
@end