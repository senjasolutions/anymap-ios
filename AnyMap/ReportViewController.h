//
//  ReportViewController.h
//  AnyMap
//
//  Created by Antonio M on 30/10/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol overFlowMapDelegate
- (void)showEdit;
- (void)hideOverFlow;
@end
@interface ReportViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *tapGesture;
@property (weak)id<overFlowMapDelegate>delegateOverFlowMap;
@end
