//
//  PinUpDetailViewController.h
//  AnyMap
//
//  Created by Antonio M on 14/10/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReportViewController.h"
@interface PinUpDetailViewController : UIViewController<overFlowMapDelegate>
@property (strong, nonatomic) IBOutlet UIButton *likePin;
@property (strong, nonatomic) IBOutlet UILabel *roadName;
@property (strong, nonatomic) IBOutlet UIImageView *coverPhoto;
@property (strong, nonatomic) IBOutlet UILabel *distance;
@property (strong, nonatomic) IBOutlet UIImageView *creatorPhoto;
@property (strong, nonatomic) IBOutlet UILabel *descriptionMapTV;
@property (strong, nonatomic) IBOutlet UIButton *addComment;
@property (strong, nonatomic) IBOutlet UIButton *profileName;
@property (strong, nonatomic) IBOutlet UILabel *titilePin;
@property (weak, nonatomic) IBOutlet UIButton *commentCounter;
@property (strong, nonatomic) IBOutlet UILabel *mapTitle;
@property (strong,nonatomic)NSString *road;
@property (strong, nonatomic)UIImage * cover;
@property (strong,nonatomic)NSString *mapTitleS;
@property (strong, nonatomic)NSString *distanceRoad;
@property (strong, nonatomic)UIImage * creator;
@property (strong, nonatomic)NSString *descript;
@property (strong, nonatomic)NSString *profile;
@property (strong,nonatomic)NSString *pinID;
@property (strong,nonatomic)NSString *titleS;
@property (nonatomic)NSInteger indexPin;
@property (nonatomic)NSInteger indexMap;
@property (copy, nonatomic) NSString *likeCountString;
@property (nonatomic) double longitude;
@property (nonatomic) double latitude;
@property(strong,nonatomic)ReportViewController *report;
@property(nonatomic)NSInteger indexRow;
@property (strong,nonatomic)NSString *pinButtonValue;
@property (nonatomic)BOOL buttonState;
@end
