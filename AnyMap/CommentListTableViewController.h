//
//  CommentListTableViewController.h
//  AnyMap
//
//  Created by Gumilar Adyana Putra on 10/23/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentListTableViewController : UITableViewController
@property(strong,nonatomic)NSMutableArray *arrayComments;
@end
