//
//  PinCreationViewController.h
//  AnyMap
//
//  Created by Antonio M on 15/10/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PinCreationViewController : UIViewController<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UIView *dummyGesture;
@property (strong, nonatomic) IBOutlet UITextField *nameLocation;
@property (strong, nonatomic) IBOutlet UITextView *writeDescription;
@property (strong, nonatomic) IBOutlet UILabel *locationNow;
@property (strong, nonatomic) IBOutlet UIImageView *mapPhoto;
@property (strong, nonatomic) NSString *mapID;
@property (nonatomic) double longitude;
@property (nonatomic) double latitude;
@property (strong, nonatomic) IBOutlet UITextField *gpsLocation;
@property (strong,nonatomic) NSString *address;
@end
