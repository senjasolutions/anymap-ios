//
//  CommentViewController.h
//  AnyMap
//
//  Created by Antonio M on 23/10/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *titlePin;
@property (strong, nonatomic) IBOutlet UITextView *textCommet;
@property (strong,nonatomic)NSString *pinTitle;
@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (strong,nonatomic)NSString *pinID;
@end
