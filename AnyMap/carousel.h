//
//  carousel.h
//  AnyMap
//
//  Created by Antonio M on 30/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 A carousel page cell based by UIViewController 
 */
@interface carousel : UIViewController
/**set an page index by the collection carousel page */
@property(nonatomic)NSInteger pageIndex;
/**set for image background identifier */
@property(strong,nonatomic)UIImage *imageBackground;
/**set a  string in header map */
@property(strong,nonatomic)NSString *header;
/**set a string to get the Id pin */
@property(strong,nonatomic)NSString *idPin;
/**set for image background as a getter  identifier */
@property (strong, nonatomic) IBOutlet UIImageView *background;
/**set a property string heade map identifier */
@property (strong, nonatomic) IBOutlet UILabel *headerMap;

@end
