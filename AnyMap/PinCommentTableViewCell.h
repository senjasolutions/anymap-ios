//
//  PinCommentTableViewCell.h
//  AnyMap
//
//  Created by Antonio M on 11/11/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 a property to list comment cell
 */
@interface PinCommentTableViewCell : UITableViewCell
/**
 A set an image for other profile
 */
@property (strong, nonatomic) IBOutlet UIImageView *profileUserComment;
/**
 A set a username comment
 */
@property (strong, nonatomic) IBOutlet UILabel *userNameComment;
/**
 A set a comment message text
 */
@property (strong, nonatomic) IBOutlet UILabel *commentMessage;
/**
 A set a comment label
 */
@property (strong, nonatomic) IBOutlet UILabel *timeComment;
@end
