//
//  mapDetailCollectionViewController.h
//  AnyMap
//
//  Created by Antonio M on 23/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExploreMapViewController.h"
#import <CoreLocation/CoreLocation.h>
@interface mapDetailCollectionViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,CLLocationManagerDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property(strong,nonatomic)ExploreMapViewController *exploreMap;
@property(strong,nonatomic)NSMutableArray *mapCollections;
@property (strong,nonatomic) CLLocationManager *locationManager;
@property (strong,nonatomic) CLLocation *currentLocation;
- (void)location;
- (void)updateMaps;
- (void)loadFirstMaps;
@end
