//
//  ExploreMapListTableViewCell.h
//  AnyMap
//
//  Created by Antonio M on 29/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 A ExploreMapListTableViewCell table view cell represents an
 */
@interface ExploreMapListTableViewCell : UITableViewCell
/**set  for text header Map identifier */
@property (strong, nonatomic) IBOutlet UILabel *headerMap;
/**set  for text location identifier */
@property (strong, nonatomic) IBOutlet UILabel *counterLocation;
/**set  for image creator photo identifier */
@property (strong, nonatomic) IBOutlet UIImageView *creatorPhoto;
/**set  for text description map */
@property (strong, nonatomic) IBOutlet UILabel *descriptionMap;
/**set  for image map identifier */
@property (strong, nonatomic) IBOutlet UIImageView *map;
/**set  for change behavior bookmark button identifier */
@property (strong, nonatomic) IBOutlet UIButton *bookmark;
/**set  for action detail creator  */
@property (strong, nonatomic) IBOutlet UIButton *creatorButton;
/**set  for creator name */
@property (strong, nonatomic) IBOutlet UIButton *creatorName;

@end
