IPHONE_IP:=hong
PROJECTNAME:=AnyMap
APPFOLDER:=$(PROJECTNAME).app
INSTALLFOLDER:=$(PROJECTNAME).app

CC:=ios-clang
CPP:=ios-clang++

CFLAGS += -g0 -O2
CFLAGS += -DNO_IB
CFLAGS += -DNO_GM
CFLAGS += -DNO_GP
CFLAGS += -DNO_FB
CFLAGS += -fobjc-arc
CFLAGS += -fblocks

CPPFLAGS += -std=c++11
CPPFLAGS += $(CFLAGS)
CPPFLAGS += -stdlib=libstdc++

LDFLAGS += -lcurl
LDFLAGS += -ljsoncpp
LDFLAGS += -lz
LDFLAGS += -framework Foundation
LDFLAGS += -framework UIKit
LDFLAGS += -framework Security
LDFLAGS += -framework CoreLocation
//LDFLAGS += -framework Accelerate
//LDFLAGS += -framework AVFoundation
//LDFLAGS += -framework CoreBluetooth
//LDFLAGS += -framework CoreData
//LDFLAGS += -framework CoreGraphics
//LDFLAGS += -framework CoreText
//LDFLAGS += -framework GLKit
//LDFLAGS += -framework ImageIO
//LDFLAGS += -framework OpenGLES
//LDFLAGS += -framework QuartzCore
//LDFLAGS += -framework SystemConfiguration
//LDFLAGS += -framework FBSDKCoreKit
//LDFLAGS += -framework GoogleMaps

SRCDIR=.
OBJS+=$(patsubst %.m,%.o,$(wildcard $(SRCDIR)/*.m))
OBJS+=$(patsubst %.mm,%.o,$(wildcard $(SRCDIR)/*.mm))
OBJS+=$(patsubst %.c,%.o,$(wildcard $(SRCDIR)/*.c))
OBJS+=$(patsubst %.cc,%.o,$(wildcard $(SRCDIR)/*.cc))

INFOPLIST:=$(wildcard *Info.plist)

//RESOURCES+=$(wildcard ./Assets/Account/*)
RESOURCES+=$(wildcard ./Assets/Appicon/*)
//RESOURCES+=$(wildcard ./Assets/Cards/*)
//RESOURCES+=$(wildcard ./Assets/Custom/*)
//RESOURCES+=$(wildcard ./Assets/Fonts/*)
RESOURCES+=$(wildcard ./Assets/Interests/*)
RESOURCES+=$(wildcard ./Assets/Logo/*)
//RESOURCES+=$(wildcard ./Assets/Misc/*)
//RESOURCES+=$(wildcard ./Assets/"NavBar Icon"/*)
//RESOURCES+=$(wildcard ./Assets/Notification/*)
RESOURCES+=$(wildcard ./Assets/Onboarding/*)
//RESOURCES+=$(wildcard ./Assets/TabIcon/*)

all:	$(PROJECTNAME)

$(PROJECTNAME):	$(OBJS)
	$(CPP) $(CPPFLAGS) $(LDFLAGS) $(filter %.o,$^) -o $@

%.o:	%.m
	$(CC) -c $(CFLAGS) $< -o $@

%.o:	%.mm
	$(CPP) -c $(CPPFLAGS) $< -o $@

%.o:	%.c
	$(CC) -c $(CFLAGS) $< -o $@

%.o:	%.cc
	$(CPP) -c $(CPPFLAGS) $< -o $@

dist:	$(PROJECTNAME)
	mkdir -p $(APPFOLDER)
ifneq ($(RESOURCES),)
	cp -r $(RESOURCES) $(APPFOLDER)
endif
	cp $(INFOPLIST) $(APPFOLDER)/Info.plist
	cp $(PROJECTNAME) $(APPFOLDER)
	find $(APPFOLDER) -name \*.png|xargs ios-pngcrush -c
	find $(APPFOLDER) -name \*.plist|xargs ios-plutil -c
	find $(APPFOLDER) -name \*.strings|xargs ios-plutil -c

langs:
	ios-genLocalization

install: dist
ifeq ($(IPHONE_IP),)
	echo "Please set IPHONE_IP"
else
	ssh root@$(IPHONE_IP) 'rm -fr /Applications/$(INSTALLFOLDER)'
	scp -r $(APPFOLDER) root@$(IPHONE_IP):/Applications/$(INSTALLFOLDER)
	echo "Application $(INSTALLFOLDER) installed"
	ssh mobile@$(IPHONE_IP) 'uicache'
endif

uninstall:
ifeq ($(IPHONE_IP),)
	echo "Please set IPHONE_IP"
else
	ssh root@$(IPHONE_IP) 'rm -fr /Applications/$(INSTALLFOLDER)'
	echo "Application $(INSTALLFOLDER) uninstalled"
endif
clean:
	find . -name \*.o|xargs rm -rf
	rm -rf $(APPFOLDER)

.PHONY: all dist install uninstall clean
