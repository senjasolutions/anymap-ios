//
//  ProfileViewController.h
//  AnyMap
//
//  Created by Antonio M on 23/10/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OverFlowMenuViewController.h"
@interface ProfileViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *coverPhoto;
@property (strong, nonatomic) IBOutlet UIImageView *avatarPhoto;
@property (strong, nonatomic) IBOutlet UILabel *counterProfile;
@property (strong, nonatomic) IBOutlet UILabel *counterFollowing;
@property (strong, nonatomic) IBOutlet UIButton *pickPhoto;
@property (strong, nonatomic) IBOutlet UIButton *doneButton;
@property (strong, nonatomic) IBOutlet UITextField *textName;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentIndex;
@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UIView *doneView;
@property (strong, nonatomic) IBOutlet UIView *moreView;

@property(strong,nonatomic)UIImage *cover;
@property(strong,nonatomic)UIImage *avatar;
@property(strong,nonatomic)NSString *nameS;
@property(strong,nonatomic)NSString *counterFollowerS;
@property(strong,nonatomic)NSString *counterFollowingS;
@property(strong,nonatomic)NSString *descriptionProfileS;
@end
