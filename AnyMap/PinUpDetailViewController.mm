//
//  PinUpDetailViewController.m
//  AnyMap
//
//  Created by Antonio M on 14/10/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import "AppDelegate.h"
#import "PinUpViewController.h"
#import "PinUpDetailViewController.h"
#import "PinUpCollectionViewCell.h"
#import "CommentViewController.h"
#import <OpenInGoogleMaps/OpenInGoogleMapsController.h>
#import "NetworkManager.h"
#import "LoginVC.h"
@implementation PinUpDetailViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    //initial value
    
    self.roadName.text = self.road;
    self.coverPhoto.image = self.cover;
    self.distance.text = self.distanceRoad;
    self.creatorPhoto.image = self.creator;
    [self.profileName setTitle:self.profile forState:UIControlStateNormal];
    self.titilePin.text = self.title;
    
    NSString *htmlString = _descript;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    UIFont *fontRoboto = [UIFont fontWithName:@"Roboto-Regular" size: 14.0]; // select needed font
    UIColor *colorDeepGrey = [UIColor colorWithRed:0.38 green:0.38 blue:0.38 alpha:1.0]; // select needed color
    [attributedString addAttribute:NSFontAttributeName value:fontRoboto range:NSMakeRange(0, [attributedString length])];
    [attributedString addAttribute:NSForegroundColorAttributeName value:colorDeepGrey range:NSMakeRange(0, [attributedString length])];
    
    self.descriptionMapTV.attributedText = attributedString;
    self.mapTitle.text = self.mapTitleS;
    self.likePin.selected = self.buttonState;
    [self.likePin setTitle:self.pinButtonValue forState:UIControlStateNormal];
    
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)like:(UIButton *)sender
{

    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"hasSignIn"])
    {
        sender.selected =!sender.selected;
        NetworkManager *network = [[NetworkManager alloc]init];
        
        if (sender.selected) {
            NSLog(@"like the pin");
            NSString *likePIN = [NSString stringWithFormat:@"%@|0",self.pinID];
            [network likeThePin:likePIN  statusPin:self.pinID];
            NSInteger counlike = [self.likePin.titleLabel.text integerValue] + 1 ;
            [sender setTitle:[NSString stringWithFormat:@"%ld",(long)counlike] forState:UIControlStateNormal];
        }
        else
        {
            NSLog(@"unlike the pin");
            NSString *unLikeThePin = [NSString stringWithFormat:@"%@|1",self.pinID];
             NSInteger counlike = [self.likePin.titleLabel.text integerValue] - 1 ;
            [sender setTitle:[NSString stringWithFormat:@"%ld",(long)counlike] forState:UIControlStateNormal];
            [network unLikeThePin:unLikeThePin  statusPin:self.pinID];
        }
    }
    else
    {
        [self infoNeedSignInToLikeThePin];
    }

}
- (void)infoNeedSignInToLikeThePin
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Information"
                                  message:@"Please signup or login to like this pin"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* no = [UIAlertAction
                         actionWithTitle:@"Cancel"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                         }];
    
    
    UIAlertAction*  yes = [UIAlertAction
                           actionWithTitle:@"Login/Signup"
                           style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * action)
                           {
                               
                               UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                               LoginVC *login = [main instantiateViewControllerWithIdentifier:@"LOGIN"];
                               [self presentViewController:login animated:YES completion:nil];
                           }];
    
    
    [alert addAction:no];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"hasSignIn"])
    {

        if ([segue.identifier isEqualToString:@"pinComment"]) {
            CommentViewController * childViewController = (CommentViewController *) [segue destinationViewController];
            childViewController.pinTitle = self.titilePin.text;
            childViewController.pinID = self.pinID;
        }

    }
    else
    {
        [self infoNeedSignInToBookmark];
    }

}
- (void)infoNeedSignInToBookmark
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Information"
                                  message:@"Please signup or login to bookmark this map"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* no = [UIAlertAction
                         actionWithTitle:@"Cancel"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                         }];
    
    
    UIAlertAction*  yes = [UIAlertAction
                           actionWithTitle:@"Login/Signup"
                           style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * action)
                           {
                               
                               UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                               LoginVC *login = [main instantiateViewControllerWithIdentifier:@"LOGIN"];
                               [self presentViewController:login animated:YES completion:nil];
                           }];
    
    
    [alert addAction:no];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}
- (IBAction)getDirection:(UIButton *)sender {
    NSLog(@"direction");
    NSLog(@"my desstination : %f, %f", self.latitude,self.longitude);
    
    GoogleDirectionsDefinition *definition = [[GoogleDirectionsDefinition alloc]init];
    definition.startingPoint = nil;
    
    definition.destinationPoint =  [GoogleDirectionsWaypoint waypointWithLocation:CLLocationCoordinate2DMake(self.latitude,self.longitude)];
    
    definition.travelMode = kGoogleMapsTravelModeDriving;
    
    [OpenInGoogleMapsController sharedInstance].fallbackStrategy   = kGoogleMapsFallbackSafari;
    
    [[OpenInGoogleMapsController sharedInstance] openDirections:definition];
 
}
- (IBAction)showReport:(id)sender {
    NSLog(@"report pin ");
    self.report = [[ReportViewController alloc]initWithNibName:@"OverflowMapTopic" bundle:nil];
    self.report.view.alpha = 0.0f;
    self.report.view.frame = CGRectMake(self.view.frame.size.width - (self.report.view.frame.size.width +16.0),20.0f, self.report.view.frame.size.width, self.report.view.frame.size.height);
    [self.view insertSubview:self.report.view aboveSubview:self.view];
    self.report.delegateOverFlowMap = self;
    [UIView animateWithDuration:0.2f animations:^{
        self.report.view.alpha =1.0f;
    }completion:^(BOOL finished){}];
}
- (void)showEdit
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
       
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"hasSignIn"])
        {
            NetworkManager *network =[[NetworkManager alloc]init];
            bool statusReport =    [network addLog:@"pin_id" addLogID:self.pinID];
            if (statusReport) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertView *alert= [[UIAlertView alloc ]initWithTitle:@"Information" message:@"This pin has been reported" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
                    [alert show];
                });
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertView *alert= [[UIAlertView alloc ]initWithTitle:@"Information" message:@"failed to report" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
                    [alert show];
                });
            }
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information"
                                                                message:@"Please signup or login to report this pin"
                                                               delegate:self
                                                      cancelButtonTitle:nil
                                                      otherButtonTitles:@"Cancel",@"Login/Signup" ,nil];
                [alert show];
            });
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self hideOverFlow];
        });

    });
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 1) {
        NSLog(@"nilai parent VC : %@",self.parentViewController.parentViewController);
        LoginVC *login = [self.storyboard instantiateViewControllerWithIdentifier:@"LOGIN"];
        [self presentViewController:login animated:YES completion:nil];
    }
    else if (buttonIndex == 0)
    {
        
        
    }
}
- (void)hideOverFlow{
    [UIView animateWithDuration:0.2f animations:^{
        self.report.view.alpha =0.0f;
    }completion:^(BOOL finished){
        [self.report.view removeFromSuperview];
    }];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NetworkManager *network = [[NetworkManager alloc]init];
    NSMutableArray *arrayComments = [network geTheCommentList:self.pinID];
    
    [self.addComment setTitle:[NSString stringWithFormat:@"Comments(%lu)",(unsigned long)arrayComments.count] forState:UIControlStateNormal];
    
}
@end
