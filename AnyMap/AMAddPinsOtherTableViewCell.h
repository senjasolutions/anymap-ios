//
//  AMAddPinsOtherTableViewCell.h
//  AnyMap
//
//  Created by Antonio M on 25/01/16.
//  Copyright © 2016 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AMAddPinsOtherTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *photoImagePins;
@property (strong, nonatomic) IBOutlet UILabel *pinTitle;
@property (strong, nonatomic) IBOutlet UILabel *pinDescription;
@property (strong, nonatomic) IBOutlet UIButton *pinLikes;

@end
