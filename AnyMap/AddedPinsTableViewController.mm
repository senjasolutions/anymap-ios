//
//  AddedPinsTableViewController.m
//  AnyMap
//
//  Created by Antonio M on 09/11/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//


#import "AppDelegate.h"
#import "AddedPinsTableViewController.h"
#import "CellAddedPinsTableViewCell.h"

@interface AddedPinsTableViewController ()

@end

@implementation AddedPinsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    CGFloat ratio = 0;
    if (screenHeight == 480.0f)
    {
        ratio = 178;
    }
    else if (screenHeight == 568.0f)
    {
        ratio = 178;
    }
    else if (screenHeight == 667.0f)
    {
        ratio =178;
    }
    else if (screenHeight == 736.0f)
    {
        ratio = 178;
    }
    self.tableView.rowHeight = ratio;
    
    self.arrayAddedPins = [[[NSUserDefaults standardUserDefaults] objectForKey:@"newPin"] mutableCopy];
    NSLog(@"my pins: %@", self.arrayAddedPins);
    [self.tableView reloadData];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.arrayAddedPins = [[[NSUserDefaults standardUserDefaults] objectForKey:@"newPin"] mutableCopy];
    [self.tableView reloadData];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{    
    return self.arrayAddedPins.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CellAddedPinsTableViewCell *cell = (CellAddedPinsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"celladdpins"];
    if (!cell) {
        NSLog(@"cell pins ");
        [tableView registerNib:[UINib nibWithNibName:@"CardAddedPinsPhotoCell" bundle:nil] forCellReuseIdentifier:@"celladdpins"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"celladdpins" forIndexPath:indexPath];
    }
    cell.mapTitle.text = @"MAP TITLE";
    cell.pinTitle.text = [[self.arrayAddedPins objectAtIndex:indexPath.row] valueForKey:@"title"];
    //    cell.closePin
    if(![[[self.arrayAddedPins objectAtIndex:indexPath.row] valueForKey:@"filePath"] isEqualToString:@""]){
        //        NSString *theImagePath = [[self.arrayAddedPins objectAtIndex:indexPath.row] valueForKey:@"filePath"];
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString  *imagePath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/%@", [[self.arrayAddedPins objectAtIndex:indexPath.row] valueForKey:@"filePath"]]];
                UIImage *customImage = [UIImage imageWithContentsOfFile:imagePath];
                //        NSLog(@"image path: %@", theImagePath);
                NSLog(@"image path2: %@", imagePath);
                cell.pinOwner.image = customImage;
            });
        });
    } else {
        cell.pinOwner.image = [UIImage imageNamed:@"placeholder_pic_big"];
    }
    
    cell.pinDescription.text = [[self.arrayAddedPins objectAtIndex:indexPath.row] valueForKey:@"description"];    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
