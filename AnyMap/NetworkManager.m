//
//  NetworkManager.m
//  AnyMap
//
//  Created by Antonio M on 14/01/16.
//  Copyright © 2016 AnyMap. All rights reserved.
//

#import "NetworkManager.h"

@implementation NetworkManager

- (void)installGlobalDevice
{
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"alreadyInstalled"]) {
        NSString *uuid = [[NSUUID UUID] UUIDString];
        NSString *appVersion=  [NSString stringWithFormat:@"%@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
        NSLocale *currentLocale = [NSLocale currentLocale];  // get the current locale.
        NSString *countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
//        NSLog(@"country code : %@",countryCode);
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.completionQueue =  dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
       AFHTTPRequestOperation  *operation = [manager POST:[NSString stringWithFormat:@"%@users/register-install",APIURL] parameters:@{@"install_id":uuid,@"os":@"ios",@"register_country":countryCode,@"app_version":appVersion} success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"install global device : %@", responseObject);
            [[NSUserDefaults standardUserDefaults] setObject:responseObject forKey:@"installDevice"];
            [[NSUserDefaults standardUserDefaults] setObject:@"installed" forKey:@"alreadyInstalled"];
            dispatch_semaphore_signal(semaphore);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
             dispatch_semaphore_signal(semaphore);
        }];
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        [operation waitUntilFinished];
        
    }
}
- (BOOL)loginFb:(NSString *)facebookId token:(NSString *)tokenFB facebookName:(NSString *)name
{   __block BOOL loginStatus ;
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    NSLog(@"name parse : %@",name);
    __block NSString *nameFromFB = name;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.completionQueue =  dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    AFHTTPRequestOperation  *operation = [manager POST:[NSString stringWithFormat:@"%@users/facebook-login",APIURL] parameters:@{@"facebook_id":facebookId,@"token":tokenFB} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"login Facebook Status : %@", responseObject);
        
        if ([[responseObject valueForKey:@"login_status"]isEqualToString:@"fail"]) {
            loginStatus = NO;
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"hasSignIn"];
            NSUserDefaults *standar = [[NSUserDefaults alloc]init];
            loginStatus = YES;
            NSString *profileImage =@"";
            if ([responseObject valueForKey:@"profile_image"] == (id)[NSNull null]) {
                profileImage= @"http://nil";
                //                NSLog(@"image is empty");
            }
            else if(![responseObject valueForKey:@"profile_image"])
            {
                profileImage= @"http://nil";
                
                //                NSLog(@"there is image in there");
            }
            else
            {
                profileImage =[responseObject valueForKey:@"profile_image"] ;
            }
            NSString *no_following = [[NSString alloc]init];
            NSString *no_of_follower =[[NSString alloc]init];
            if (![responseObject valueForKey:@"following"]|| [responseObject valueForKey:@"follower"]== (id)[NSNull null]) {
                
                no_following = @"0";
                no_of_follower =@"0";
            }
            
            else
            {
                no_following =[responseObject valueForKey:@"following"];
                no_of_follower =[responseObject valueForKey:@"follower"];
            }
            NSString *installID = [[standar objectForKey:@"installDevice" ]objectForKey:@"install_id"];
            //            NSLog(@"nilai install id  : %@",installID);
            //            NSLog(@"api key baru  : %@",[loginResponse valueForKey:@"api_key"]);
            //            NSLog(@"name  : %@",[loginResponse valueForKey:@"name"]);
            //            NSLog(@"profile image  : %@",[loginResponse valueForKey:@"profile_image"]);
            [standar setObject:[NSMutableDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[responseObject valueForKey:@"api_key"],[responseObject valueForKey:@"id"],@"ok",installID,nameFromFB
                                                                           , profileImage,no_following,no_of_follower, nil] forKeys:[NSArray arrayWithObjects:@"api_key",@"id",@"install_status",@"install_id",@"name",@"profile_image",@"following",@"follower",nil]] forKey:@"installDevice"];
            [standar synchronize];
            [self maps_by_followed];
            NSString *empty =[[NSString alloc]init];
            
            //just update the profile ,not change the information when try to login
            NSLog(@"name from fb : %@",nameFromFB);
            [self editProfile:nameFromFB imageURL:empty imagePath:[UIImage imageNamed:@""]];
            
        }
        dispatch_semaphore_signal(semaphore);

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        loginStatus = NO;
        dispatch_semaphore_signal(semaphore);
    }];
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    [operation waitUntilFinished];
    return loginStatus;
}

- (BOOL)loginbyAM: (NSString* )email  password:(NSString* )pass
{
    __block BOOL loginStatus ;
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.completionQueue =  dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    AFHTTPRequestOperation  *operation = [manager POST:[NSString stringWithFormat:@"%@users/login",APIURL] parameters:@{@"email":email,@"password":pass} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"login AM : %@", responseObject);
        NSArray *loginResponse = (NSArray *)responseObject;
       
        if ([[loginResponse valueForKey:@"login_status"]isEqualToString:@"fail"]) {
            loginStatus = NO;
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"hasSignIn"];
            NSUserDefaults *standar = [[NSUserDefaults alloc]init];
            loginStatus = YES;
            NSString *profileImage =@"";
            if ([loginResponse valueForKey:@"profile_image"] == (id)[NSNull null] ||![loginResponse valueForKey:@"profile_image"]) {
                profileImage= @"http://nil";
//                NSLog(@"image is empty");
            }
            else
            {
                profileImage =[loginResponse valueForKey:@"profile_image"] ;
            }
            NSString *no_following = [[NSString alloc]init];
            NSString *no_of_follower =[[NSString alloc]init];
            if (![loginResponse valueForKey:@"following"]|| [loginResponse valueForKey:@"follower"]== (id)[NSNull null]) {
                
                no_following = @"0";
                no_of_follower =@"0";
            }

            else
            {
                no_following =[loginResponse valueForKey:@"following"];
                no_of_follower =[loginResponse valueForKey:@"follower"];
            }
            NSString *installID = [[standar objectForKey:@"installDevice" ]objectForKey:@"install_id"];
            [standar setObject:[NSMutableDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[loginResponse valueForKey:@"api_key"],[loginResponse valueForKey:@"id"],@"ok",installID,[loginResponse valueForKey:@"name"]
                                                                           , profileImage,no_following,no_of_follower, nil] forKeys:[NSArray arrayWithObjects:@"api_key",@"id",@"install_status",@"install_id",@"name",@"profile_image",@"following",@"follower",nil]] forKey:@"installDevice"];
            [standar synchronize];
            [self maps_by_followed];
            NSString *empty =[[NSString alloc]init];
            //just update the profile ,not change the information when try to login 
            [self editProfile:[loginResponse valueForKey:@"name"] imageURL:empty imagePath:[UIImage imageNamed:@""]];

        }
               dispatch_semaphore_signal(semaphore);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        loginStatus = NO;
        dispatch_semaphore_signal(semaphore);
    }];
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    [operation waitUntilFinished];
    return loginStatus;
}
-(BOOL)signUpByAM:(NSString *)email password:(NSString *)pass username:(NSString *)user
{
    
    NSString *uuid = [[NSUUID UUID] UUIDString];
    NSLocale *currentLocale = [NSLocale currentLocale];  // get the current locale.
    NSString *countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
    __block BOOL signStatus ;
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.completionQueue =  dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    AFHTTPRequestOperation  *operation = [manager POST:[NSString stringWithFormat:@"%@users/signup",APIURL] parameters:@{@"install_id":uuid,@"os":@"ios",@"register_country":countryCode,@"email":email,@"password":pass,@"name":user} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *signUpResponse = (NSArray *)responseObject;
//        NSLog(@"nilai sign up : %@",signUpResponse);
        if ([[signUpResponse valueForKey:@"signup_status"] isEqualToString:@"fail"]) {
        signStatus = NO;
        }
        else{
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"hasSignIn"];
            NSUserDefaults *standar = [[NSUserDefaults alloc]init];
            NSString *installID = [[standar objectForKey:@"installDevice" ]objectForKey:@"install_id"];
//            NSLog(@"nilai install id  : %@",installID);
//            NSLog(@"api key baru  : %@",[signUpResponse valueForKey:@"api_key"]);
//            NSLog(@"name  : %@",[signUpResponse valueForKey:@"name"]);
//            NSLog(@"profile image  : %@",[signUpResponse valueForKey:@"profile_image"]);
//            
            NSString *profileImage =@"";
            if ([signUpResponse valueForKey:@"profile_image"] == (id)[NSNull null]) {
                profileImage= @"http://nil";
                NSLog(@"image is empty");
            }
            else if(![signUpResponse valueForKey:@"profile_image"])
            {
                profileImage= @"http://nil";
                
                NSLog(@"there is image in there");
            }
            else
            {
                profileImage =[signUpResponse valueForKey:@"profile_image"] ;
            }


            [standar setObject:[NSMutableDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[signUpResponse valueForKey:@"api_key"],[signUpResponse valueForKey:@"id"],@"ok",installID,[signUpResponse valueForKey:@"name"]
                                                                           , profileImage,nil] forKeys:[NSArray arrayWithObjects:@"api_key",@"id",@"install_status",@"install_id",@"name",@"profile_image",nil]] forKey:@"installDevice"];
            [standar synchronize];
            signStatus = YES;
        }
        
        dispatch_semaphore_signal(semaphore);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        signStatus = NO;
        dispatch_semaphore_signal(semaphore);
    }];
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    [operation waitUntilFinished];
    return signStatus;

}
-(BOOL)signUpByFacebook:(NSString *)email facebookID:(NSString *)fbID username:(NSString *)user tokenFB:(NSString *)token
{
    NSString *uuid = [[NSUUID UUID] UUIDString];
    NSLocale *currentLocale = [NSLocale currentLocale];  // get the current locale.
    NSString *countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
    __block BOOL signStatus ;
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.completionQueue =  dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    AFHTTPRequestOperation  *operation = [manager POST:[NSString stringWithFormat:@"%@users/facebook-signup",APIURL] parameters:@{@"install_id":uuid,@"os":@"ios",@"register_country":countryCode,@"email":email,@"facebook_id":fbID,@"token":token} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *signUpResponse = (NSArray *)responseObject;
        NSLog(@"nilai sign up : %@",signUpResponse);
        if ([[signUpResponse valueForKey:@"signup_status"] isEqualToString:@"fail"]) {
            signStatus = NO;
        }
        else{
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"hasSignIn"];
            NSUserDefaults *standar = [[NSUserDefaults alloc]init];
            NSString *installID = [[standar objectForKey:@"installDevice" ]objectForKey:@"install_id"];
            NSLog(@"nilai install id  : %@",installID);
            NSLog(@"api key baru  : %@",[signUpResponse valueForKey:@"api_key"]);
            NSLog(@"name  : %@",user);
            NSLog(@"profile image  : %@",[signUpResponse valueForKey:@"profile_image"]);
            
            NSString *profileImage =@"";
            if ([signUpResponse valueForKey:@"profile_image"] == (id)[NSNull null]) {
                profileImage= @"http://nil";
//                NSLog(@"image is empty");
            }
            else if(![signUpResponse valueForKey:@"profile_image"])
            {
                profileImage= @"http://nil";
                
//                NSLog(@"there is image in there");
            }
            else
            {
                profileImage =[signUpResponse valueForKey:@"profile_image"] ;
            }
            
            
            [standar setObject:[NSMutableDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[signUpResponse valueForKey:@"api_key"],[signUpResponse valueForKey:@"id"],@"ok",installID,user
                                                                           , profileImage,nil] forKeys:[NSArray arrayWithObjects:@"api_key",@"id",@"install_status",@"install_id",@"name",@"profile_image",nil]] forKey:@"installDevice"];
            [standar synchronize];
            signStatus = YES;
        }
        
        dispatch_semaphore_signal(semaphore);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        NSLog(@"Error: %@", error);
        signStatus = NO;
        dispatch_semaphore_signal(semaphore);
    }];
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    [operation waitUntilFinished];
    return signStatus;

}

- (NSMutableArray *)maps_by_response:(NSString *)latitude longitude:(NSString *)point
{
    NSLocale *currentLocale = [NSLocale currentLocale];  // get the current locale.
    NSString *countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
    NSString *result = [[NSString alloc]init];
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"category"]) {
        
          NSMutableArray *category = [[NSUserDefaults standardUserDefaults]objectForKey:@"category"];
        
        result = [category componentsJoinedByString:@","];
    }
    else
    {
        result = @"";
    }
    
    __block NSMutableArray *arrayTemp;
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.completionQueue =  dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    // header("Content-Type: application/json");
    //    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSUserDefaults *identifier = [[NSUserDefaults alloc]init];
    AFHTTPRequestOperation  *operation = [manager GET:[NSString stringWithFormat:@"%@maps/maps-by-mode",APIURL ] parameters:@{@"user_id":[[identifier objectForKey:@"installDevice"] objectForKey:@"id"],@"access-token":[[identifier objectForKey:@"installDevice"] objectForKey:@"api_key"],@"mode":@"0",@"lat":latitude,@"lon":point,@"pin_radius":@"20",@"pin_limit":@"5",@"map_limit":@"5",@"offset":@"0",@"category_ids":result,@"locale":countryCode} success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"responseObject %@",responseObject);
        
        NSString *jsonString =  [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        
        NSString *newJsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\'" withString:@""];
        
        /*
         NSRange range = [jsonString rangeOfString:@"}" options:NSBackwardsSearch];
         jsonString = [jsonString substringToIndex:range.location + 1];
         */
        NSData *data = [newJsonString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSError *error;
        NSMutableArray *array = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        
        NSLog(@"array %@",array);
        arrayTemp = [array mutableCopy];
        
        if (!array) {
            NSLog(@"Parsing JSON failed: %@", error);
            arrayTemp = nil;
        }
        
        /*
         NSData *newJSONData = [newJsonString dataUsingEncoding:NSUTF8StringEncoding];
         NSDictionary* json = [NSJSONSerialization
         JSONObjectWithData:newJSONData
         options:NSJSONReadingMutableContainers
         error:&error];
         NSLog(@"json %@",json);
         */
        
        //        NSLog(@"responseObject = %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);

        dispatch_semaphore_signal(semaphore);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //        NSLog(@"Error tes: %@", error);
        //        NSLog(@"Error object: %@", operation.responseObject);
        dispatch_semaphore_signal(semaphore);
    }];
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    [operation waitUntilFinished];
    return arrayTemp;

}

- (void)saveTheBookMarkLocally:(NSMutableArray *)map
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSMutableArray *arrayPins =  [map valueForKey:@"pins"];
//    NSMutableArray *arr = [arrayPins mutableCopy];
    NSMutableArray *arrayTemp = [[NSMutableArray alloc]init];
    NSArray *arrayDic= [NSArray arrayWithObject:@{
                                                  @"id":[map valueForKey:@"id"],
                                                  @"created_by_id":[map valueForKey:@"created_by_id"],
                                                  @"title":[map valueForKey:@"title"],
                                                  @"description": [map valueForKey:@"description"],
                                                  @"profile_name":[map valueForKey:@"profile_name"],
                                                  @"profile_image":[map valueForKey:@"profile_image"],
                                                  @"image": [map valueForKey:@"image"],
                                                  @"no_of_pins":[map valueForKey:@"no_of_pins"],
                                                  @"no_of_followers":[map valueForKey:@"no_of_followers"],
                                                  @"created_at":[map valueForKey:@"created_at"],
                                                  @"is_private":[map valueForKey:@"is_private"],
                                                  @"is_featured":[map valueForKey:@"is_featured"],
                                                  @"is_following":[map valueForKey:@"is_following"],
                                                  }];
    
    
    
//    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:map];
    
    
    if ([defaults objectForKey:@"bookmarkedMaps"]) {
        arrayTemp = [[defaults objectForKey:@"bookmarkedMaps"] mutableCopy];
        [arrayTemp addObject:[arrayDic lastObject]];
//        [arrayTemp addObject:data];
        
        [defaults setObject:arrayTemp forKey:@"bookmarkedMaps"];
//        [defaults setObject:data forKey:[map valueForKey:@"id"]];
        [defaults synchronize];
        
//        NSData *dataPins = [defaults objectForKey:[map valueForKey:@"id"]];
//        NSArray *arrloadTheData = [NSKeyedUnarchiver unarchiveObjectWithData:dataPins];
        
        NSLog(@"second data : %@ ",[defaults objectForKey:@"bookmarkedMaps"]);
    }
    else
    {
        [defaults setObject:arrayDic forKey:@"bookmarkedMaps"];
//        [defaults setObject:data forKey:[map valueForKey:@"id"]];
        [defaults synchronize];
        
//        NSData *dataPins = [defaults objectForKey:[map valueForKey:@"id"]];
//        NSArray *arrloadTheData = [NSKeyedUnarchiver unarchiveObjectWithData:dataPins];
        
        NSLog(@"First data : %@ ",[defaults objectForKey:@"bookmarkedMaps"]);
        
    }
 
}

- (void)bookmarkMap :(NSString *)mapID statusMap:(NSString *)map arrayMap:(NSMutableArray *)maps
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"hasSignIn"])
    {
        NSUserDefaults *identifier = [[NSUserDefaults alloc]init];
        [self saveTheBookMarkLocally:maps];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            [manager POST:[NSString stringWithFormat:@"%@maps/followed-map?user_id=%@&access-token=%@",APIURL,[[identifier objectForKey:@"installDevice"] objectForKey:@"id"],[[identifier objectForKey:@"installDevice"] objectForKey:@"api_key"]] parameters:@{@"followed_map_ids":mapID } success:^(AFHTTPRequestOperation *operation, id responseObject) {
//                NSLog(@"json followed map news : %@",responseObject);
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                NSLog(@"Error: %@", operation.responseObject);
            }];
        
    }
}
-(void)unBookmarkMap:(NSString *)mapID statusMap:(NSString *)map arrayMap:(NSMutableArray *)maps
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"hasSignIn"])
    {
        NSUserDefaults *identifier = [[NSUserDefaults alloc]init];
//        [identifier removeObjectForKey:@"bookmarkedMaps"];
        if ([[NSUserDefaults standardUserDefaults]objectForKey:@"bookmarkedMaps"]) {
            NSMutableArray *arrayTempBookmarkedMaps =  [[identifier objectForKey:@"bookmarkedMaps"] mutableCopy];
            NSMutableArray *arrayTemp = [arrayTempBookmarkedMaps mutableCopy];
//            NSLog(@"pref bookmarked map : %@",arrayTempBookmarkedMaps);

         
            for (int i =0 ; i < [arrayTemp count]; i++) {
                NSString *value = [arrayTemp objectAtIndex:i];
                
                if ([[value valueForKey:@"id"] isEqualToString:map]) {
                    
                    NSLog(@"tes");
                    [arrayTempBookmarkedMaps removeObjectAtIndex:i];
                    break;
                }
            
            }
            if ([arrayTempBookmarkedMaps count] == 0) {
                [identifier removeObjectForKey:@"bookmarkedMaps"];
                [identifier synchronize];
                NSLog(@"delete the key");
            }
            else
            {
//                NSLog(@"delete by one");
                [identifier setObject:arrayTempBookmarkedMaps forKey:@"bookmarkedMaps"];
                [identifier synchronize];
            }
        }
        else
        {
            //do nothing

        }
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager POST:[NSString stringWithFormat:@"%@maps/followed-map?user_id=%@&access-token=%@",APIURL,[[identifier objectForKey:@"installDevice"] objectForKey:@"id"],[[identifier objectForKey:@"installDevice"] objectForKey:@"api_key"]] parameters:@{@"followed_map_ids":mapID } success:^(AFHTTPRequestOperation *operation, id responseObject) {
//            NSLog(@"unbookmarkmap : %@",responseObject);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//            NSLog(@"Error: %@", operation.responseObject);
        }];
        
    }

}

- (void)likeThePin:(NSString *)pinID statusPin:(NSString *)map
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"hasSignIn"])
    {
        NSUserDefaults *identifier = [[NSUserDefaults alloc]init];
        if ([[NSUserDefaults standardUserDefaults]objectForKey:@"likeThePinMaps"]) {
//            NSLog(@"enter in to add the pin Likes");
            NSMutableArray *arrayTempBookmarkedMaps = [[NSMutableArray alloc]init];
            arrayTempBookmarkedMaps = [[identifier objectForKey:@"likeThePinMaps"] mutableCopy];
//            NSLog(@"like the pin map : %@" ,arrayTempBookmarkedMaps);
            [arrayTempBookmarkedMaps addObject:map];
            [identifier setObject:arrayTempBookmarkedMaps forKey:@"likeThePinMaps"];
            [identifier synchronize];
        }
        else
        {
            
//            NSLog(@"new pin like");
            [identifier setObject:[NSArray arrayWithObject:map] forKey:@"likeThePinMaps"];
            [identifier synchronize];
        }
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager POST:[NSString stringWithFormat:@"%@pins/pin-like?user_id=%@&access-token=%@",APIURL,[[identifier objectForKey:@"installDevice"] objectForKey:@"id"],[[identifier objectForKey:@"installDevice"] objectForKey:@"api_key"]] parameters:@{@"liked_pin_ids":pinID } success:^(AFHTTPRequestOperation *operation, id responseObject) {
//            NSLog(@"json like the pins : %@",responseObject);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//            NSLog(@"Error: %@", operation.responseObject);
        }];
        
    }

}
-(void)unLikeThePin:(NSString *)pinID statusPin:(NSString *)map
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"hasSignIn"])
    {
        NSUserDefaults *identifier = [[NSUserDefaults alloc]init];
        //        [identifier removeObjectForKey:@"bookmarkedMaps"];
        if ([[NSUserDefaults standardUserDefaults]objectForKey:@"likeThePinMaps"]) {
            NSMutableArray *arrayTempBookmarkedMaps =  [[identifier objectForKey:@"likeThePinMaps"] mutableCopy];
            NSMutableArray *arrayTemp = [arrayTempBookmarkedMaps mutableCopy];
//            NSLog(@"pref pins  : %@",arrayTempBookmarkedMaps);
            
            
            for (int i =0 ; i < [arrayTemp count]; i++) {
                if ([[arrayTemp objectAtIndex:i]isEqualToString:map]) {
                    [arrayTempBookmarkedMaps removeObjectAtIndex:i];
                    break;
                }
            }
            if ([arrayTempBookmarkedMaps count] == 0) {
                [identifier removeObjectForKey:@"likeThePinMaps"];
                [identifier synchronize];
//                NSLog(@"delete the key pin");
            }
            else
            {
//                NSLog(@"delete by one pin");
                [identifier setObject:arrayTempBookmarkedMaps forKey:@"likeThePinMaps"];
                [identifier synchronize];
            }
        }
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager POST:[NSString stringWithFormat:@"%@pins/pin-like?user_id=%@&access-token=%@",APIURL,[[identifier objectForKey:@"installDevice"] objectForKey:@"id"],[[identifier objectForKey:@"installDevice"] objectForKey:@"api_key"]] parameters:@{@"liked_pin_ids":pinID } success:^(AFHTTPRequestOperation *operation, id responseObject) {
//            NSLog(@"unlike the pin  : %@",responseObject);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//            NSLog(@"Error: %@", operation.responseObject);
        }];
        
    }

}
 -(NSMutableArray * )geTheCommentList:(NSString *)pinID
{
       __block NSMutableArray *arrayTemp;
         NSUserDefaults *identifier = [[NSUserDefaults alloc]init];
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.completionQueue =  dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
          AFHTTPRequestOperation  *operation =   [manager GET:[NSString stringWithFormat:@"%@pins/comments?user_id=%@&access-token=%@&pin_id=%@",APIURL,[[identifier objectForKey:@"installDevice"] objectForKey:@"id"],[[identifier objectForKey:@"installDevice"] objectForKey:@"api_key"],pinID] parameters:nil
                                                                                                                                                                        success:^(AFHTTPRequestOperation *operation, id responseObject) {
                NSLog(@"get the comment list  : %@",responseObject);
                                                                                                                                                                            
                                                                                                                                                                            arrayTemp = (NSMutableArray *)responseObject;
                                                                                                                                                                                 dispatch_semaphore_signal(semaphore);
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                NSLog(@"Error: %@", operation.responseObject);
                     dispatch_semaphore_signal(semaphore);
            }];
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    [operation waitUntilFinished];
    return arrayTemp;

}

-(NSMutableArray * )geTheAddedPins
{
    __block NSMutableArray *arrayTemp;

    return arrayTemp;
    
}
-(void)addTheComment:(NSString *)pinID comment:(NSString *)commmentList
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"hasSignIn"])
    {
            int  i =  arc4random() %(100000000000000)-1;
                NSUserDefaults *identifier = [[NSUserDefaults alloc]init];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager POST:[NSString stringWithFormat:@"%@pins/add-comment?user_id=%@&access-token=%@",APIURL,[[identifier objectForKey:@"installDevice"] objectForKey:@"id"],[[identifier objectForKey:@"installDevice"] objectForKey:@"api_key"]] parameters:@{@"comment_id":[NSString stringWithFormat:@"%d",i],@"comment":commmentList,@"pin_id":pinID} success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"pin respon add comment  : %@",responseObject);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//            NSLog(@"Error: %@", operation.responseObject);
        }];
    }
}
- (void)maps_by_followed
{
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.completionQueue =  dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);

    AFHTTPRequestOperation  *operation = [manager GET:[NSString stringWithFormat:@"%@maps/followed-maps-by-user?user_id=%@&access-token=%@&profile_id=%@" ,APIURL,[[[NSUserDefaults standardUserDefaults]objectForKey:@"installDevice"] objectForKey:@"id"],[[[NSUserDefaults standardUserDefaults]objectForKey:@"installDevice"]objectForKey:@"api_key"],[[[NSUserDefaults standardUserDefaults]objectForKey:@"installDevice"] objectForKey:@"id"]] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"map followed: %@", responseObject);
        NSMutableArray *  arrayTemp = (NSMutableArray *)responseObject;
        if ([arrayTemp count] !=0) {
            NSLog(@"save the bookmarked map");
            [[NSUserDefaults standardUserDefaults] setObject:arrayTemp forKey:@"bookmarkedMaps"
             ];
        }
        dispatch_semaphore_signal(semaphore);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        NSLog(@"Error tes: %@", error);
//        NSLog(@"Error object: %@", operation.responseObject);
        dispatch_semaphore_signal(semaphore);
    }];
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    [operation waitUntilFinished];
}
- (void)updateGCM:(NSString*)token
{

        NSUserDefaults *identifier = [[NSUserDefaults alloc]init];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:[NSString stringWithFormat:@"%@users/update-gcm?user_id=%@&access-token=%@",APIURL,[[identifier objectForKey:@"installDevice"] objectForKey:@"id"],[[identifier objectForKey:@"installDevice"] objectForKey:@"api_key"]] parameters:@{@"gcm_id":token} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"update the GCM  : %@",responseObject);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//            NSLog(@"Error: %@", operation.responseObject);
        }];

}

- (NSString *)addPin:(NSString *)mapID titlePin:(NSString *)title descriptionPin:(NSString *)description latitudePin:(NSString *)latitude longitudePin:(NSString *)longitude addressPin:(NSString *)address pathImagePin:(NSString *)filePath imagePin:(UIImage *)image
{
   __block NSString *info = @"";
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"hasSignIn"])
    {
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
        
        NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
        
        
        [self savePinLocally:mapID titlePin:title descriptionPin:description latitudePin:latitude longitudePin:longitude addressPin:address pathImagePin:filePath imagePin:image];
        AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]init];
        
        manager.completionQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        
        NSUserDefaults *identifier = [[NSUserDefaults alloc]init];
        
        AFHTTPRequestOperation *operation = [manager POST:[NSString stringWithFormat:@"%@pins/add-pin?user_id=%@&access-token=%@",APIURL,[[identifier objectForKey:@"installDevice"] objectForKey:@"id"],[[identifier objectForKey:@"installDevice"] objectForKey:@"api_key"]]
                                               parameters:@{@"map_id": mapID,
                                                            @"title": title,
                                                            @"description": description,
                                                            @"latitude":latitude,
                                                            @"longitude":longitude,
                                                            @"address":address
                                                            }
                                             
                                constructingBodyWithBlock:^(id <AFMultipartFormData> formData) {
                                    [formData appendPartWithFileData:imageData
                                                                name:@"pin_image"
                                                            fileName:filePath
                                                            mimeType:@"image/jpeg"];
                                }
                                                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                      if (operation.isFinished) {
                                                          
                                                      }
//                                                      NSLog(@"Success upload the image: %@", responseObject);
                                                      
                                                      info = (NSString *)responseObject;
                                                      
                                                      dispatch_semaphore_signal(semaphore);
                                                  } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                                                      NSLog(@"Error: %@ ***** %@", operation.responseString, error);
//                                                        NSLog(@"Error uploading image: %@ ***** %@ ***** %@", operation.responseString, error,operation.responseObject);
                                                      info = (NSString *)operation.responseObject;
                                                      dispatch_semaphore_signal(semaphore);
                                                  }];
        
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        
        [operation waitUntilFinished];
    }
    return info;
}
- (NSString *)editProfile:(NSString *)name imageURL:(NSString *)imagePath imagePath:(UIImage *)image
{
    __block NSString *info = @"";
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"hasSignIn"])
    {
       
        
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
        
        NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
        
        AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]init];
        
        manager.completionQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        
        NSUserDefaults *identifier = [[NSUserDefaults alloc]init];
        if (imagePath.length == 0) {
            AFHTTPRequestOperation *operation = [manager POST:[NSString stringWithFormat:@"%@users/edit-profile?user_id=%@&access-token=%@",APIURL,[[identifier objectForKey:@"installDevice"] objectForKey:@"id"],[[identifier objectForKey:@"installDevice"] objectForKey:@"api_key"]]
                                                   parameters:@{@"name":name
                                                                }
                                                      success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                          if (operation.isFinished) {
                                                              
                                                          }
#if DEBUG
                                                          NSLog(@"update profile: %@", responseObject);
#endif
                                                          [self responseObjectRetrive:responseObject];
                                                          info = (NSString *)responseObject;
                                                          
                                                          dispatch_semaphore_signal(semaphore);
                                                      } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                          NSLog(@"Error: %@ ***** %@", operation.responseString, error);
                                                          NSLog(@"Error uploading image: %@ ***** %@ ***** %@", operation.responseString, error,operation.responseObject);
                                                          info = (NSString *)operation.responseObject;
                                                          dispatch_semaphore_signal(semaphore);
                                                      }];
            
            dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
            
            [operation waitUntilFinished];
            
        }
        else
        {
            
            
            AFHTTPRequestOperation *operation = [manager POST:[NSString stringWithFormat:@"%@users/edit-profile?user_id=%@&access-token=%@",APIURL,[[identifier objectForKey:@"installDevice"] objectForKey:@"id"],[[identifier objectForKey:@"installDevice"] objectForKey:@"api_key"]]
                                                   parameters:@{@"name":name
                                                                }
                                    constructingBodyWithBlock:^(id <AFMultipartFormData> formData) {
                                        [formData appendPartWithFileData:imageData
                                                                    name:@"profile_image"
                                                                fileName:imagePath
                                                                mimeType:@"image/jpeg"];
                                    }
                                                      success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                          if (operation.isFinished) {
                                                              
                                                          }
#if DEBUG
                                                          NSLog(@"update profile: %@", responseObject);
#endif
                                                          [self responseObjectRetrive:responseObject];
                                                          info = (NSString *)responseObject;
                                                          
                                                          dispatch_semaphore_signal(semaphore);
                                                      } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
#if DEBUG
                                                          NSLog(@"Error: %@ ***** %@", operation.responseString, error);
#endif
                                                          NSLog(@"Error uploading image: %@ ***** %@ ***** %@", operation.responseString, error,operation.responseObject);
                                                          info = (NSString *)operation.responseObject;
                                                          dispatch_semaphore_signal(semaphore);
                                                      }];
            
            dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
            
            [operation waitUntilFinished];
        }
    }
    return info;

}

- (void)responseObjectRetrive:(id)responseObject
{
    NSString *profileImage =@"";
    if ([responseObject valueForKey:@"image"] == (id)[NSNull null]) {
        profileImage= @"http://nil";
        NSLog(@"image is empty");
    }
    else if(![responseObject valueForKey:@"image"])
    {
        profileImage= @"http://nil";
        
        NSLog(@"there is image in there");
    }
    else
    {
        profileImage =[responseObject valueForKey:@"image"] ;
    }
    
    NSString *no_following = [[NSString alloc]init];
    NSString *no_of_follower =[[NSString alloc]init];
    if (![responseObject valueForKey:@"no_following"]|| [responseObject valueForKey:@"no_of_followers"]== (id)[NSNull null]) {
        
        no_following = @"0";
        no_of_follower =@"0";
    }
    
    else
    {
        no_following =[responseObject valueForKey:@"no_following"];
        no_of_follower =[responseObject valueForKey:@"no_of_followers"];
    }
    NSUserDefaults *standar = [[NSUserDefaults alloc]init];
    NSString *installID = [[standar objectForKey:@"installDevice" ]objectForKey:@"install_id"];
    NSString *api_key = [[standar objectForKey:@"installDevice" ]objectForKey:@"api_key"];
    NSString *idUser =[[standar objectForKey:@"installDevice" ]objectForKey:@"id"];
    [standar setObject:[NSMutableDictionary dictionaryWithObjects:[NSArray arrayWithObjects:api_key,idUser ,@"ok",installID,[responseObject valueForKey:@"name"]
                                                                   , profileImage,no_following,no_of_follower, nil] forKeys:[NSArray arrayWithObjects:@"api_key",@"id",@"install_status",@"install_id",@"name",@"profile_image",@"following",@"follower",nil]] forKey:@"installDevice"];
    [standar synchronize];

}
- (void)addFilteringCategory
{
    NSUserDefaults *identifier =[[NSUserDefaults alloc]init];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:[NSString stringWithFormat:@"%@categories/choose-category?user_id=%@&access-token=%@",APIURL,[[identifier objectForKey:@"installDevice"] objectForKey:@"id"],[[identifier objectForKey:@"installDevice"] objectForKey:@"api_key"]] parameters:@{@"chosen_category_ids":@"" } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //                NSLog(@"json followed map news : %@",responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //                NSLog(@"Error: %@", operation.responseObject);
    }];
}

- (BOOL)addFeedback:(NSString*)feedbackName
{
    
    return NO;
}
- (BOOL)addLog:(NSString *)addlogTypes addLogID:(NSString *)theLogID
{
    __block BOOL info;
    
    NSString *reportType = [[NSString alloc]init];
    if ([addlogTypes isEqualToString:@"map_id"]) {
        reportType =@"2";
    }
    else if ([addlogTypes isEqualToString:@"email"])
    {
        reportType =@"3";
    }
    else if ([addlogTypes isEqualToString:@"message"])
    {
        reportType =@"4";
    }
    else if ([addlogTypes isEqualToString:@"pin_id"])
    {
        reportType =@"1";
    }
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]init];
    
    manager.completionQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    NSUserDefaults *identifier = [[NSUserDefaults alloc]init];
    
    AFHTTPRequestOperation *operation =  [manager POST:[NSString stringWithFormat:@"%@logs/add-log?user_id=%@&access-token=%@",APIURL,[[identifier objectForKey:@"installDevice"] objectForKey:@"id"],[[identifier objectForKey:@"installDevice"] objectForKey:@"api_key"]] parameters:@{@"log_type":reportType,addlogTypes:theLogID} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"response log : %@ ",responseObject);
        if ([[responseObject valueForKey:@"status"]isEqualToString:@"fail"]) {
            info = NO;
        }
        else if ([[responseObject valueForKey:@"status"]isEqualToString:@"ok"])
        {
            info = YES;
        }

        dispatch_semaphore_signal(semaphore);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        info = NO;
        dispatch_semaphore_signal(semaphore);
    }];
    
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    
    [operation waitUntilFinished];
    
    return info;
}

- (void)followUser :(NSString *)userID statusUser:(NSString*)user{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"hasSignIn"]){
        NSUserDefaults *identifier = [[NSUserDefaults alloc]init];
        if ([[NSUserDefaults standardUserDefaults]objectForKey:@"followUser"]) {
            //            NSLog(@"enter in to add follow");
            NSMutableArray *arrayTempFollowUser = [[NSMutableArray alloc]init];
            arrayTempFollowUser = [[identifier objectForKey:@"followUser"] mutableCopy];
            //            NSLog(@follow user: %@" ,arrayTempBookmarkedMaps);
            [arrayTempFollowUser addObject:user];
            [identifier setObject:arrayTempFollowUser forKey:@"followUser"];
            [identifier synchronize];
        } else {
            
            //            NSLog(@"new follow user");
            [identifier setObject:[NSArray arrayWithObject:user] forKey:@"followUser"];
            [identifier synchronize];
        }
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager POST:[NSString stringWithFormat:@"%@users/followed-user?user_id=%@&access-token=%@",APIURL,[[identifier objectForKey:@"installDevice"] objectForKey:@"id"],[[identifier objectForKey:@"installDevice"] objectForKey:@"api_key"]] parameters:@{@"followed_user_ids":userID } success:^(AFHTTPRequestOperation *operation, id responseObject) {
                            NSLog(@"follow user : %@",responseObject);
            if ([[responseObject valueForKey:@"sync_status"]isEqualToString:@"ok"]) {
                NSString *empty =[[NSString alloc]init];
                [self editProfile:[[identifier objectForKey:@"installDevice"] objectForKey:@"name"] imageURL:empty imagePath:[UIImage imageNamed:@""]];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            //                NSLog(@"Error: %@", operation.responseObject);
        }];
    }
}

-(void)unFollowUser:(NSString *)userID statusUser:(NSString* )user
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"hasSignIn"])
    {
        NSUserDefaults *identifier = [[NSUserDefaults alloc]init];
        //        [identifier removeObjectForKey:@"bookmarkedMaps"];
        if ([[NSUserDefaults standardUserDefaults]objectForKey:@"followUser"]) {
            NSMutableArray *arrayTempFollowUser =  [[identifier objectForKey:@"followUser"] mutableCopy];
            NSMutableArray *arrayTemp = [arrayTempFollowUser mutableCopy];
            //            NSLog(@"pref bookmarked map : %@",arrayTempBookmarkedMaps);
            
            for (int i =0 ; i < [arrayTemp count]; i++) {
                if ([[arrayTemp objectAtIndex:i]isEqualToString:user]) {
                    [arrayTempFollowUser removeObjectAtIndex:i];
                    break;
                }
            }
            if ([arrayTempFollowUser count] == 0) {
                [identifier removeObjectForKey:@"followUser"];
                [identifier synchronize];
                //                NSLog(@"delete the key");
            }
            else
            {
                //                NSLog(@"delete by one");
                [identifier setObject:arrayTempFollowUser forKey:@"followUser"];
                [identifier synchronize];
            }
        }
        else
        {
            //do nothing
            
        }
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager POST:[NSString stringWithFormat:@"%@users/followed-user?user_id=%@&access-token=%@",APIURL,[[identifier objectForKey:@"installDevice"] objectForKey:@"id"],[[identifier objectForKey:@"installDevice"] objectForKey:@"api_key"]] parameters:@{@"followed_user_ids":userID } success:^(AFHTTPRequestOperation *operation, id responseObject) {
                        NSLog(@"unfollowuser : %@",responseObject);
            if ([[responseObject valueForKey:@"sync_status"]isEqualToString:@"ok"]) {
                NSString *empty =[[NSString alloc]init];
                [self editProfile:[[identifier objectForKey:@"installDevice"] objectForKey:@"name"] imageURL:empty imagePath:[UIImage imageNamed:@""]];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            //            NSLog(@"Error: %@", operation.responseObject);
        }];
        
    }
    
}

- (void)savePinLocally:(NSString *)mapID titlePin:(NSString *)title descriptionPin:(NSString *)description latitudePin:(NSString* )latitude longitudePin:(NSString* )longitude addressPin:(NSString *)address pathImagePin:(NSString *)filePath imagePin:(UIImage* )image
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *arrayTemp = [[NSMutableArray alloc]init];
    NSArray *arrayDic= [NSArray arrayWithObject:@{
                                                  @"map_id": mapID,
                                                  @"title": title,
                                                  @"description": description,
                                                  @"latitude":latitude,
                                                  @"longitude":longitude,
                                                  @"address":address,
                                                  @"filePath":filePath
                                                  }];
    
    
    if ([defaults objectForKey:@"newPin"]) {
        arrayTemp = [[defaults objectForKey:@"newPin"] mutableCopy];
        [arrayTemp addObject:[arrayDic lastObject]];
        
        [defaults setObject:arrayTemp forKey:@"newPin"];
        [defaults synchronize];
        
        //        NSData *dataPins = [defaults objectForKey:[map valueForKey:@"id"]];
        //        NSArray *arrloadTheData = [NSKeyedUnarchiver unarchiveObjectWithData:dataPins];
        
        NSLog(@"second data : %@ ",[defaults objectForKey:@"newPin"]);
    }
    else
    {
        [defaults setObject:arrayDic forKey:@"newPin"];
        [defaults synchronize];
        
        //        NSData *dataPins = [defaults objectForKey:[map valueForKey:@"id"]];
        //        NSArray *arrloadTheData = [NSKeyedUnarchiver unarchiveObjectWithData:dataPins];
        
        NSLog(@"First data : %@ ",[defaults objectForKey:@"newPin"]);
        
    }
    
}

- (NSMutableArray *)retrivePinByMap:(NSString*)latitude longitude:(NSString *)point map:(NSString*)mapID
{
    
//    https://api.anymap.co/index.php/maps/pins?user_id=5972&access-token=6fIVQTUx4sGDw3TIVOgtmOxcfLhcb9YWsaWb_aBE06wqM&lat=1.2971379&lon=103.7867546&pin_radius=30&pin_limit=20&map_id=2363
    NSString *result = [[NSString alloc]init];
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"category"]) {
        
        NSMutableArray *category = [[NSUserDefaults standardUserDefaults]objectForKey:@"category"];
        
        result = [category componentsJoinedByString:@","];
    }
    else
    {
        result = @"";
    }
    
    __block NSMutableArray *arrayTemp;
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.completionQueue =  dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
//    manager.responseSerializer.acceptableContentTypes =[NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    // header("Content-Type: application/json");
    //    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSUserDefaults *identifier = [[NSUserDefaults alloc]init];
    AFHTTPRequestOperation  *operation = [manager GET:[NSString stringWithFormat:@"%@maps/pins?user_id=%@&access-token=%@",APIURL,[[identifier objectForKey:@"installDevice"] objectForKey:@"id"] ,[[identifier objectForKey:@"installDevice"] objectForKey:@"api_key"]] parameters:@{@"lat":latitude,@"lon":point,@"pin_radius":@"30",@"pin_limit":@"5",@"map_id":mapID} success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    NSLog(@"pin results : %@", responseObject);
        NSLog(@"responseObject %@",responseObject);
        
        NSString *jsonString =  [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        
        NSString *newJsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\'" withString:@""];
        
        /*
         NSRange range = [jsonString rangeOfString:@"}" options:NSBackwardsSearch];
         jsonString = [jsonString substringToIndex:range.location + 1];
         */
        NSData *data = [newJsonString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSError *error;
        NSMutableArray *array = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        
        NSLog(@"array %@",array);
        arrayTemp = [array mutableCopy];
        
        if (!array) {
            NSLog(@"Parsing JSON failed: %@", error);
            arrayTemp = nil;
        }
        
        /*
         NSData *newJSONData = [newJsonString dataUsingEncoding:NSUTF8StringEncoding];
         NSDictionary* json = [NSJSONSerialization
         JSONObjectWithData:newJSONData
         options:NSJSONReadingMutableContainers
         error:&error];
         NSLog(@"json %@",json);
         */
        
//        NSLog(@"responseObject = %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
        dispatch_semaphore_signal(semaphore);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //        NSLog(@"Error tes: %@", error);
                NSLog(@"Error object: %@", operation.responseObject);
        dispatch_semaphore_signal(semaphore);
    }];
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    [operation waitUntilFinished];
    return arrayTemp;
}

@end
