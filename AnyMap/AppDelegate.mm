
#import <UIKit/UIKit.h>
#import "LoadingViewController.h"
#import "AppDelegate.h"
#ifndef NO_GP
#import <GooglePlus/GooglePlus.h>
#import <GoogleMaps/GoogleMaps.h>
#endif
#ifndef NO_FB
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#endif
#import <Google/CloudMessaging.h>
#import "NetworkManager.h"
@implementation AppDelegate

/** Google Analytics configuration constants **/
static NSString *const kGaPropertyId = @"UA-64298650-1"; // Placeholder property ID.
static NSString *const kTrackingPreferenceKey = @"Anymap";
- (void)location{
    self.locationManager = [CLLocationManager new];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    //------
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    else{
        [self.locationManager startUpdatingLocation];
        
    }
}
#pragma locationManager
-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
        {
            // do some error handling
        }
            break;
        default:{
            [manager startUpdatingLocation];
            auto coordinate = self.locationManager.location.coordinate;
//            double latitude = coordinate.latitude;
//            double longitude = coordinate.longitude;
//            NSLog(@"saat di update latitude : %f", latitude);
//            NSLog(@"saat di update longitude : %f", longitude);
            
//            NetworkManager *network = [[NetworkManager alloc]init];
//
//            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
//                self.downloadMaps = [network maps_by_response:[NSString stringWithFormat:@"%f",latitude] longitude:[NSString stringWithFormat:@"%f",longitude]];
//                dispatch_async(dispatch_get_main_queue(), ^{
//
//                });
//            });
        }
            break;
    }
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSUserDefaults *identifier = [[NSUserDefaults alloc]init];
        NSLog(@"bookmarked map : %@" ,[identifier objectForKey:@"bookmarkedMaps"]);
    
    //read the notification files
    [self readPlistFile];
    
    //update the maps , this method use to download all the maps and update the maps every 5 minutes and update by the locations
    
    [self location];
    
    
    [Fabric with:@[CrashlyticsKit]];
    [IQKeyboardManager sharedManager].enable = YES;
//    [[IQKeyboardManager sharedManager] setCanAdjustTextView:YES];
//    auto user = new AnyMap::User(std::string([[[NSFileManager defaultManager] URLsForDirectory:NSLibraryDirectory inDomains:NSUserDomainMask].lastObject path].UTF8String) + "/Preferences/");
//    self.currentUser = user;
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    // [START_EXCLUDE]
    _registrationKey = @"onRegistrationCompleted";
    _messageKey = @"onMessageReceived";
    // Configure the Google context: parses the GoogleService-Info.plist, and initializes
    // the services that have entries in the file
    NSError* configureError;
    [[GGLContext sharedInstance] configureWithError:&configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    _gcmSenderID = [[[GGLContext sharedInstance] configuration] gcmSenderID];
    // Register for remote notifications
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
        // iOS 7.1 or earlier
        
        NSLog(@"for ios 7");
        UIRemoteNotificationType allNotificationTypes =
        (UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge);
        [application registerForRemoteNotificationTypes:allNotificationTypes];
    } else {
        // iOS 8 or later
        // [END_EXCLUDE]
        NSLog(@"for ios 8");
        
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    // [END register_for_remote_notifications]
    // [START start_gcm_service]
     GCMConfig *gcmConfig = [GCMConfig defaultConfig];
    gcmConfig.receiverDelegate = self;
    [[GCMService sharedInstance] startWithConfig:gcmConfig];
    // [END start_gcm_service]
    __weak typeof(self) weakSelf = self;
    // Handler for registration token request
    _registrationHandler = ^(NSString *registrationToken, NSError *error){
        if (registrationToken != nil) {
            weakSelf.registrationToken = registrationToken;
            NSLog(@"Registration Token: %@", registrationToken);
//            auto tokenGCM = user->update_gcm(registrationToken.UTF8String);
            NetworkManager *network = [[NetworkManager alloc]init];
            [network updateGCM:registrationToken];
        
            NSDictionary *userInfo = @{@"registrationToken":registrationToken};
            [[NSNotificationCenter defaultCenter] postNotificationName:weakSelf.registrationKey
                                                                object:nil
                                                              userInfo:userInfo];
        } else {
            NSLog(@"Registration to GCM failed with error: %@", error.localizedDescription);
            NSDictionary *userInfo = @{@"error":error.localizedDescription};
            [[NSNotificationCenter defaultCenter] postNotificationName:weakSelf.registrationKey
                                                                object:nil
                                                              userInfo:userInfo];
        }
    };

    
    
    
    
#ifdef NO_IB
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.rootViewController = [LoadingViewController new];
    [self.window makeKeyAndVisible];
#endif
    application.statusBarStyle = UIStatusBarStyleLightContent;
    [UINavigationBar appearance].barTintColor = [UIColor colorWithRed:0.1607843137254902 green:0.58823529409999997 blue:0.95294117649999999 alpha:1.0];
#ifndef NO_GM
    [GMSServices provideAPIKey:@"AIzaSyA8CTJco1Lok_DMTiM5oKdtM7dXzaMnihk"];
#endif
#ifndef NO_FB
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];
#else
    return YES;
#endif
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [[GCMService sharedInstance] disconnect];
    // [START_EXCLUDE]
    _connectedToGCM = NO;
    // [END_EXCLUDE]
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    //[FBSDKAppEvents activateApp];
    [[GCMService sharedInstance] connectWithHandler:^(NSError *error) {
        if (error) {
            NSLog(@"Could not connect to GCM: %@", error.localizedDescription);
        } else {
            _connectedToGCM = true; 
            NSLog(@"Connected to GCM");
            // [START_EXCLUDE]
            
            // [END_EXCLUDE]
        }
    }];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
//    delete self.currentUser;
//    self.currentUser = nullptr;
}

#ifndef NO_FB
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{

    NSString *scheme = url.scheme;
    NSLog(@"scheme url nya : %@",scheme);

    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation],[GPPURLHandler handleURL:url
                                                                                     sourceApplication:sourceApplication
                                                                                            annotation:annotation];
    return YES;
}
#endif


#pragma GCM
// [START receive_apns_token]
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    // [END receive_apns_token]
    // [START get_gcm_reg_token]
    // Create a config and set a delegate that implements the GGLInstaceIDDelegate protocol.
    GGLInstanceIDConfig *instanceIDConfig = [GGLInstanceIDConfig defaultConfig];
    instanceIDConfig.delegate = self;
    // Start the GGLInstanceID shared instance with the that config and request a registration
    // token to enable reception of notifications
    [[GGLInstanceID sharedInstance] startWithConfig:instanceIDConfig];
    _registrationOptions = @{kGGLInstanceIDRegisterAPNSOption:deviceToken,
                             kGGLInstanceIDAPNSServerTypeSandboxOption:@YES};
    [[GGLInstanceID sharedInstance] tokenWithAuthorizedEntity:_gcmSenderID
                                                        scope:kGGLInstanceIDScopeGCM
                                                      options:_registrationOptions
                                                      handler:_registrationHandler];
    // [END get_gcm_reg_token]
}

// [START receive_apns_token_error]
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Registration for remote notification failed with error: %@", error.localizedDescription);
    // [END receive_apns_token_error]
    NSDictionary *userInfo = @{@"error" :error.localizedDescription};
    [[NSNotificationCenter defaultCenter] postNotificationName:_registrationKey
                                                        object:nil
                                                      userInfo:userInfo];
}

// [START ack_message_reception]
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSLog(@"Notification received for the : %@", userInfo);
    
    [self saveTheNotificationInsideTheApp:userInfo];
    // This works only if the app started the GCM service
    [[GCMService sharedInstance] appDidReceiveMessage:userInfo];
    // Handle the received message
    // [START_EXCLUDE]
    [[NSNotificationCenter defaultCenter] postNotificationName:_messageKey
                                                        object:nil
                                                      userInfo:userInfo];
    // [END_EXCLUDE]
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))handler {

    // This works only if the app started the GCM service
    [[GCMService sharedInstance] appDidReceiveMessage:userInfo];
        NSLog(@"Notification received: %@", userInfo);
    application.applicationIconBadgeNumber = 0;
    // Handle the received message
    // Invoke the completion handler passing the appropriate UIBackgroundFetchResult value
    // [START_EXCLUDE]
    [self saveTheNotificationInsideTheApp:userInfo];
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:_messageKey
                                                        object:nil
                                                      userInfo:userInfo];
    handler(UIBackgroundFetchResultNoData);
    // [END_EXCLUDE]
}
// [END ack_message_reception]

// [START on_token_refresh]
- (void)onTokenRefresh {
    // A rotation of the registration tokens is happening, so the app needs to request a new token.
    NSLog(@"The GCM registration token needs to be changed.");
    [[GGLInstanceID sharedInstance] tokenWithAuthorizedEntity:_gcmSenderID
                                                        scope:kGGLInstanceIDScopeGCM
                                                      options:_registrationOptions
                                                      handler:_registrationHandler];
}
// [END on_token_refresh]

// [START upstream_callbacks]
- (void)willSendDataMessageWithID:(NSString *)messageID error:(NSError *)error {
    if (error) {
        NSLog(@"send the message id : %@",messageID);
        // Failed to send the message.
    } else {
        NSLog(@"want to see the message : %@",messageID);
        // Will send message, you can save the messageID to track the message
    }
}

- (void)didSendDataMessageWithID:(NSString *)messageID {
    NSLog(@"message id : %@",messageID);
    // Did successfully send message identified by messageID
}
// [END upstream_callbacks]

- (void)didDeleteMessagesOnServer {
    // Some messages sent to this device were deleted on the GCM server before reception, likely
    // because the TTL expired. The client should notify the app server of this, so that the app
    // server can resend those messages.
}

#pragma saveTheNotification
- (void)saveTheNotification:(NSDictionary *)data
{

    Notifications *notif = [[Notifications alloc]init];
    notif.idNotification = [[data objectForKey:[NSString stringWithFormat:@"gcm.notification.%@",NOTIFICATION_ID]] integerValue];
    notif.type = [[data objectForKey:[NSString stringWithFormat:@"gcm.notification.%@",NOTIFICATION_TYPE]]integerValue];
    notif.message = [data objectForKey:[NSString stringWithFormat:@"gcm.notification.%@",NOTIFICATION_MESSAGE]];
    notif.profile_id = [[data objectForKey:[NSString stringWithFormat:@"gcm.notification.%@",NOTIFICATION_PROFILE_ID]]integerValue];
    notif.profile_name =[data objectForKey:[NSString stringWithFormat:@"gcm.notification.%@",NOTIFICATION_PROFILE_NAME]];
    notif.profile_image = [data objectForKey:[NSString stringWithFormat:@"gcm.notification.%@",NOTIFICATION_PROFILE_IMAGE]];
    notif.pin_id = [[data objectForKey:[NSString stringWithFormat:@"gcm.notification.%@",NOTIFICATION_PIN_ID]]integerValue];
    notif.pin_title = [data objectForKey:[NSString stringWithFormat:@"gcm.notification.%@",NOTIFICATION_PIN_TITLE]];
    notif.pin_image = [data objectForKey:[NSString stringWithFormat:@"gcm.notification.%@",NOTIFICATION_PIN_IMAGE]];
    notif.pin_description = [data objectForKey:[NSString stringWithFormat:@"gcm.notification.%@",NOTIFICATION_PIN_DESCRIPTION]];
    notif.mapId = [[data objectForKey:[NSString stringWithFormat:@"gcm.notification.%@",NOTIFICATION_MAP_ID]]integerValue];
    notif.mapCreatorId = [[data objectForKey:[NSString stringWithFormat:@"gcm.notification.%@",NOTIFICATION_MAP_CREATOR_ID]]integerValue];
    notif.mapTitle = [data objectForKey:[NSString stringWithFormat:@"gcm.notification.%@",NOTIFICATION_MAP_TITLE]];
    notif.mapDescription =[data objectForKey:[NSString stringWithFormat:@"gcm.notification.%@",NOTIFICATION_MAP_DESCRIPTION]];
    notif.from =[data objectForKey:[NSString stringWithFormat:@"gcm.notification.%@",NOTIFICATION_FROM]];
    notif.createdAt = [data objectForKey:[NSString stringWithFormat:@"gcm.notification.%@",NOTIFICATION_CREATED_AT]];
    notif.title = [data objectForKey:[NSString stringWithFormat:@"gcm.notification.%@",NOTIFICATION_TITLE]];
    notif.subTitle = [data objectForKey:[NSString stringWithFormat:@"gcm.notification.%@",NOTIFICATION_SUB_TITLE]];
    notif.tickerText= [data objectForKey:[NSString stringWithFormat:@"gcm.notification.%@",NOTIFICATION_TICKER_TEXT]];
    notif.mapCreatorImage= [data objectForKey:[NSString stringWithFormat:@"gcm.notification.%@",NOTIFICATION_MAP_CREATOR_IMAGE]];
    notif.isDeleted =0;
    notif.isRead = 0;
    NSLog(@"isi id no : %ld",(long)notif.idNotification);
    NSLog(@"message type : %ld",(long)notif.type);
    NSLog(@"profile id : %ld",(long)notif.profile_id);
    NSLog(@"profileName : %@",notif.profile_name);
    NSLog(@"pin image : %@",notif.pin_image);
    NSLog(@"pin description : %@",notif.pin_description);
    NSLog(@"map id : %ld",(long)notif.mapId);
    NSLog(@"map creator id : %ld",(long)notif.mapCreatorId);
    NSLog(@"map title : %@",notif.mapTitle);
    NSLog(@"map description : %@",notif.mapDescription);
    NSLog(@"from: %@",notif.from);
    NSLog(@"created at : %@",notif.createdAt);
    NSLog(@"title : %@",notif.title);
    NSLog(@"sub title : %@",notif.subTitle);
    NSLog(@"ticker text : %@",notif.tickerText);
    
    
//    NSError *error;
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //1
//    NSString *documentsDirectory = [paths objectAtIndex:0]; //2
//    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"Notifications.plist"]; //3
//    NSFileManager *fileManager = [NSFileManager defaultManager];
//    if (![fileManager fileExistsAtPath: path]) //4
//    {
//        NSString *bundle = [[NSBundle mainBundle] pathForResource:@"Notifications" ofType:@"plist"]; //5
//        
//        [fileManager copyItemAtPath:bundle toPath: path error:&error]; //6
//    }
//    NSMutableDictionary *notifD = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
//    NSLog(@"the data : %@",notifD);
//    NSMutableArray *plisData = [[NSMutableArray alloc] initWithContentsOfFile: path];
//    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[NSString stringWithFormat:@"%ld",(long)notif.idNotification],[NSString stringWithFormat:@"%ld",(long)notif.type],notif.message, [NSString stringWithFormat:@"%ld",(long)notif.profile_id],notif.profile_name, [NSString stringWithFormat:@"%ld",(long)notif.pin_id],notif.pin_title,notif.pin_image,notif.pin_description,[NSString stringWithFormat:@"%ld",(long) notif.mapId],[NSString stringWithFormat:@"%ld",(long) notif.mapCreatorId],notif.mapTitle,notif.createdAt,notif.title,notif.subTitle,notif.tickerText,[NSString stringWithFormat:@"%ld",(long) notif.isDeleted],[NSString stringWithFormat:@"%ld",(long) notif.isRead], notif.mapDescription,notif.from,notif.mapCreatorImage, nil] forKeys:[NSArray arrayWithObjects:@"id_notification",@"type",@"message",@"profile_id",@"profile_name",@"pin_id",@"pin_title",@"pin_image",@"pin_description",@"map_id",@"map_creator_id",@"map_title",@"createdAt",@"title",@"sub_title",@"tickerText",@"is_deleted",@"is_read",@"map_description",@"from",@"map_creator_image",nil]];
//    [plisData addObject:dict];
//    
//    NSLog(@"new data : %@",plisData);
//    
//    [plisData writeToFile: path atomically:YES];
//    NSLog(@"save the data");
}
- (void)saveTheNotificationInsideTheApp:(NSDictionary *)data
{
    
    Notifications *notif = [[Notifications alloc]init];
    notif.idNotification = [[data objectForKey:@"id"] integerValue];
    notif.type = [[data objectForKey:@"type"]integerValue];
    notif.message = [data objectForKey:@"message"];
    if (notif.message.length == 0) {
        notif.message =@"null";
    }
    notif.profile_id = [[data objectForKey:@"profile_id"]integerValue];
    if (notif.profile_id == 0) {
        notif.profile_id = 0;
    }
    notif.profile_name =[data objectForKey:@"profile_name"];
    if (notif.profile_name.length == 0) {
        notif.profile_name = @"";
    }
    notif.profile_image = [data objectForKey:@"profile_image"];
    NSLog(@"by the key :  %@",[data objectForKey:@"profile_image"]);
    if (notif.profile_image.length == 0) {
        notif.profile_image =@"null";
    }
    notif.profile_following = [[data objectForKey:@"profile_following"] integerValue];
    if (notif.profile_following == 0) {
        notif.profile_following = 0;
    }
    notif.profile_followers = [[data objectForKey:@"profile_followers"]integerValue];
    if (notif.profile_followers == 0) {
        notif.profile_followers = 0;
    }
    notif.pin_id = [[data objectForKey:@"pin_id"]integerValue];
    notif.pin_title = [data objectForKey:@"pin_title"];
    notif.pin_image = [data objectForKey:@"pin_image"];
    notif.pin_description = [data objectForKey:@"pin_description"];
    if (notif.pin_description.length == 0) {
        notif.pin_description =@"null";
    }
    notif.mapId = [[data objectForKey:@"map_id"]integerValue];
    notif.mapCreatorId = [[data objectForKey:@"map_creator_id"]integerValue];
    notif.mapTitle = [data objectForKey:@"map_title"];
    notif.mapDescription =[data objectForKey:@"map_description"];
    notif.mapNoPins = [[data objectForKey:@"map_no_of_pins"]integerValue];
    if (notif.mapNoPins == 0) {
        notif.mapNoPins = 0;
    }
    
    notif.from =[data objectForKey:@"from"];
    notif.createdAt = [data objectForKey:@"created_at"];
    if (notif.createdAt.length == 0) {
        notif.createdAt =@"null";
    }
    notif.title = [data objectForKey:@"title"];
    notif.subTitle = [data objectForKey:@"subtitle"];
    notif.tickerText= [data objectForKey:@"ticker_text"];
    if (notif.tickerText.length == 0) {
        notif.tickerText =@"null";
    }
    notif.mapCreatorImage= [data objectForKey:@"map_creator_image"];
    if (notif.mapCreatorImage.length == 0) {
        notif.mapCreatorImage =@"null";
    }
    notif.isDeleted =0;
    notif.isRead = 0;
    NSLog(@"isi id no : %ld",(long)notif.idNotification);
    NSLog(@"message type : %ld",(long)notif.type);
    NSLog(@"profile id : %ld",(long)notif.profile_id);
    NSLog(@"profileName : %@",notif.profile_name);
    NSLog(@"profile image : %@",notif.profile_image);
    NSLog(@"profile following : %ld",(long)notif.profile_following);
    NSLog(@"profile follower : %ld",(long)notif.profile_followers);
    NSLog(@"pin image : %@",notif.pin_image);
    NSLog(@"pin description : %@",notif.pin_description);
    NSLog(@"map id : %ld",(long)notif.mapId);
    NSLog(@"map creator id : %ld",(long)notif.mapCreatorId);
    NSLog(@"map title : %@",notif.mapTitle);
    NSLog(@"map description : %@",notif.mapDescription);
    NSLog(@"from: %@",notif.from);
    NSLog(@"created at : %@",notif.createdAt);
    NSLog(@"title : %@",notif.title);
    NSLog(@"sub title : %@",notif.subTitle);
    NSLog(@"ticker text : %@",notif.tickerText);
    NSLog(@"map pins number : %ld",(long)notif.mapNoPins);
//         if ([signUpResponse valueForKey:@"profile_image"] == (id)[NSNull null]) {

   
    
        NSError *error;
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //1
        NSString *documentsDirectory = [paths objectAtIndex:0]; //2
        NSString *path = [documentsDirectory stringByAppendingPathComponent:@"Notifications.plist"]; //3
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if (![fileManager fileExistsAtPath: path]) //4
        {
            NSString *bundle = [[NSBundle mainBundle] pathForResource:@"Notifications" ofType:@"plist"]; //5
    
            [fileManager copyItemAtPath:bundle toPath: path error:&error]; //6
        }
        NSMutableDictionary *notifD = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
        NSLog(@"the data : %@",notifD);
        NSMutableArray *plisData = [[NSMutableArray alloc] initWithContentsOfFile: path];
        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[NSString stringWithFormat:@"%ld",(long)notif.idNotification],[NSString stringWithFormat:@"%ld",(long)notif.type],notif.message, [NSString stringWithFormat:@"%ld",(long)notif.profile_id],notif.profile_name,notif.profile_image,[NSString stringWithFormat:@"%ld",(long)notif.profile_following],[NSString stringWithFormat:@"%ld",(long)notif.profile_followers],[NSString stringWithFormat:@"%ld",(long)notif.pin_id],notif.pin_title,notif.pin_image,notif.pin_description,[NSString stringWithFormat:@"%ld",(long) notif.mapId],[NSString stringWithFormat:@"%ld",(long) notif.mapCreatorId],notif.mapTitle,notif.createdAt,notif.title,notif.subTitle,notif.tickerText,[NSString stringWithFormat:@"%ld",(long) notif.isDeleted],[NSString stringWithFormat:@"%ld",(long) notif.isRead], notif.mapDescription,notif.from,notif.mapCreatorImage,[NSString stringWithFormat:@"%ld",(long) notif.mapNoPins], nil] forKeys:[NSArray arrayWithObjects:@"id_notification",@"type",@"message",@"profile_id",@"profile_name",@"profile_image",@"profile_following",@"profile_followers",@"pin_id",@"pin_title",@"pin_image",@"pin_description",@"map_id",@"map_creator_id",@"map_title",@"createdAt",@"title",@"sub_title",@"tickerText",@"is_deleted",@"is_read",@"map_description",@"from",@"map_creator_image",@"map_no_of_pins",nil]];
        [plisData addObject:dict];
    
        NSLog(@"new data : %@",plisData);
    
        [plisData writeToFile: path atomically:YES];
        NSLog(@"save the data");
}
- (void)readPlistFile
{
    NSError *errorDesc = nil;
    NSPropertyListFormat format;
    NSString *plistPath;
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                              NSUserDomainMask, YES) objectAtIndex:0];
    plistPath = [rootPath stringByAppendingPathComponent:@"Notifications.plist"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
        plistPath = [[NSBundle mainBundle] pathForResource:@"Notifications" ofType:@"plist"];
    }
    NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
    NSDictionary *temp = (NSDictionary *)[NSPropertyListSerialization propertyListWithData:plistXML options:NSPropertyListMutableContainersAndLeaves format:&format error:&errorDesc];
    
    if (!temp) {
        NSLog(@"Error reading plist: %@, format: %lu", errorDesc, (unsigned long)format);
    }
    
    NSLog(@"temp data: %@ ",temp);
}
-(void)savetoNotificationPlist
{

}

@end
