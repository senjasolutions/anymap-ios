/*-*-objc-*-*/
//  MapDetailLocationViewController.m
//  AnyMap
//
//  Created by Antonio M on 01/10/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//
#ifdef DEBUG
#endif
#import <CoreLocation/CoreLocation.h>
#import "AppDelegate.h"
#import "mapDetailCollectionViewController.h"
#import "CardMapPhotoCollectionViewCell.h"
#import "MapDetailLocationViewController.h"
#import "NetworkManager.h"
#import "LoginVC.h"
#import <INTULocationManager/INTULocationManager.h>

#ifdef NO_GM
@interface MapDetailLocationViewController ()<CLLocationManagerDelegate>
#else
@interface MapDetailLocationViewController ()<GMSMapViewDelegate,CLLocationManagerDelegate,overFlowMapDelegate>
@property (nonatomic)NSInteger indexPin;
#endif
@property (nonatomic, strong) CLLocationManager *locationManager;
@end
static NSString * const TitleKey = @"title";
static NSString * const InfoKey = @"info";
static NSString * const LatitudeKey = @"latitude";
static NSString * const LongitudeKey = @"longitude";
@implementation MapDetailLocationViewController{ GMSPlacesClient *_placesClient;}
- (void)viewDidLoad {
    [super viewDidLoad];
    _placesClient = [[GMSPlacesClient alloc] init];
    self.headerLocation.text =self.titleLocation;
#ifndef NO_GM
    self.mapViewLocationDetail.delegate = self;

    self.mapViewLocationDetail.settings.rotateGestures = NO;
    self.mapViewLocationDetail.settings.tiltGestures = NO;
    self.mapViewLocationDetail.myLocationEnabled = YES;
#endif
    self.locationManager = [[CLLocationManager alloc] init];
    CLLocation *fake = [[CLLocation alloc]initWithLatitude:1.3048425 longitude:103.8318243];
    [self.locationManager.delegate locationManager:self.locationManager didUpdateLocations:@[fake]];
    self.locationManager.delegate = self;
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    else{
        [self.locationManager startUpdatingLocation];
    }
}
#pragma locationManager
-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
        {
            // do some error handling
        }
            break;
        default:{
            [self.locationManager startUpdatingLocation];
        }
            break;
    }
}
- (void)checkAddress:(CLLocationCoordinate2D )coordinate
{
    [[GMSGeocoder geocoder] reverseGeocodeCoordinate:coordinate completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error) {
//        NSLog(@"reverse geocoding results:");
//        NSLog(@"nilai response nya adalah : %@",response);
           GMSAddress* address = [response firstResult];
        if (address) {
//            NSLog(@"thoroughfare %@",address.thoroughfare);
//            NSLog(@"locality %@",address.locality);
//            NSLog(@"subLocality %@",address.subLocality);
//            NSLog(@"administrativeArea %@",address.administrativeArea);
//            NSLog(@"postalCode %@",address.postalCode);
//            NSLog(@"country %@",address.country);
//            NSLog(@"lines %@",address.lines);
            if(address.thoroughfare.length != 0)
            {
                    self.address = address.thoroughfare;
            }
            else
            {
                self.address = address.thoroughfare;
            }
            
//            for(NSString * adress in address.lines)
//            {
//                NSLog(@"nilai adress %@",adress);
//            }
        }
    }];
}
//- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
//{
//    NSLog(@"didUpdateToLocation: %@", newLocation);
//    CLLocationCoordinate2D  currentLocation = CLLocationCoordinate2DMake(newLocation.coordinate.latitude, newLocation.coordinate.longitude);
//    
//    [[GMSGeocoder geocoder] reverseGeocodeCoordinate:currentLocation completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error) {
//        //        NSLog(@"reverse geocoding results:");
//        //        NSLog(@"nilai response nya adalah : %@",response);
//        GMSAddress* address = [response firstResult];
//        if (address) {
//                        NSLog(@"thoroughfare %@",address.thoroughfare);
//                        NSLog(@"locality %@",address.locality);
//                        NSLog(@"subLocality %@",address.subLocality);
//                        NSLog(@"administrativeArea %@",address.administrativeArea);
//                        NSLog(@"postalCode %@",address.postalCode);
//                        NSLog(@"country %@",address.country);
//                        NSLog(@"lines %@",address.lines);
////            self.address = [address.lines lastObject];
//            self.address = address.thoroughfare;
//            //            for(NSString * adress in address.lines)
//            //            {
//            //                NSLog(@"nilai adress %@",adress);
//            //            }
//        }
//    }];
//}
- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
    [self checkAddress:position.target];
}
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
//     NSLog(@"take new position  : %@", [locations lastObject]);
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (self.isFromBookmarked) {
        NSLog(@"is from the bookmarkmap : %@",self.bookmarkMap);
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"Loading...";
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            INTULocationManager *locMgr = [INTULocationManager sharedInstance];
            [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyHouse
                                               timeout:10.0
                                  delayUntilAuthorized:YES  // This parameter is optional, defaults to NO if omitted
                                                 block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                                     if (status == INTULocationStatusSuccess) {
                                                         
                                                         auto latitude = currentLocation.coordinate.latitude;
                                                         auto longitude = currentLocation.coordinate.longitude;
                                                         NetworkManager *network = [[NetworkManager alloc]init];
                                                         self.pinsArray = nil;
                                                         NSLog(@"map id: %@",self.mapID);
                                                         NSLog(@"nilai latitude: %f",latitude);
                                                         NSLog(@"nilai longitude: %f",longitude);
                                                         self.pinsArray = [network retrivePinByMap:[NSString stringWithFormat:@"%f", latitude]  longitude:[NSString stringWithFormat:@"%f", longitude] map:self.mapID];
                                                     
                                                     
                                                         
#ifndef NO_GM
                                                         GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
                                                         CLLocationCoordinate2D location;
                                                         GMSMarker *marker;
                                                         [self.mapViewLocationDetail clear];
#endif
                                                         
                                                         CLLocation *myLocation = self.mapViewLocationDetail.myLocation;
                                                         //    NSLog(@"report my location : %@",myLocation);
#ifndef NO_GM
                                                         self.indexPin = 0;
                                                         for (int i =0 ; i < [self.pinsArray count]; i++) {
                                                             NSString * value = [self.pinsArray objectAtIndex:i];
                                                             location.longitude = [[value valueForKey:@"longitude"] doubleValue];
                                                             location.latitude =  [[value valueForKey:@"latitude"]doubleValue];
                                                             marker = [[GMSMarker alloc] init];
                                                             marker.userData = [NSString stringWithFormat:@"%ld", (long)self.indexPin];
                                                             marker.icon = [UIImage imageNamed:(@"pin.png")];
                                                             self.indexPin++;
                                                             marker.position = CLLocationCoordinate2DMake(location.latitude, location.longitude);
                                                             CGPoint anchor = CGPointMake(0.5f, 0.5f);
                                                             marker.groundAnchor = anchor;
                                                             bounds = [bounds includingCoordinate:marker.position];
                                                             marker.map = self.mapViewLocationDetail;
                                                         }
                                                         //    NSLog(@"nilai bounds before nya adalah : %@",bounds);
                                                         CLLocationCoordinate2D location2D= CLLocationCoordinate2DMake(myLocation.coordinate.latitude, myLocation.coordinate.longitude);
                                                         bounds =[bounds includingCoordinate:location2D];;
                                                         [self.mapViewLocationDetail animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:20.0f]];
#endif
                                                         NSLog(@" nilai pins array : %@",self.pinsArray);
                                                         dispatch_async(dispatch_get_main_queue(), ^{
                                                             [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                         });


                                                     
                                                     }
                                                     
                                                     
                                                     else if (status == INTULocationStatusTimedOut) {
                                                         
                                                     }
                                                     else {
                                                         
                                                     }
                                                 }];
            });

    }
    else
    {
     //do nothing
    }
  

#ifndef NO_GM
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    CLLocationCoordinate2D location;
    GMSMarker *marker;
    [self.mapViewLocationDetail clear]; 
#endif

     CLLocation *myLocation = self.mapViewLocationDetail.myLocation;
//    NSLog(@"report my location : %@",myLocation);
#ifndef NO_GM
    self.indexPin = 0;
    for (int i =0 ; i < [self.pinsArray count]; i++) {
        NSString * value = [self.pinsArray objectAtIndex:i];
        location.longitude = [[value valueForKey:@"longitude"] doubleValue];
        location.latitude =  [[value valueForKey:@"latitude"]doubleValue];
        marker = [[GMSMarker alloc] init];
        marker.userData = [NSString stringWithFormat:@"%ld", (long)self.indexPin];
        marker.icon = [UIImage imageNamed:(@"pin.png")];
        self.indexPin++;
        marker.position = CLLocationCoordinate2DMake(location.latitude, location.longitude);
        CGPoint anchor = CGPointMake(0.5f, 0.5f);
        marker.groundAnchor = anchor;
        bounds = [bounds includingCoordinate:marker.position];
        marker.map = self.mapViewLocationDetail;
    }
//    NSLog(@"nilai bounds before nya adalah : %@",bounds);
    CLLocationCoordinate2D location2D= CLLocationCoordinate2DMake(myLocation.coordinate.latitude, myLocation.coordinate.longitude);
    bounds =[bounds includingCoordinate:location2D];;
    [self.mapViewLocationDetail animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:20.0f]];
#endif

}

#ifndef NO_GM
- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    self.pinUpVC = [self.storyboard instantiateViewControllerWithIdentifier:@"pinUpView"];
    self.pinUpVC.view.frame = CGRectMake(0.0f, 0.0f, self.view.frame.size.width, self.view.frame.size.height);
    self.pinUpVC.view.alpha = 0.0f;
    self.pinUpVC.index = self.indexRow;
    self.pinUpVC.pinID = marker.userData;
    self.pinUpVC.bookmarkMap = self.bookmarkMap;
    self.pinUpVC.arrayPINS = self.pinsArray;
    self.pinUpVC.isFromBookmark = self.isFromBookmarked;
    [self.view insertSubview:self.pinUpVC.view aboveSubview:self.view];
    [UIView animateWithDuration:0.2f animations:^{
        self.pinUpVC.view.alpha = 1.1f;
    }completion:^(BOOL finished){
    }];

    return YES;
}
#endif

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)addLocation:(id)sender {
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"hasSignIn"])
    {
        self.pinCre = [[PinCreationViewController alloc]init];
        self.pinCre= (PinCreationViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"PinCreationViewController"];
        self.pinCre.view.frame = CGRectMake(0.0f, 0.0f, self.view.frame.size.width, self.view.frame.size.height);
        self.pinCre.view.alpha = 0.0f;
        self.pinCre.mapID = self.mapID;
        
        INTULocationManager *locMgr = [INTULocationManager sharedInstance];
        [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyHouse
                                           timeout:10.0
                              delayUntilAuthorized:YES  // This parameter is optional, defaults to NO if omitted
                                             block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                                 if (status == INTULocationStatusSuccess) {
                                                     NSLog(@"address current location : %@",self.address);
                                                     
                                                 }
                                                 
                                                 
                                                 else if (status == INTULocationStatusTimedOut) {
                                                     
                                                 }
                                                 else {
                                                     
                                                 }
                                             }];
        CLLocation *myLocation = self.mapViewLocationDetail.myLocation;
        self.pinCre.longitude = myLocation.coordinate.longitude;
        self.pinCre.latitude = myLocation.coordinate.latitude;
//        NSLog(@"nilai latitude : %f",myLocation.coordinate.latitude);
        NSLog(@"nilai longitude : %f",myLocation.coordinate.longitude);
        NSLog(@"nilai address : %@",self.address);
        self.pinCre.address = self.address;

        [self.view insertSubview:self.pinCre.view aboveSubview:self.view];
        [UIView animateWithDuration:0.2f animations:^{
            self.pinCre.view.alpha = 1.1f;
        }completion:^(BOOL finished){}];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information"
                                                        message:@"Please signup or login to add this pin"
                                                       delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"Cancel",@"Login/Signup" ,nil];
        [alert show];
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 1) {
        NSLog(@"nilai parent VC : %@",self.parentViewController.parentViewController);
        LoginVC *login = [self.storyboard instantiateViewControllerWithIdentifier:@"LOGIN"];
        [self presentViewController:login animated:YES completion:nil];
    }
    else if (buttonIndex == 0)
    {
        
        
    }
}

- (IBAction)shareLocation:(id)sender {
    NSLog(@"share Location");
    NSString *originalString = self.titleLocation;
    NSString *newString = [originalString stringByReplacingOccurrencesOfString:@" " withString:@"-"];
    NSLog(@"map slug = %@",newString);
    NSLog(@"map id = %@",self.mapID);
    NSString *url = [NSString stringWithFormat:@"anymap.co/maps/%@/%@",newString,self.mapID];
    NSString *shareString = [NSString stringWithFormat:@"Check out  '%@' %@ ",originalString,url];
    NSLog(@"final Share = %@",shareString);
    NSURL *myWebsite = [NSURL URLWithString:url];
    NSArray *objectsToShare = @[shareString, myWebsite];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    

    activityVC.completionWithItemsHandler = ^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError) {
        
        if (completed) {
            NSLog(@"success");
        }
        else
        {
            NSLog(@"not success");
            
        }
    };
    
    [self presentViewController:activityVC animated:YES completion:nil];
}
- (IBAction)report:(id)sender {
    NSLog(@"show report");
    self.report = [[ReportViewController alloc]initWithNibName:@"OverflowMapTopic" bundle:nil];
    self.report.view.alpha = 0.0f;
    self.report.view.frame = CGRectMake(self.view.frame.size.width - (self.report.view.frame.size.width +16.0),20.0f, self.report.view.frame.size.width, self.report.view.frame.size.height);
    [self.view insertSubview:self.report.view aboveSubview:self.view];
    self.report.delegateOverFlowMap = self;
    [UIView animateWithDuration:0.2f animations:^{
        self.report.view.alpha =1.0f;
    }completion:^(BOOL finished){}];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)showEdit
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"hasSignIn"])
        {
            NetworkManager *network =[[NetworkManager alloc]init];
            bool statusReport =    [network addLog:@"map_id" addLogID:self.mapID];
            if (statusReport) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertView *alert= [[UIAlertView alloc ]initWithTitle:@"Information" message:@"This map has been reported" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
                    [alert show];
                });
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertView *alert= [[UIAlertView alloc ]initWithTitle:@"Information" message:@"failed to report" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
                    [alert show];
                });
            }
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information"
                                                                message:@"Please signup or login to report this map"
                                                               delegate:self
                                                      cancelButtonTitle:nil
                                                      otherButtonTitles:@"Cancel",@"Login/Signup" ,nil];
                [alert show];
            });
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
           
            [self hideOverFlow];
        });
    });
    
}
- (void)hideOverFlow{
    [UIView animateWithDuration:0.2f animations:^{
        self.report.view.alpha =0.0f;
    }completion:^(BOOL finished){
        [self.report.view removeFromSuperview];
    }];
}

@end
