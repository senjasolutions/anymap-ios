//
//  LoginVC.h
//  AnyMap
//
//  Created by Antonio M on 15/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginVC : UIViewController<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property(strong,nonatomic)UIActivityIndicatorView *spinner;
@property(strong,nonatomic)UIImageView *overlayImageView;
@end
