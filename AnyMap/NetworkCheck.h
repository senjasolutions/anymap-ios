//
//  NetworkCheck.h
//  AnyMap
//
//  Created by Antonio M on 13/02/16.
//  Copyright © 2016 AnyMap. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "Reachability.h"

@interface NetworkCheck : NSObject {
    Reachability * internetReachable;
}

+ (NetworkCheck*)instance;
- (NetworkStatus)internetStatus;
- (void)addTarget:(id)target selector:(SEL)selector;
- (void)removeTarget:(id)target;

@end
