//
//  RootNavTabBarViewController.h
//  AnyMap
//
//  Created by Antonio M on 17/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootNavTabBarViewController : UITabBarController

@end
