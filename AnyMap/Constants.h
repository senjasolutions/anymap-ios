//
//  Constants.h
//  AnyMap
//
//  Created by Antonio M on 07/12/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//


/**
 this are the constant variable for notification , it will use to Google Cloud Messaged
 */
#define NOTIFICATION_ID  @"id"
#define NOTIFICATION_TYPE  @"type"
#define NOTIFICATION_MESSAGE  @"message"
#define NOTIFICATION_PIN_ID  @"pin_id"
#define NOTIFICATION_PIN_TITLE  @"pin_title"
#define NOTIFICATION_PIN_IMAGE  @"pin_image"
#define NOTIFICATION_PIN_DESCRIPTION   @"pin_description"
#define NOTIFICATION_MAP_ID  @"map_id"
#define NOTIFICATION_MAP_TITLE  @"map_title"
#define NOTIFICATION_MAP_DESCRIPTION  @"map_description"
#define NOTIFICATION_MAP_NO_OF_PINS  @"map_no_of_pins"
#define NOTIFICATION_MAP_CREATOR_ID  @"map_creator_id"
#define NOTIFICATION_MAP_CREATOR_IMAGE  @"map_creator_image"
#define NOTIFICATION_PROFILE_ID  @"profile_id"
#define NOTIFICATION_PROFILE_NAME  @"profile_name"
#define NOTIFICATION_PROFILE_IMAGE  @"profile_image"
#define NOTIFICATION_CREATED_AT  @"created_at"
#define NOTIFICATION_IS_PUSH  @"is_push"
#define NOTIFICATION_VIBRATE  @"vibrate"
#define NOTIFICATION_FROM  @"from"
#define NOTIFICATION_TITLE  @"message_title"
#define NOTIFICATION_SUB_TITLE  @"subtitle"
#define NOTIFICATION_TICKER_TEXT  @"tickerText"
#define NOTIFICATION_TYPE_NO_ACTION  @"0"
#define NOTIFICATION_TYPE_GO_TO_MAIN  @"1"
#define NOTIFICATION_TYPE_GO_TO_PIN  @"2"
#define NOTIFICATION_TYPE_GO_TO_PROFILE  @"3"
#define NOTIFICATION_TYPE_GO_TO_MAP  @"4"
#define NOTIFICATION_TYPE_DISABLE_ANALYTIC  @"5"
#define NOTIFICATION_TYPE_DISABLE_MAP_TRACKER  @"6"
#define NOTIFICATIONS_LOAD_LIMIT  @"25"
#ifdef DEBUG
        #define APIURL @"http://52.76.12.169/index.php/"
    #else
        #define APIURL @"https://api.anymap.co/index.php/"
#endif
#import <Foundation/Foundation.h>

@interface Constants : NSObject


@end
