/*-*-objc-*-*/

#import <UIKit/UIKit.h>

typedef struct {
    char *memory;
    size_t size;
    int index;
} AMImageData;

#ifdef __cplusplus
extern "C" {
#endif
size_t AMSaveImage(char const *dl_img, size_t size, size_t nmemb, AMImageData *mem_img);
#ifdef __cplusplus
}
#endif

@interface AMImageManager : NSObject

+ (AMImageManager *)sharedManager;
- (void)setImage:(UIImage *)image forURL:(NSURL *)url;
- (UIImage *)imageForURL:(NSURL *)url;

@end
