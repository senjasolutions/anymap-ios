
#import "CommentListTableViewController.h"
#import "PinUpDetailViewController.h"
#import "PinCommentTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NetworkManager.h"
@implementation CommentListTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    CGFloat ratio = 0;
    if (screenHeight == 480.0f)
    {
        ratio = 57;
    }
    else if (screenHeight == 568.0f)
    {
        ratio = 57;
    }
    else if (screenHeight == 667.0f)
    {
        ratio =57;
    }
    else if (screenHeight == 736.0f)
    {
        ratio = 57;
    }
    
    self.tableView.rowHeight = ratio;
    [self.tableView setContentInset:UIEdgeInsetsMake(0.0, 0.0, 9.0, 0.0)];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return  self.arrayComments.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PinCommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"pinComment"];
    
    if (!cell) {
        
        [tableView registerNib:[UINib nibWithNibName:@"PinCommentCell" bundle:nil] forCellReuseIdentifier:@"pinComment"];
        
        cell = [tableView dequeueReusableCellWithIdentifier:@"pinComment" forIndexPath:indexPath];
    }
    
    NSString *value =[self.arrayComments objectAtIndex:indexPath.row];
    cell.userNameComment.text = [value valueForKey:@"comment_author"];
    cell.commentMessage.text = [value valueForKey:@"comment"];
    cell.timeComment.text = [value valueForKey:@"comment_date"];
    cell.profileUserComment.tag = indexPath.row;
    
    if ([value valueForKey:@"comment_author_image"] == (id)[NSNull null]) {
        
        cell.profileUserComment.image = [UIImage imageNamed:@"placeholder_pic_big"];
    }
    else
    {
        
        [cell.profileUserComment sd_setImageWithURL:[NSURL URLWithString:[value valueForKey:@"comment_author_image"]] placeholderImage:[UIImage imageNamed:@"placeholder_pic_big"] ];
    }
    return cell;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

@end
