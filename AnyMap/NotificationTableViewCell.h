//
//  NotificationTableViewCell.h
//  AnyMap
//
//  Created by Antonio M on 23/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 A NotificationTableViewCell table view cell represents an
 */
@interface NotificationTableViewCell : UITableViewCell
/**set  for image identifier */
@property (strong, nonatomic) IBOutlet UIImageView *notificationImage;
/**set  for descrption of notif identifier */
@property (strong, nonatomic) IBOutlet UILabel *notificationDescrip;
/**set  for time of the notification */
@property (strong, nonatomic) IBOutlet UILabel *notificationTime;

@end
