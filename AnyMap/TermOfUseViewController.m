//
//  TermOfUseViewController.m
//  AnyMap
//
//  Created by Antonio M on 21/10/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import "TermOfUseViewController.h"

@interface TermOfUseViewController ()
@property (strong, nonatomic) IBOutlet UIView *tapDumy;

@end

@implementation TermOfUseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeView:)];
          [tapGesture setNumberOfTapsRequired:1];
            [tapGesture setNumberOfTouchesRequired:1];
         [self.tapDumy addGestureRecognizer:tapGesture];
    }
- (void)closeView:(UITapGestureRecognizer *)sender
{
    [UIView animateWithDuration:0.2f animations:^{
        self.view.alpha = 0.0f;
    }completion:^(BOOL FINISHED)
     {
         [self.view removeFromSuperview];
     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)back:(id)sender {
    [UIView animateWithDuration:0.2f animations:^{
        self.view.alpha = 0.0f;
    }completion:^(BOOL FINISHED)
     {
         [self.view removeFromSuperview];
     }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)done:(id)sender {
    
    [UIView animateWithDuration:0.2f animations:^{
        self.view.alpha = 0.0f;
    }completion:^(BOOL FINISHED)
     {
         [self.view removeFromSuperview];
     }];
}
@end
