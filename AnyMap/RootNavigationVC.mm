//
//  RootNavigationVC.m
//  AnyMap
//
//  Created by Antonio M on 16/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.

#import "AppDelegate.h"
#import "RootNavTabBarViewController.h"
#import "RootNavigationVC.h"

@interface RootNavigationVC ()

@end

@implementation RootNavigationVC

- (void)viewDidLoad
{
    [super viewDidLoad];    
     if(![[NSUserDefaults standardUserDefaults] objectForKey:@"has_run_before"])
     {
         [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"has_run_before"];
         [self.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
         self.navigationBar.shadowImage = [UIImage new];
     }
     else
     {
         [self.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
         self.navigationBar.shadowImage = [UIImage new];
         [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:41.f/255.f green:150.f/255.f blue:243.f/255 alpha:1.0f]];
         [[UINavigationBar appearance]setTranslucent:NO];
         RootNavTabBarViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ROOTTABBAR"];
         [self pushViewController:vc animated:NO];

     }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
