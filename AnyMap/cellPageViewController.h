//
//  cellPageViewController.h
//  AnyMap
//
//  Created by Antonio M on 30/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 A cellPageViewController of view cell represents an
 */
@interface cellPageViewController : UIViewController<UIPageViewControllerDataSource,UIPageViewControllerDelegate>
/**set an array for image background identifier */
@property (strong, nonatomic) NSArray *imageBackground;
/**a property for get the collection carousel page */
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;

@end
