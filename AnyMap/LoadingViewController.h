/*-*-objc-*-*/


#import <UIKit/UIKit.h>

#import "NetworkManager.h"
#import <CoreLocation/CoreLocation.h>


@interface LoadingViewController : UIViewController <CLLocationManagerDelegate>
@property (strong,nonatomic) CLLocationManager *locationManager;
@property (strong,nonatomic) CLLocation *currentLocation;
@end
