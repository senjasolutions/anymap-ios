/*-*-c++-*-*/

#ifndef __ANYMAP_HTTP_GET_REQUEST_H__
#define __ANYMAP_HTTP_GET_REQUEST_H__

#include "HTTPRequest.h"

namespace AnyMap {

template<typename T> class HTTPGETRequest : public HTTPRequest<T> {
public:
    HTTPGETRequest(std::string const& base_url, std::map<std::string, std::string>* params)
        : HTTPRequest<T>{base_url}
    {
        if (HTTPRequest<T>::get_handle()) {
            auto url = HTTPRequest<T>::get_url();
            if (params) {
                *url += "?";
                for (auto param : *params) {
                    *url += param.first + "=" + param.second;
                    if (param != *--params->end())
                        *url += "&";
                }
            }
        };
    }
    HTTPGETRequest(std::string const& base_url)
        : HTTPGETRequest<T>{base_url, nullptr} {}
};

}

#endif
