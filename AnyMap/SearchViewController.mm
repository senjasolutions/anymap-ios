//
//  SearchViewController.m
//  AnyMap
//
//  Created by Antonio M on 19/10/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import "SearchViewController.h"
#import "SearchTableViewController.h"
@interface SearchViewController ()

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.result.text = self.resultString;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    {
        if ([segue.identifier isEqualToString:@"search"]) {
            SearchTableViewController * childViewController = (SearchTableViewController *) [segue destinationViewController];
            childViewController.term = self.resultString;
            NSLog(@"map detail search");
        }
    }
}


@end
