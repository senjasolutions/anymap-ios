//
//  BoardingVC.h
//  AnyMap
//
//  Created by Antonio M on 15/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
@interface BoardingVC : UIViewController<UIPageViewControllerDataSource>
@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *pageTitles;
@property (strong, nonatomic) NSArray *pageImages;
@property (strong,nonatomic)NSMutableArray *arrayPages;
@property(weak,nonatomic)ViewController *vc;
@end
