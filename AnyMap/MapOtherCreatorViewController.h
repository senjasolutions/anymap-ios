//
//  MapOtherCreatorViewController.h
//  AnyMap
//
//  Created by Antonio M on 21/01/16.
//  Copyright © 2016 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapOtherCreatorViewController : UITableViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong,nonatomic)NSMutableArray *mapsOtherCreator;
@property (strong,nonatomic)NSString *mapID;
@property (strong,nonatomic)NSString *creator_id;
@end
