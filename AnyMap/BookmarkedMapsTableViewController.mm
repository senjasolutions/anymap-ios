//
//  BookmarkedMapsTableViewController.m
//  AnyMap
//
//  Created by Antonio M on 09/11/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import "AppDelegate.h"

#import "ExploreMapListTableViewCell.h"
#import "MapDetailLocationViewController.h"
#import "BookmarkedMapsTableViewController.h"
#import <AFNetworking/AFNetworking.h>
#import "UIImageView+AFNetworking.h"
#import "MBProgressHUD.h"
#import "NetworkManager.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation BookmarkedMapsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    CGFloat ratio = 0;
    if (screenHeight == 480.0f)
    {
        ratio = 178;
    }
    else if (screenHeight == 568.0f)
    {
        ratio = 178;
    }
    else if (screenHeight == 667.0f)
    {
        ratio =178;
    }
    else if (screenHeight == 736.0f)
    {
        ratio = 178;
    }
    self.tableView.rowHeight = ratio;
    [self readLocally];
    
}
- (void)readLocally
{
    
     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.followedMaps =  [defaults objectForKey:@"bookmarkedMaps"];
        NSLog(@"the data : %@",self.followedMaps);
    [self.tableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self readLocally];
//    [self loadTheData];
}


- (void)dealloc
{
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   return  self.followedMaps.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *key = [self.followedMaps objectAtIndex:indexPath.row];
    ExploreMapListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"exploreMapList"];
    if (!cell) {
        NSLog(@"add the cell bookmarked");
        [tableView registerNib:[UINib nibWithNibName:@"Clist" bundle:nil] forCellReuseIdentifier:@"exploreMapList"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"exploreMapList" forIndexPath:indexPath];
        
    }
        cell.bookmark.selected = YES;
    cell.bookmark.tag = indexPath.row;
        [cell.bookmark addTarget:self action:@selector(addBookmark:) forControlEvents:UIControlEventTouchUpInside];
        cell.creatorButton.hidden = YES;
        cell.headerMap.text = [key valueForKey:@"title"];
        cell.counterLocation.text =[NSString stringWithFormat:@"%@ Locations",[key valueForKey: @"no_of_pins"]];
        cell.descriptionMap.text =[key valueForKey:@"description"];
        [cell.creatorName setTitle:[key valueForKey:@"profile_name"] forState:UIControlStateNormal];
        cell.creatorPhoto.hidden = YES;
        if ([key valueForKey:@"image"] == (id)[NSNull null]) {
            cell.map.image = [UIImage imageNamed:@"placeholder_pic_big"];
        }
        else
        {
            [cell.map sd_setImageWithURL:[NSURL URLWithString:[key valueForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"placeholder_pic_big"] ];
        }
    
    return cell;
}

- (void)addBookmark:(UIButton *)sender
{
  
    sender.selected = !sender.selected;
    NSMutableArray *array =[self.followedMaps objectAtIndex:sender.tag];
    NetworkManager *network =[[NetworkManager alloc]init];
    NSLog(@"map id : %ld",(long)sender.tag);
    if(!sender.selected)
    {
        NSString *bookmarkMap = [NSString stringWithFormat:@"%@|1",[array valueForKey:@"id"]];
        [network unBookmarkMap:bookmarkMap statusMap:[array valueForKey:@"id"] arrayMap:array];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        self.followedMaps = nil;
        self.followedMaps = [defaults objectForKey:@"bookmarkedMaps"];
        [self.tableView reloadData];
    };
    
}
-(void)bookmarkMapStatus:(NSString *)mapId
{
  
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:[NSString stringWithFormat:@"%@maps/followed-map?user_id=%@&access-token=%@",APIURL,[[[NSUserDefaults standardUserDefaults]objectForKey:@"installDevice"] objectForKey:@"id"],[[[NSUserDefaults standardUserDefaults]objectForKey:@"installDevice"] objectForKey:@"api_key"]] parameters:@{@"followed_map_ids":mapId } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"json followed map news : %@",responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", operation.responseObject);
    }];
    

}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *key = [self.followedMaps objectAtIndex:indexPath.row];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MapDetailLocationViewController *mapDetailLocation = [storyboard instantiateViewControllerWithIdentifier:@"MapDetailLocation"];
    mapDetailLocation.titleLocation = [key valueForKey:@"title"];
    mapDetailLocation.isFromBookmarked = YES;
    mapDetailLocation.mapID = [key valueForKey:@"id"];
    mapDetailLocation.bookmarkMap = [[self.followedMaps objectAtIndex:indexPath.row] mutableCopy];
    [self presentViewController:mapDetailLocation animated:YES completion:nil];

}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
