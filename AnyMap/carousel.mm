//
//  carousel.m
//  AnyMap
//
//  Created by Antonio M on 30/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//
#ifdef DEBUG
#endif
#import "carousel.h"
#import "PinUpDetailViewController.h"

@interface carousel ()
@property (strong,nonatomic)PinUpDetailViewController * pinUpDetail;
@end

@implementation carousel

- (void)viewDidLoad {
    [super viewDidLoad];
    self.background.image = self.imageBackground;
    self.headerMap.text = self.header;
    // Do any additional setup after loading the view.
    
}
- (void)showDetailPin:(UITapGestureRecognizer *)sender
{
    NSLog(@"tap the image");
    NSLog(@"id pin yang tap : %@",self.idPin);
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSLog(@"the result %@ ,%@",[self class], event.class);
    NSLog(@"id pin : %@",self.idPin);
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)showDetailMap
{
    self->_pinUpDetail = [self.storyboard instantiateViewControllerWithIdentifier:@"pinUpView"];
     self->_pinUpDetail.view.frame = CGRectMake(0.0f, 0.0f, self.tabBarController.view.frame.size.width, self.tabBarController.view.frame.size.height);
    self->_pinUpDetail .view.alpha = 0.0f;
    [self.tabBarController.view insertSubview: self->_pinUpDetail .view aboveSubview:self.tabBarController.view];
    [UIView animateWithDuration:0.2f animations:^{
         self->_pinUpDetail.view.alpha = 1.0f;
    }completion:^(BOOL finished){
    }];
;
    
//    auto maps = ((AppDelegate *)[UIApplication sharedApplication].delegate).currentUser->get_maps();
//    auto map = (*maps)[static_cast<int>(self.index)];
//    auto pins= map["pins"];
//    auto pin  = (pins)[static_cast<int>(senderButton.tag)];
//    
//    auto image_manager = [AMImageManager sharedManager];
//    
//    auto image_url = pin["image"].asString();
//    __block auto image = [image_manager imageForURL:[NSURL URLWithString:[NSString stringWithUTF8String:image_url.c_str()]]];
//    if (image)
//         self->_pinUpDetail.cover = image;
//    else {
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//            auto image_data = new AMImageData();
//            image_data->memory = static_cast<char*>(malloc(1));
//            image_data->size = 0;
//            AnyMap::HTTPGETRequest<AMImageData> request{image_url};
//            request.send(image_data, AMSaveImage);
//            image = [UIImage imageWithData:[NSData dataWithBytes:image_data->memory length:image_data->size]];
//            [image_manager setImage:image forURL:[NSURL URLWithString:[NSString stringWithUTF8String:image_url.c_str()]]];
//            dispatch_async(dispatch_get_main_queue(), ^{
//                 self->_pinUpDetail.cover = image;
//            });
//        });
//    }
//    
//    auto profile_image_url = map["profile_image"].asString();
//    __block auto profileImage = [image_manager imageForURL:[NSURL URLWithString:[NSString stringWithUTF8String:profile_image_url.c_str()]]];
//    if (profileImage)
//         self->_pinUpDetail.creator = profileImage;
//    else {
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//            auto image_data = new AMImageData();
//            image_data->memory = static_cast<char*>(malloc(1));
//            image_data->size = 0;
//            AnyMap::HTTPGETRequest<AMImageData> request{profile_image_url};
//            request.send(image_data, AMSaveImage);
//            profileImage = [UIImage imageWithData:[NSData dataWithBytes:image_data->memory length:image_data->size]];
//            [image_manager setImage:profileImage forURL:[NSURL URLWithString:[NSString stringWithUTF8String:profile_image_url.c_str()]]];
//            dispatch_async(dispatch_get_main_queue(), ^{
//                 self->_pinUpDetail.creator = profileImage;
//            });
//        });
//    }
//     self->_pinUpDetail .profile = [NSString stringWithUTF8String:map["profile_name"].asString().c_str()];
//     self->_pinUpDetail .road =[NSString stringWithUTF8String:pin["address"].asString().c_str()];
//    NSString * distance = [NSString stringWithUTF8String:pin["distance"].asString().c_str()];
//    float convertDistance  = ceilf([distance floatValue] *10) /10;
//     self->_pinUpDetail .distanceRoad = [NSString stringWithFormat:@"%.1fkm",convertDistance];
//     self->_pinUpDetail .descript =[NSString stringWithUTF8String:pin["description"].asString().c_str()];
//     self->_pinUpDetail .title =[NSString stringWithUTF8String:pin["title"].asString().c_str()];
//     self->_pinUpDetail .mapTitleS =[NSString stringWithUTF8String:map["title"].asString().c_str()];
//     self->_pinUpDetail .indexPin = senderButton.tag;
//     self->_pinUpDetail .indexMap = self.index;
////    UIButton *likeButton = ((PinUpCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:senderButton.tag inSection:0]]).pinLike;
//    UIButton *likeButton;
//    if (likeButton.selected)
//         self->_pinUpDetail .likeCountString = [likeButton titleForState:UIControlStateSelected];
//    else
//         self->_pinUpDetail .likeCountString = [likeButton titleForState:UIControlStateNormal];

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
