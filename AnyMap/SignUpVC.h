//
//  SignUpVC.h
//  AnyMap
//
//  Created by Antonio M on 15/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignUpVC : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *username;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UIButton *female;
@property (weak, nonatomic) IBOutlet UIButton *male;
@property (weak, nonatomic) IBOutlet UIView *accountDataPanel;
@property (weak, nonatomic) IBOutlet UIView *facebookPanel;
@property(strong,nonatomic)UIActivityIndicatorView *spinner;
@property(strong,nonatomic)UIImageView *overlayImageView;
@end
