/*-*-objc-*-*/
//  OnBoardPageVC.h
//  AnyMap
//
//  Created by Antonio M on 15/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
@interface OnBoardPageVC : UIViewController <CLLocationManagerDelegate>
@property (strong,nonatomic) CLLocationManager *locationManager;
@property (strong,nonatomic) CLLocation *currentLocation;
@property (weak, nonatomic) IBOutlet UIImageView *ImageViewOnBoard;
@property (weak, nonatomic) IBOutlet UIImageView *ImageViewIconOnBoard;
@property (weak, nonatomic) IBOutlet UILabel *TitleOnBoardPage;
@property (weak, nonatomic) IBOutlet UILabel *descOnBoardPage;
@property(strong,nonatomic)UIImage *imageBackground;
@property(strong,nonatomic)UIImage *imageLogo;
@property(strong,nonatomic)NSString *titleLabel;
@property(strong,nonatomic)NSString *descLabel;
@property(assign ,nonatomic)NSInteger pageIndex;
@property (weak, nonatomic) IBOutlet UIView *interestPageView;
@property (weak, nonatomic) IBOutlet UIView *onBoardingPage;
@property(nonatomic)BOOL currentView;
@property (strong, nonatomic) IBOutlet UIButton *startExplore;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttons;
@end
