//
//  CardMapPhotoCollectionViewCell.h
//  AnyMap
//
//  Created by Antonio M on 23/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

/*-*-objc-*-*/
#import <UIKit/UIKit.h>

/**
 A CardMapPhotoCollectionViewCell collection view cell represents an
 */
@interface CardMapPhotoCollectionViewCell : UICollectionViewCell
/**a to show map image i
 @param term find by the json image url
 */
@property (weak, nonatomic) IBOutlet UIImageView *photoImage;
/**function to get the pin map by the custom pin image
 @param term find by json image url
 */
@property (weak, nonatomic) IBOutlet UIImageView *pinIconImag;
/**function to get map title
 @param term  by string json map
 */
@property (weak, nonatomic) IBOutlet UILabel *titleHead;
/**function to get count of the location in map view and map list
 @param term find by integer json location
 */
@property (weak, nonatomic) IBOutlet UILabel *counterLocation;
/**function to get the description map
 @param term find by string json description
 */
@property (weak, nonatomic) IBOutlet UILabel *describeTitle;

@end
