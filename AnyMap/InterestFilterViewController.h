//
//  InterestFilterViewController.h
//  AnyMap
//
//  Created by Antonio M on 05/10/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
@protocol filterDelegate
- (void)done;
@end
@interface InterestFilterViewController : UIViewController<CLLocationManagerDelegate>
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttons;
@property (strong, nonatomic) IBOutlet UIView *tapView;
@property (weak)id<filterDelegate> delegateFilter;
@property (strong,nonatomic) CLLocationManager *locationManager;
@property (strong,nonatomic) CLLocation *currentLocation;
@end
