//
//  PinUpViewController.h
//  AnyMap
//
//  Created by Antonio M on 06/10/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PinUpViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate>
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UIView *dummyGesture;
@property (nonatomic)NSInteger index;
@property (nonatomic)NSInteger indexCollection;
@property (nonatomic,strong)NSString *pinID;
@property (nonatomic,strong)NSMutableArray *arrayPINS;
@property (nonatomic)double myLocationLongitude;
@property (nonatomic)double myLocationLatitude;
@property (nonatomic)BOOL isFromBookmark;
@property (strong,nonatomic)NSMutableArray *bookmarkMap;
@end
