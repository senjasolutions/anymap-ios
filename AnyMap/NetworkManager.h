//
//  NetworkManager.h
//  AnyMap
//
//  Created by Antonio M on 14/01/16.
//  Copyright © 2016 AnyMap. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import <AFNetworking/AFNetworking.h>
#import "UIImageView+AFNetworking.h"
#import "MBProgressHUD.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface NetworkManager : NSObject
- (void)installGlobalDevice;
- (BOOL)loginFb: (NSString* )facebookId  token:(NSString* )tokenFB  facebookName :(NSString *)name;
- (BOOL)loginbyAM: (NSString* )email  password:(NSString* )pass;
- (BOOL)signUpByAM: (NSString* )email  password:(NSString* )pass username:(NSString *)user;
- (BOOL)signUpByFacebook: (NSString* )email  facebookID:(NSString* )fbID username:(NSString *)user tokenFB:(NSString*)token;

- (NSMutableArray *)maps_by_response:(NSString*)latitude longitude:(NSString *)point;
- (NSMutableArray *)retrivePinByMap:(NSString*)latitude longitude:(NSString *)point map:(NSString*)mapID;
- (void)maps_by_followed;
- (void)bookmarkMap :(NSString *)mapID statusMap:(NSString*)map arrayMap:(NSMutableArray *)maps;
- (void)unBookmarkMap :(NSString *)mapID statusMap:(NSString*)map arrayMap:(NSMutableArray*)maps;
- (void)likeThePin :(NSString *)pinID statusPin:(NSString*)map;
- (void)unLikeThePin :(NSString *)pinID statusPin:(NSString*)map;
- (NSMutableArray *)geTheCommentList:(NSString *)mapID;
- (void)addTheComment:(NSString *)pinID comment:(NSString*)commmentList;
- (void)updateGCM:(NSString*)token;
- (void)addFilteringCategory;
- (NSString *)addPin :(NSString * )mapID titlePin:(NSString*)title descriptionPin:(NSString*)description latitudePin:(NSString *)latitude longitudePin:(NSString*)longitude addressPin:(NSString*)address pathImagePin:(NSString *)filePath imagePin:(UIImage*)image;
-(NSMutableArray * )geTheAddedPins;
-(NSString *)editProfile:(NSString*)name  imageURL :(NSString*)image imagePath:(UIImage *)image
;
- (BOOL)addFeedback:(NSString*)feedbackName;
- (BOOL)addLog:(NSString *)addlogTypes addLogID:(NSString*)theLogID;
- (void)followUser :(NSString *)userID statusUser:(NSString*)user;
- (void)unFollowUser :(NSString *)userID statusUser:(NSString*)user;

@end
