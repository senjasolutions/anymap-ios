/*-*-c++-*-*/

#ifndef __ANYMAP_HTTP_POST_REQUEST_H__
#define __ANYMAP_HTTP_POST_REQUEST_H__

#include "HTTPRequest.h"

namespace AnyMap {

template<typename T> class HTTPPOSTRequest : public HTTPRequest<T> {
public:
    HTTPPOSTRequest(std::string const& base_url, std::map<std::string, std::string> const* params)
        : HTTPRequest<T>{base_url}
    {
        auto handle = HTTPRequest<T>::get_handle();
        if (handle) {
#ifdef DEBUG
            std::cout << "POST: " << std::endl;
#endif
            post = nullptr;
            struct curl_httppost* last = nullptr;
            for (auto param : *params) {
                auto key = param.first;
                if (key == "profile_image" || key == "pin_image")
                    curl_formadd(&post, &last,
                                 CURLFORM_COPYNAME, key.c_str(),
                                 CURLFORM_FILECONTENT, param.second.c_str(),
                                 CURLFORM_END);
                else
                    curl_formadd(&post, &last,
                                 CURLFORM_COPYNAME, key.c_str(),
                                 CURLFORM_COPYCONTENTS, param.second.c_str(),
                                 CURLFORM_END);
#ifdef DEBUG
                std::cout << "\t" << last->name << " : " << last->contents << std::endl;
#endif
            }
            curl_easy_setopt(handle, CURLOPT_HTTPPOST, post);
        }
    }

    ~HTTPPOSTRequest() { if (post) curl_formfree(post); }
private:
    struct curl_httppost* post;
};

}

#endif
