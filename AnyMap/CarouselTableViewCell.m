//
//  CarouselTableViewCell.m
//  AnyMap
//
//  Created by Antonio M on 29/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import "CarouselTableViewCell.h"

@implementation CarouselTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
