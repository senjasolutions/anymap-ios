/*-*-objc-*-*/
//  LoginVC.m
//  AnyMap
//
//  Created by Antonio M on 15/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#ifndef NO_FB
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#endif
#import "AppDelegate.h"
#import "LoginVC.h"
#import "MBProgressHUD.h"
#import "RootNavTabBarViewController.h"
#import "NetworkManager.h"

@interface LoginVC ()
@property (nonatomic) CGFloat screenHeight;
@end

@implementation LoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.email.delegate = self;
    self.password.delegate=self;
    self.screenHeight =   [UIScreen mainScreen].bounds.size.height;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)waitScreen
{
    self.spinner = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.spinner.center = CGPointMake(self.view.bounds.size.width * 0.5, self.view.bounds.size.height * 0.5);
    [self.view addSubview:self.spinner];
    [self.spinner startAnimating];
    self.overlayImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"overlay_shade"]];
    self.overlayImageView.frame =CGRectMake(0.0f, 0.0f, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:self.overlayImageView];
    self.overlayImageView.alpha = 0.0f;
    [UIView animateWithDuration:0.25f
                     animations:^ {
                         self.overlayImageView.alpha = 1.0f;
                     }
                     completion:^(BOOL finished) {
                     }];
}
- (IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)log_in:(id)sender {
    if ([self.email.text isEqualToString:@""] || [self.password.text isEqualToString:@""] ) {
        UIAlertView *alert= [[UIAlertView alloc ]initWithTitle:@"Information" message:@"text field cant be empty" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        __block BOOL loginSuccessed ;
        NetworkManager *network =[[NetworkManager alloc]init];
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = @"Loading";
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            loginSuccessed  =  [network loginbyAM:self.email.text password:self.password.text];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (loginSuccessed) {
                    UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    RootNavTabBarViewController *rootTabBar =  [main instantiateViewControllerWithIdentifier:@"ROOTTABBAR"];
                    [self presentViewController:rootTabBar animated:YES completion:nil];
                }
                else
                {
                    UIAlertView *alert= [[UIAlertView alloc ]initWithTitle:@"Information" message:@"Invalid email or password" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
                    [alert show];
                    [self.spinner stopAnimating];
                    self.overlayImageView.alpha = 0.0;
                    [self.overlayImageView removeFromSuperview];
                }

                [MBProgressHUD hideHUDForView:self.view animated:YES];
            });
        });
        

      }
}

#pragma UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    textField.placeholder = nil;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.tag ==0 ) {
        textField.placeholder = @"Email";
    }else if (textField.tag ==1)
    {
        textField.placeholder = @"Password";
    }
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}
#pragma loginFacebook
- (IBAction)loginFB:(id)sender {
#ifndef NO_FB
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"public_profile",@"email"] fromViewController:self.navigationController handler:
     ^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error");
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
         } else {
             NSLog(@"Logged in");
             [self waitScreen];
             if ([result.grantedPermissions containsObject:@"email"])
             {
                 if ([FBSDKAccessToken currentAccessToken])
                 {
                     [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, link, first_name, last_name, picture.type(large), email, birthday, bio ,location ,friends ,hometown , friendlists"}]
                      startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                          
                          if (!error) {
                              NSLog(@"fetched user:%@  and Email : %@", result,result[@"email"]);
                              NSLog(@"facebook id  : %@",result[@"id"]);
                              
                              NSLog(@"facebook access token  : %@",[FBSDKAccessToken currentAccessToken].tokenString);
                              
                              NSString *facebookID = result[@"id"];
                              NSString *token = [FBSDKAccessToken currentAccessToken].tokenString;
                              
                              NetworkManager * network = [[NetworkManager alloc]init];
                              
                              NSString *name =[[NSString alloc]init];
                              NSString *fullname = [result valueForKey:@"name"];
                              NSLog(@"the name  : %@",fullname);
                              if (fullname.length == 0) {
                                  name = [result valueForKey:@"first_name"];
                              }
                              else
                              {
                                  name = fullname;
                              }
                              BOOL loginSuccessed = [network loginFb:facebookID token:token facebookName:name] ;
                              
                              if (loginSuccessed) {
                                  UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                  RootNavTabBarViewController *rootTabBar =  [main instantiateViewControllerWithIdentifier:@"ROOTTABBAR"];
                                  [self presentViewController:rootTabBar animated:YES completion:nil];
                              }
                              else
                              {
                                  UIAlertView *alert= [[UIAlertView alloc ]initWithTitle:@"Information" message:@"Login failed " delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
                                  [alert show];
                                  [self.spinner stopAnimating];
                                  self.overlayImageView.alpha = 0.0;
                                  [self.overlayImageView removeFromSuperview];
                              }
                              
                          }
                      }];
                     
                     
                 }
             }
         }
     }];
#endif
}
- (IBAction)loginGoogle:(id)sender {
//    auto user = ((AppDelegate *)[UIApplication sharedApplication].delegate).currentUser;
//    user->log_in_google("email", "token");
    
}
- (IBAction)forgotPassword:(id)sender {
    
    NetworkManager *network = [[NetworkManager alloc]init];
   __block UITextField *myTextField;
//    network forgotPassword:
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Information"
                                          message:@"Please input your email"
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = NSLocalizedString(@"Email...", @"Email");
         [textField addTarget:self
                       action:@selector(alertTextFieldDidChange:)
             forControlEvents:UIControlEventEditingChanged];
         
         myTextField = textField;
     }];
    
    
    UIAlertAction* no = [UIAlertAction
                         actionWithTitle:@"Cancel"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alertController dismissViewControllerAnimated:YES completion:nil];
                         }];
    
    
    UIAlertAction*  yes = [UIAlertAction
                           actionWithTitle:@"Ok"
                           style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * action)
                           {
                            NSLog(@"show the email : %@",myTextField.text);
                               dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0),^{
                                      BOOL function=  [network addLog:@"email" addLogID:myTextField.text];
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       if (function) {
                                           UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Information" message:@"We will email you the instructions to reset your password" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                                           [alert show];
                                       }
                                       else
                                       {
                                           UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Information" message:@"Check again your email address" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                                           [alert show];
                                       }
                                   });
                               });
                               
                           }];
    
    
    [alertController addAction:no];
    [alertController addAction:yes];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)alertTextFieldDidChange:(UITextField *)sender
{
    UIAlertController *alertController = (UIAlertController *)self.presentedViewController;
    if (alertController)
    {
        UITextField *email = alertController.textFields.firstObject;
        NSLog(@"email show : %@",email.text);
        UIAlertAction *okAction = alertController.actions.lastObject;
        okAction.enabled = email.text.length > 2;
    }
}


@end
