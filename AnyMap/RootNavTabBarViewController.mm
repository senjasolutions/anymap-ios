//
//  RootNavTabBarViewController.m
//  AnyMap
//
//  Created by Antonio M on 17/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import "RootNavTabBarViewController.h"
#import "AppDelegate.h"
#import "LoginVC.h"

@interface RootNavTabBarViewController ()

@end

@implementation RootNavTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBarHidden = YES;
    [[UINavigationBar appearance]setTranslucent:NO];
    NSArray *unselectedImages = [NSArray arrayWithObjects:@"icon_explore_non.png",@"icon_notification_non.png",@"icon_profile_non.png", nil];
    NSArray *selectedImages = [NSArray arrayWithObjects:@"icon_explore.png",@"icon_notification.png",@"icon_profile.png", nil];
    NSArray *tabBarCollection = self.tabBar.items;
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat scale = [UIScreen mainScreen].scale;
    for (int i =0 ;i<tabBarCollection.count; i++)
    {
        UITabBarItem *item = [tabBarCollection objectAtIndex:i];
        UIImage *selectedImage =  [[UIImage imageNamed:[selectedImages objectAtIndex:i]]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        UIImage *unselectedImage = [[UIImage imageNamed:[unselectedImages objectAtIndex:i]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        item = [item initWithTitle:@"" image:unselectedImage selectedImage:selectedImage];
        UIEdgeInsets insets = {
            .top = 3.f,
            .left = 0.f,
            .bottom = -3.f,
            .right = 0.f
        };
        item.imageInsets = insets;
    }
        UIImage* tab_indicator;
        if (scale == 2.f && screenWidth == 320.0f)
        {
        tab_indicator = [UIImage imageNamed:@"tab_selection_indicator@2x.png"];
        }
        else if (scale == 2.f && screenWidth == 375.0f) {
            tab_indicator = [UIImage imageNamed:@"tab_selection_indicator-375w@2x.png"];
        }
        else if (scale == 3.f && screenWidth == 414.0f) {
            tab_indicator = [UIImage imageNamed:@"tab_selection_indicator-414w@3x.png"];
        }
        else {
            tab_indicator = [UIImage imageNamed:@"tab_selection_indicator.png"];
        }
      [self.tabBar setSelectionIndicatorImage:tab_indicator];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
//    if ([item isEqual:[self.tabBar.items objectAtIndex:2]]) {
//        [self setSelectedIndex:0];
//        if (![[NSUserDefaults standardUserDefaults] objectForKey:@"hasSignIn"])
//        {
//            
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information"
//                                                            message:@"Please signup or login to see your profile"
//                                                           delegate:self
//                                                  cancelButtonTitle:nil
//                                                  otherButtonTitles:@"Cancel",@"Login/Signup" ,nil];
//            [alert show];
//            
//            
//        }
//
//    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 1) {
        NSLog(@"nilai parent VC : %@",self.parentViewController.parentViewController);
        LoginVC *login = [self.storyboard instantiateViewControllerWithIdentifier:@"LOGIN"];
        [self presentViewController:login animated:YES completion:nil];
    }
    else if (buttonIndex == 0)
    {
        NSLog(@"index");
        [self setSelectedIndex: 0];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
