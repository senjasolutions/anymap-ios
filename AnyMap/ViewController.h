//
//  ViewController.h
//  AnyMap
//
//  Created by Antonio M on 21/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 this is class , the class to capture boarding page.
 */
@interface ViewController : UIViewController
/**
 A property for page controller of the onboarding view
 */
@property (weak, nonatomic) IBOutlet UIPageControl *paging;
/**
 A property for cover the view under buttons
 */
@property (weak, nonatomic) IBOutlet UIView *footerViewButtons;

/** an action button
   this is the action method to skip onboarding page
 */
- (IBAction)skip:(id)sender;

@end
