//
//  BookmarkedMapsTableViewController.h
//  AnyMap
//
//  Created by Antonio M on 09/11/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
@interface BookmarkedMapsTableViewController : UITableViewController

@property (strong, nonatomic)  NSMutableArray *followedMaps;
@end
