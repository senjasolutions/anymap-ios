//
//  AddedPinsOtherCreatorViewController.h
//  AnyMap
//
//  Created by Antonio M on 25/01/16.
//  Copyright © 2016 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddedPinsOtherCreatorViewController : UITableViewController <UITableViewDataSource,UITableViewDelegate>
@property (strong,nonatomic)NSMutableArray * arrayPinsOtherCreator;
@property (strong,nonatomic)NSString *creator_id;
@end
