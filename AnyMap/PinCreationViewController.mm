//
//  PinCreationViewController.m
//  AnyMap
//
//  Created by Antonio M on 15/10/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import "AppDelegate.h"
#import "PinCreationViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "NetworkManager.h"
#import "MBProgressHUD.h"
@interface PinCreationViewController ()< UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate,UITextViewDelegate>
@property (nonatomic) CGFloat screenHeight;
@property (copy) NSString *urlPath;
@end

@implementation PinCreationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenHeight =   [UIScreen mainScreen].bounds.size.height;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeView:)];
    [tapGesture setNumberOfTapsRequired:1];
    [tapGesture setNumberOfTouchesRequired:1];
    [self.dummyGesture addGestureRecognizer:tapGesture];
    self.urlPath = nil;
    self.writeDescription.delegate = self;
    

}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.nameLocation.delegate = self;
    self.gpsLocation.text = self.address;
    self.gpsLocation.delegate = self;

}
#pragma mark - Text View delegates

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:
(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]) {
        [self setViewMovedUp:NO heightKeyboard:200];
        [textView resignFirstResponder];
    }
    return YES;
}
-(void)textViewDidBeginEditing:(UITextView *)textView{
    NSLog(@"Did begin editing");
    [self setViewMovedUp:YES heightKeyboard:200];
}
-(void)textViewDidChange:(UITextView *)textView{
    NSLog(@"Did Change");
    
}
-(void)textViewDidEndEditing:(UITextView *)textView{
    NSLog(@"Did End editing");
    [self setViewMovedUp:NO heightKeyboard:200];
    
}
-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    return YES;
}
-(void)setViewMovedUp:(BOOL)movedUp heightKeyboard:(double)height
{
    __block  CGRect rect = self.view.frame;
    [UIView animateWithDuration:0.1 animations:^{
        if (movedUp) {
            rect.size.height -= height;
        }else
        {
            rect.size.height += height;
        }
    }completion:^(BOOL finished){self.view.frame = rect;}];
}
- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
//    NSLog(@"MAP ID  : %@",self.mapID);
//    NSLog(@"address  : %@",self.address);
//    NSLog(@"long  : %f",self.longitude);
//    NSLog(@"lat  : %f",self.latitude);
//    [self.gpsLocation becomeFirstResponder];
    [[UITextField appearance] setTintColor:[UIColor blackColor]];
}
- (void)closeView:(UITapGestureRecognizer *)sender
{
    [self removeAnimateView];
}
- (void)removeAnimateView
{
    [UIView animateWithDuration:0.2f animations:^
     {
         self.view.alpha = 0.f;
     } completion:^(BOOL FINISHED)
     {
         [self.view removeFromSuperview];
     }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    textField.placeholder = nil;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.tag ==0 ) {
        textField.placeholder = @"Name this Location";
    }else if (textField.tag ==1)
    {
        textField.placeholder = @"Tracked GPS Location";
    }
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}
- (IBAction)done:(id)sender {
    if ([self.writeDescription.text isEqualToString:@""] || [self.nameLocation.text isEqualToString:@""]) {
        UIAlertView *alert= [[UIAlertView alloc ]initWithTitle:@"Information" message:@"Fields can't be empty" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }

    [self removeAnimateView];
       __block  NSString * infoUploadingImage;
        NetworkManager *network = [[NetworkManager alloc]init];
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = @"Uploading...";
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
             infoUploadingImage  =   [network addPin:self.mapID titlePin:self.nameLocation.text descriptionPin:self.writeDescription.text latitudePin:[NSString stringWithFormat:@"%f", self.latitude] longitudePin:[NSString stringWithFormat:@"%f", self.longitude] addressPin:self.address pathImagePin:self.urlPath imagePin:self.mapPhoto.image];
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([[infoUploadingImage valueForKey:@"status" ] isEqualToString:@"ok"]) {
                    UIAlertView *alert= [[UIAlertView alloc ]initWithTitle:@"Information" message:@"Pin added successfully!" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
                    [alert show];
                }
                
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            });
        });
}
- (IBAction)takeOrPick:(id)sender {

    UIImagePickerController *pickerController = [[UIImagePickerController alloc]
                                                 init];

    pickerController.delegate = self;
    
    [self presentViewController:pickerController animated:YES completion:nil];
}

#pragma mark UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    NSURL *refURL = [info valueForKey:UIImagePickerControllerReferenceURL];
    
    self.mapPhoto.image  = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
    ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *imageAsset)
    {
    
        ALAssetRepresentation *imageRep = [imageAsset defaultRepresentation];
        
        NSLog(@"[imageRep filename] : %@", [imageRep filename]);
        
        [self writeImageData:self.mapPhoto.image filename:[imageRep filename]];
    };
    
    ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
    
    [assetslibrary assetForURL:refURL resultBlock:resultblock failureBlock:nil];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}
- (void)writeImageData:(UIImage *)image filename:(NSString *)name
{
    
    NSData *pngData = UIImagePNGRepresentation(image);
//
//    NSString *urlTemp = [self documentsPathForFileName:name];
//    
//    [pngData writeToFile:urlTemp atomically:YES]; //Write the file
//    
//    self.urlPath = [NSString stringWithFormat:@"%@",urlTemp];
//    
//    NSLog(@"url image : %@",self.urlPath);
//     //read the url data again
    
    NSString  *urlTemp = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/%@", name]];
    
    if (![pngData writeToFile:urlTemp atomically:YES])
    {
        self.urlPath = @"";
        NSLog((@"Failed to cache image data to disk"));
    }
    else
    {
        self.urlPath = [NSString stringWithFormat:@"%@",name];
        NSLog(@"the cachedImagedPath is %@", self.urlPath);
    }
}
- (NSString *)documentsPathForFileName:(NSString *)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    
    return [documentsPath stringByAppendingPathComponent:name];
}

@end
