//
//  OtherCreatorViewController.h
//  AnyMap
//
//  Created by Antonio M on 09/10/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
@interface OtherCreatorViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *profileName;
@property (strong, nonatomic) IBOutlet UILabel *countFollower;
@property (strong, nonatomic) IBOutlet UILabel *countFollowing;
@property (strong, nonatomic) IBOutlet UIImageView *avatarProfile;
@property (strong, nonatomic) IBOutlet UIImageView *coverProfile;
@property (strong, nonatomic) IBOutlet UILabel *profileDesc;
@property (strong, nonatomic) IBOutlet UIButton *forFollow;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentControl;
@property (strong, nonatomic) IBOutlet UIView *containerView;

@property(strong,nonatomic)NSString *profile;
@property(strong,nonatomic)NSString *creator_id;
@property(strong,nonatomic)NSString *counterFollowers;
@property(strong,nonatomic)NSString *counterFollowings;
@property(strong,nonatomic)UIImage *coverProfiles;
@property(strong,nonatomic)NSString *mapID;
@property(strong,nonatomic)NSString *describes;
@property(nonatomic)NSInteger indexUser;
@property(copy, nonatomic) NSString *identifier;
@property(copy, nonatomic) NSString *picURL;
@end
