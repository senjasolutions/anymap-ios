//
//  carousel.m
//  AnyMap
//
//  Created by Antonio M on 30/09/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import "carousel.h"

@interface carousel ()

@end

@implementation carousel

- (void)viewDidLoad {
    [super viewDidLoad];
    self.background.image = self.imageBackground;
    self.headerMap.text = self.header;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
