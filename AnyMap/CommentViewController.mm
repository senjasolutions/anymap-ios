//
//  CommentViewController.m
//  AnyMap
//
//  Created by Antonio M on 23/10/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import "AppDelegate.h"
#import "PinUpDetailViewController.h"
#import "CommentListTableViewController.h"
#import "CommentViewController.h"
#import "NetworkManager.h"

@interface CommentViewController ()<UITextViewDelegate>
@property (nonatomic) CGFloat screenHeight;
@end

@implementation CommentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenHeight =   [UIScreen mainScreen].bounds.size.height;
    self.textCommet.delegate = self;
    self.titlePin.text = self.pinTitle;
    NetworkManager *network = [[NetworkManager alloc]init];
    NSMutableArray *arrayCommentTemp = [network  geTheCommentList:self.pinID];
    ((CommentListTableViewController *)self.childViewControllers.firstObject).arrayComments = [arrayCommentTemp mutableCopy];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
/*
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([segue.identifier isEqualToString:@"embed"]) {
        CommentListTableViewController *comment = (CommentListTableViewController *)[segue destinationViewController];
    }
}
 */

- (IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - Text View delegates

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:
(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]) {
            [self setViewMovedUp:NO heightKeyboard:200];
        [textView resignFirstResponder];
    }
    return YES;
}
-(void)textViewDidBeginEditing:(UITextView *)textView{
    NSLog(@"Did begin editing");
    [self setViewMovedUp:YES heightKeyboard:200];
}
-(void)textViewDidChange:(UITextView *)textView{
    NSLog(@"Did Change");
    
}
-(void)textViewDidEndEditing:(UITextView *)textView{
    NSLog(@"Did End editing");
        [self setViewMovedUp:NO heightKeyboard:200];
    
}
-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    return YES;
}
-(void)setViewMovedUp:(BOOL)movedUp heightKeyboard:(double)height
{
    __block  CGRect rect = self.view.frame;
    [UIView animateWithDuration:0.1 animations:^{
        if (movedUp) {
            rect.size.height -= height;
        }else
        {
            rect.size.height += height;
        }
    }completion:^(BOOL finished){self.view.frame = rect;}];
}
- (IBAction)addComment:(id)sender {
    NSLog(@"add comment");
    
    NetworkManager *network = [[NetworkManager alloc]init];
    
    if (self.textCommet.text.length == 0 || [self.textCommet.text isEqualToString:@""]) {
        UIAlertView *alert= [[UIAlertView alloc ]initWithTitle:@"Information" message:@"Field can't be empty " delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        [network addTheComment:self.pinID comment:self.textCommet.text];
        self.textCommet.text =@"";
        NSMutableArray *arrayCommentTemp = [network  geTheCommentList:self.pinID];
        ((CommentListTableViewController *)self.childViewControllers.firstObject).arrayComments = [arrayCommentTemp mutableCopy];
         [((CommentListTableViewController *)self.childViewControllers.firstObject).tableView reloadData];
        
    }
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NetworkManager *network = [[NetworkManager alloc]init];
    NSMutableArray *arrayCommentTemp = [network  geTheCommentList:self.pinID];
    ((CommentListTableViewController *)self.childViewControllers.firstObject).arrayComments = [arrayCommentTemp mutableCopy];

}

@end
