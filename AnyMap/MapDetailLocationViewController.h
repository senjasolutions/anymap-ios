/*-*-objc-*-*/
//  MapDetailLocationViewController.h
//  AnyMap
//
//  Created by Antonio M on 01/10/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReportViewController.h"
#import "mapDetailCollectionViewController.h"
#import "PinUpViewController.h"
#import "PinCreationViewController.h"
#ifndef NO_GM
#import <GoogleMaps/GoogleMaps.h>
#endif
@class ReportViewController,PinCreationViewController;
@interface MapDetailLocationViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *headerLocation;
@property(strong,nonatomic)NSString *titleLocation;
#ifndef NO_GM
@property (strong, nonatomic) IBOutlet GMSMapView *mapViewLocationDetail;
#endif
@property(strong,nonatomic)mapDetailCollectionViewController *mapDetailColletion;
@property(nonatomic)NSInteger indexRow;
@property(copy,nonatomic)NSString * mapID;
@property(copy,nonatomic)NSString * address;
@property (strong,nonatomic)NSMutableArray *pinsArray;
@property(strong,nonatomic)PinUpViewController *pinUpVC;
@property(strong,nonatomic)ReportViewController *report;
@property(strong,nonatomic)PinCreationViewController *pinCre;
@property (nonatomic)BOOL isFromBookmarked;
@property (nonatomic)NSMutableArray *bookmarkMap;
@end
