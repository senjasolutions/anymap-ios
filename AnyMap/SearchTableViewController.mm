//
//  SearchTableViewController.m
//  AnyMap
//
//  Created by Antonio M on 29/10/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import "AppDelegate.h"
#import "SearchTableViewController.h"
#import "ResultSearchTableViewCell.h"
#import "ExploreMapListTableViewCell.h"
#import "OtherCreatorViewController.h"

@interface SearchTableViewController ()

@end

@implementation SearchTableViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    CGFloat ratio = 0;
    if (screenHeight == 480.0f)
    {
        ratio = 178;
    }
    else if (screenHeight == 568.0f)
    {
        ratio = 178;
    }
    else if (screenHeight == 667.0f)
    {
        ratio =178;
    }
    else if (screenHeight == 736.0f)
    {
        ratio = 178;
    }
    
    self.tableView.rowHeight = ratio;
//    auto user = ((AppDelegate *)[UIApplication sharedApplication].delegate).currentUser;
//    using std::string;
//    user->search(self.term.UTF8String, search);
//    jmaps = search["maps"];
//    jlocations = search["places"];
//    NSInteger countMaps = jmaps.size();
//    NSInteger countLocations = jlocations.size();
//    NSLog(@"ini L %ld adal %ld", (long)countLocations, (long)countMaps);
//    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return jmaps.size()+jlocations.size();
    return  0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  
//        auto map = (jmaps)[static_cast<int>(indexPath.row)];
//        
        ExploreMapListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"exploreMapList"];
        if (!cell) {
            NSLog(@"add the cell");
            [tableView registerNib:[UINib nibWithNibName:@"Clist" bundle:nil] forCellReuseIdentifier:@"exploreMapList"];
            cell = [tableView dequeueReusableCellWithIdentifier:@"exploreMapList" forIndexPath:indexPath];
            
        }
//        cell.bookmark.tag = indexPath.row;
//        cell.creatorButton.tag = indexPath.row;
//        [cell.bookmark addTarget:self action:@selector(addBookmark:) forControlEvents:UIControlEventTouchUpInside];
//        [cell.creatorButton addTarget:self action:@selector(otherCreator:) forControlEvents:UIControlEventTouchUpInside];
//        cell.headerMap.text = [NSString stringWithUTF8String:map["title"].asString().c_str()];
//        cell.counterLocation.text = [NSString stringWithFormat:@"%s Locations ", map["no_of_pins"].asString().c_str()];
//        cell.descriptionMap.text = [NSString stringWithUTF8String:map["description"].asString().c_str()];
//        [cell.creatorName setTitle: [NSString stringWithUTF8String:map["profile_name"].asString().c_str()] forState:UIControlStateNormal];
//    cell.map.image = nil;
//    cell.map.tag = indexPath.row;
//    cell.creatorPhoto.image = nil;
//    cell.creatorPhoto.tag = indexPath.row;
//
//    auto image_manager = [AMImageManager sharedManager];
//
//    auto image_url = map["image"].asString();
//    __block auto image = [image_manager imageForURL:[NSURL URLWithString:[NSString stringWithUTF8String:image_url.c_str()]]];
//    if (image)
//        cell.map.image = image;
//    else {
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//            auto image_data = new AMImageData();
//            image_data->memory = static_cast<char*>(malloc(1));
//            image_data->size = 0;
//            image_data->index = static_cast<int>(indexPath.row);
//            AnyMap::HTTPGETRequest<AMImageData> request{image_url};
//            request.send(image_data, AMSaveImage);
//            image = [UIImage imageWithData:[NSData dataWithBytes:image_data->memory length:image_data->size]];
//            [image_manager setImage:image forURL:[NSURL URLWithString:[NSString stringWithUTF8String:image_url.c_str()]]];
//            if (cell.map.tag == static_cast<NSInteger>(image_data->index))
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    cell.map.image = image;
//                });
//        });
//    }
//
//    auto profile_image_url = map["profile_image"].asString();
//    __block auto profileImage = [image_manager imageForURL:[NSURL URLWithString:[NSString stringWithUTF8String:profile_image_url.c_str()]]];
//    if (profileImage)
//        cell.creatorPhoto.image = profileImage;
//    else {
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//            auto image_data = new AMImageData();
//            image_data->memory = static_cast<char*>(malloc(1));
//            image_data->size = 0;
//            image_data->index = static_cast<int>(indexPath.row);
//            AnyMap::HTTPGETRequest<AMImageData> request{profile_image_url};
//            request.send(image_data, AMSaveImage);
//            profileImage = [UIImage imageWithData:[NSData dataWithBytes:image_data->memory length:image_data->size]];
//            [image_manager setImage:profileImage forURL:[NSURL URLWithString:[NSString stringWithUTF8String:profile_image_url.c_str()]]];
//            if (cell.creatorPhoto.tag == static_cast<NSInteger>(image_data->index))
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    cell.creatorPhoto.image = profileImage;
//                });
//        });
//    }
//    
        return cell;


}

-(IBAction)addBookmark:(UIButton*) sender{
//    if (!((AppDelegate *)[UIApplication sharedApplication].delegate).currentUser->is_logged_in()) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information"
//                                                        message:@"Please signup or login to bookmark this map"
//                                                       delegate:self
//                                              cancelButtonTitle:nil
//                                              otherButtonTitles:@"LOGIN",@"SIGN UP" ,nil];
//        [alert show];
//        
//        
//    }else
//    {
//        sender.selected =!sender.selected;
//        NSLog(@"enter this code");
//        auto user = ((AppDelegate *)[UIApplication sharedApplication].delegate).currentUser;
//        auto maps = user->get_maps();
//        auto map = (*maps)[static_cast<int>(sender.tag)];
//        auto map_id = map["id"].asString();
//        if (sender.selected)
//            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//                user->bookmark(map_id);
//            });
//        else
//            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//                user->unbookmark(map_id);
//            });
//        NSString *idMap = [NSString stringWithUTF8String:map["id"].asString().c_str()];
//        NSLog(@" id map : %@",idMap);
//    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self.parentViewController performSegueWithIdentifier:@"login" sender: self];
        
    }
    if (buttonIndex == 1) {
        [self.parentViewController performSegueWithIdentifier:@"signup" sender: self];
    }
}
-(IBAction)otherCreator:(UIButton*) sender{
//    NSLog(@"Row Creator %ld", (long)sender.tag);
//    auto maps = ((AppDelegate *)[UIApplication sharedApplication].delegate).currentUser->get_maps();
//    auto map = (*maps)[static_cast<int>(sender.tag)];
//
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"OuterProfile" bundle:nil];
//    OtherCreatorViewController *otherCreator= [storyboard instantiateViewControllerWithIdentifier:@"OtherCreatorViewController"];
//    otherCreator.profile =[NSString stringWithUTF8String:map["profile_name"].asString().c_str()];
//    otherCreator.counterFollowers =[NSString stringWithUTF8String:map["profile_followers"].asString().c_str()];
//    otherCreator.counterFollowings =[NSString stringWithUTF8String:map["profile_following"].asString().c_str()];
//    otherCreator.indexUser = sender.tag;
//    otherCreator.describes=[NSString stringWithUTF8String:map["title"].asString().c_str()];
//
//    auto image_manager = [AMImageManager sharedManager];
//    auto profile_image_url = map["profile_image"].asString();
//    __block auto profileImage = [image_manager imageForURL:[NSURL URLWithString:[NSString stringWithUTF8String:profile_image_url.c_str()]]];
//    if (profileImage)
//        otherCreator.avatarProfile.image = profileImage;
//    else {
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//            auto image_data = new AMImageData();
//            image_data->memory = static_cast<char*>(malloc(1));
//            image_data->size = 0;
//            AnyMap::HTTPGETRequest<AMImageData> request{profile_image_url};
//            request.send(image_data, AMSaveImage);
//            profileImage = [UIImage imageWithData:[NSData dataWithBytes:image_data->memory length:image_data->size]];
//            [image_manager setImage:profileImage forURL:[NSURL URLWithString:[NSString stringWithUTF8String:profile_image_url.c_str()]]];
//            dispatch_async(dispatch_get_main_queue(), ^{
//                otherCreator.avatarProfile.image = profileImage;
//            });
//        });
//    }
//    
//    [self.navigationController presentViewController:otherCreator animated:YES completion:nil];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
