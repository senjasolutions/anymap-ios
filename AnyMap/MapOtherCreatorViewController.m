//
//  MapOtherCreatorViewController.m
//  AnyMap
//
//  Created by Antonio M on 21/01/16.
//  Copyright © 2016 AnyMap. All rights reserved.
//
#import "AppDelegate.h"
#import "ExploreMapListTableViewCell.h"
#import "MapOtherCreatorViewController.h"
#import <AFNetworking/AFNetworking.h>
#import "UIImageView+AFNetworking.h"
#import "MBProgressHUD.h"
#import "NetworkManager.h"
#import "LoginVC.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface MapOtherCreatorViewController ()


@end

@implementation MapOtherCreatorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    CGFloat ratio = 0;
    if (screenHeight == 480.0f)
    {
        ratio = 178;
    }
    else if (screenHeight == 568.0f)
    {
        ratio = 178;
    }
    else if (screenHeight == 667.0f)
    {
        ratio =178;
    }
    else if (screenHeight == 736.0f)
    {
        ratio = 178;
    }
    
    self.tableView.rowHeight = ratio;
    [self.tableView setContentInset:UIEdgeInsetsMake(0.0, 0.0, 9.0, 0.0)];
    // Do any additional setup after loading the view.
    
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSMutableArray *mapsArray = ((AppDelegate *)[UIApplication sharedApplication].delegate).downloadMaps ;
    NSArray *filterMapsArray  = [mapsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"created_by_id=%@",self.creator_id]];
    
    NSMutableArray *filteredArray = [NSMutableArray arrayWithArray:filterMapsArray];
    self.mapsOtherCreator = [filteredArray mutableCopy];
    [self.tableView reloadData];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return self.mapsOtherCreator.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    ExploreMapListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"exploreMapList"];
    if (!cell) {
        NSLog(@"add the cell");
        [tableView registerNib:[UINib nibWithNibName:@"Clist" bundle:nil] forCellReuseIdentifier:@"exploreMapList"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"exploreMapList" forIndexPath:indexPath];
        
    }
    NSString *value = [self.mapsOtherCreator objectAtIndex:indexPath.row];
    NSUserDefaults *standar = [[NSUserDefaults alloc]init];
    NSMutableArray *arrayBookmarkedMaps  = [standar valueForKey:@"bookmarkedMaps"];
    cell.bookmark.selected = NO;
    for (int i = 0; i < [arrayBookmarkedMaps count]; i++) {
        if ([[arrayBookmarkedMaps objectAtIndex:i] containsObject:[value valueForKey:@"id"]]) {
            cell.bookmark.selected = YES;
            break;
        }
    }
    cell.bookmark.tag = indexPath.row;
  cell.creatorButton.hidden = YES;
    [cell.bookmark addTarget:self action:@selector(addBookmark:) forControlEvents:UIControlEventTouchUpInside];
     cell.creatorPhoto.hidden = YES;
    cell.headerMap.text = [value valueForKey:@"title"];
    cell.counterLocation.text = [NSString stringWithFormat:@"%@ Locations ", [value valueForKey:@"no_of_pins"]];
    
    if ([value valueForKey:@"description"] == (id)[NSNull null]) {
        cell.descriptionMap.text = @"";
    }else{
        cell.descriptionMap.text = [value valueForKey:@"description"];
    }
    
    if ([value valueForKey:@"image"] == (id)[NSNull null]) {
        cell.map.image = [UIImage imageNamed:@""];
    }
    else
    {
        [cell.map sd_setImageWithURL:[NSURL URLWithString:[value valueForKey:@"image"]] placeholderImage:[UIImage imageNamed:@""] ];
    }

    
    return cell;

}
-(IBAction)addBookmark:(UIButton*) sender{
    NSString *mapID= [self.mapsOtherCreator objectAtIndex:sender.tag];
    NSMutableArray *mapTemp =[self.mapsOtherCreator objectAtIndex:sender.tag];
    NSLog(@"map id: %@",[mapID valueForKey:@"id"]);
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"hasSignIn"])
    {
        sender.selected =!sender.selected;
        NetworkManager *network = [[NetworkManager alloc]init];
        
        if (sender.selected) {
            NSLog(@"bookmark the map");
            NSString *bookmarkMap = [NSString stringWithFormat:@"%@|0",[mapID valueForKey:@"id"]];
            [network bookmarkMap:bookmarkMap  statusMap:[mapID valueForKey:@"id"] arrayMap:mapTemp];
        }
        else
        {
            NSLog(@"unbookmark the map");
            NSString *unbookmarkmap = [NSString stringWithFormat:@"%@|1",[mapID valueForKey:@"id"]];
            [network unBookmarkMap:unbookmarkmap statusMap:[mapID valueForKey:@"id"] arrayMap:mapTemp];
        }
    }
    else
    {
        [self infoNeedSignInToBookmark];
    }
}

- (void)infoNeedSignInToBookmark
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Information"
                                  message:@"Please signup or login to bookmark this map"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* no = [UIAlertAction
                         actionWithTitle:@"Cancel"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                         }];
    
    
    UIAlertAction*  yes = [UIAlertAction
                           actionWithTitle:@"Login/Signup"
                           style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * action)
                           {
                               
                               UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                               LoginVC *login = [main instantiateViewControllerWithIdentifier:@"LOGIN"];
                               [self presentViewController:login animated:YES completion:nil];
                           }];
    
    
    [alert addAction:no];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        NSLog(@"nilai parent VC nya adalah : %@",self.parentViewController);
        [self.parentViewController performSegueWithIdentifier:@"signup" sender: self];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
