//
//  Pin.h
//  AnyMap
//
//  Created by Antonio M on 16/11/15.
//  Copyright © 2015 AnyMap. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pin : NSObject
@property (assign, nonatomic) NSString * idPin;
@property (strong, nonatomic) NSString* longitude;
@property (strong, nonatomic) NSString* latitude;
@end
